<?php

/**
 *  default message for errors or other message in api
 */
return [
	'response' => [
		'successfully_response'    => 'Your request successfully handled.' ,
		'an_error_occurred'        => 'An error has occurred' ,
		'method_not_allowed'       => 'Your requested method not allowed.' ,
		'unauthenticated'          => 'Not authenticated. Make sure your token is valid' ,
		'bad_request_error'        => 'The request is not in accepted format' ,
		'unauthorized_error'       => 'Unauthorized error make sure you have correct access' ,
		'forbidden_error'          => 'Forbidden error make sure you have correct access' ,
		'internal_error'           => 'Sorry. internal server error.' ,
		'uploading_file_error'     => 'Error uploading file' ,
		'validation_error_message' => 'The entered data is invalid' ,
		'try_again' 			   => 'An error has occurred please try again' ,

		'item_created'			   => 'Your request item create successfully.' ,
		'item_updated'			   => 'Your request item updated successfully.' ,
		'item_patched'			   => 'Your request item patched successfully.' ,
		'item_deleted'			   => 'Your request item deleted successfully.' ,
		
		'item_not_found'           => 'Your requested resource was not found.',
		'item_created_error'       => 'Error when create item!',
		'item_updated_error'       => 'Error when update item!',
		'item_patched_error'       => 'Error when patch item!',
		'item_deleted_error'       => 'Error when delete item!',
		'query_error'    	       => 'Error when execute query!',
		'url_not_valid'    	       => 'Url not valid!',
		//booking
		'booking_not_found'    	       => 'booking not found!',
	
	],
    'notification' => [
        'custom_notify_body' => "hello from turpal!",
    ]
];
