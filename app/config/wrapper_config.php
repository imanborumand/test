<?php

/*
 * get url and status of every DS from this config file
 *
 */
return [
	 "ds" => [
	 	'app_state' => APP_STATES_PRODUCTION,
	 	'base_url' => 'http://dswrapper.ddis.live/',
	 	'status' => true,
	 	'check_mobile' =>  [
	 		'path' => 'check-mobile',
	 		'header' => [
	 			'Content-Type:application/json',
	 			'token:U7ZXg0flt1m79lUcFqAua3I1CaR6jgiXbEttM33deN83G7sRdKkLr9L2lhuhAD7Y'
	 		]
	 	],
	 	'login' =>  [
	 		'path' => 'login',
	 		'header' => [
	 			'Content-Type:application/json',
	 			'token:U7ZXg0flt1m79lUcFqAua3I1CaR6jgiXbEttM33deN83G7sRdKkLr9L2lhuhAD7Y'
	 		]
	 	],
	 ],
	"test" => [
		'app_state' => APP_STATES_STAGING,
		'base_url' => 'http://testwrapper.ddis.live/',
		'status' => true,
		'check_mobile' =>  [
			'path' => 'check-mobile',
			'header' => [
				'Content-Type:application/json',
				'token:U7ZXg0flt1m79lUcFqAua3I1CaR6jgiXbEttM33deN83G7sRdKkLr9L2lhuhAD7Y'
			]
		],
		'login' =>  [
			'path' => 'login',
			'header' => [
				'Content-Type:application/json',
				'token:U7ZXg0flt1m79lUcFqAua3I1CaR6jgiXbEttM33deN83G7sRdKkLr9L2lhuhAD7Y'
			]
		],
	],

	"testmaster" => [
		'app_state' => APP_STATES_ALL,
		'base_url' => 'http://testmasterwrapper.ddis.live/',
		'status' => true,
		'check_mobile' =>  [
			'path' => 'check-mobile',
			'header' => [
				'Content-Type:application/json',
				'token:U7ZXg0flt1m79lUcFqAua3I1CaR6jgiXbEttM33deN83G7sRdKkLr9L2lhuhAD7Y'
			]
		],
		'login' =>  [
			'path' => 'login',
			'header' => [
				'Content-Type:application/json',
				'token:U7ZXg0flt1m79lUcFqAua3I1CaR6jgiXbEttM33deN83G7sRdKkLr9L2lhuhAD7Y'
			]
		],
	],
	
	
	 "demo" => [
	 	'app_state' => APP_STATES_PRODUCTION,
	 	'base_url' => 'http://demowrapper.ddis.live/',
	 	'status' => true,
	 	'check_mobile' =>  [
	 		'path' => 'check-mobile',
	 		'header' => [
	 			'Content-Type:application/json',
	 			'token:U7ZXg0flt1m79lUcFqAua3I1CaR6jgiXbEttM33deN83G7sRdKkLr9L2lhuhAD7Y'
	 		]
	 	],
	 	'login' =>  [
	 		'path' => 'login',
	 		'header' => [
	 			'Content-Type:application/json',
	 			'token:U7ZXg0flt1m79lUcFqAua3I1CaR6jgiXbEttM33deN83G7sRdKkLr9L2lhuhAD7Y'
	 		]
	 	],
	 ],
	
     "gurtur" => [
     	'app_state' => APP_STATES_PRODUCTION,
         'base_url' => 'http://gurturwrapper.ddis.live/',
         'status' => true,
         'check_mobile' =>  [
             'path' => 'check-mobile',
             'header' => [
                 'Content-Type:application/json',
                 'token:U7ZXg0flt1m79lUcFqAua3I1CaR6jgiXbEttM33deN83G7sRdKkLr9L2lhuhAD7Y'
             ]
         ],
         'login' =>  [
             'path' => 'login',
             'header' => [
                 'Content-Type:application/json',
                 'token:U7ZXg0flt1m79lUcFqAua3I1CaR6jgiXbEttM33deN83G7sRdKkLr9L2lhuhAD7Y'
             ]
         ],
     ],

];
