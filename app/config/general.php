<?php

/*
 * global config for user in project
 */
return [
	'paginate_number' => 20,

	'contact_type_keys' =>[
		'fcm'    => 'firebaseToken',
		'mobile' => 'mobile',
		'email' => 'email'
	],

	'experience_type' => [
		'full',
		'first_page'
	],

    'devices' => [
    	'default',
        'mobile',
        'web',
        'desktop',
    ],


    'app_version' => [

        ANDROID_DEVICE_NAME => [
            'current' => '1.0.0',
            'force_update' => false,
            'app_version_message' => 'please update your application',
            'app_version_title' => 'New Update!',
            'store_url' => 'http://play.google.com/'
        ],

        IPHONE_DEVICE_NAME => [
            'current' => '1.0.0',
            'force_update' => false,
            'app_version_message' => 'please update your application',
            'app_version_title' => 'New Update!',
            'store_url' => 'http://test.com/'
        ]
    ],
	'http_code' =>[
		200,
		201,
		404,
		500,
		401,
		405,
		403,
		400,
		422,
	]

];
