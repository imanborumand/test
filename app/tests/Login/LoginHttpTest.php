<?php

namespace Tests\Product;

use App\Http\Resources\V1\Language\LanguageResource;
use App\Models\Repositories\Language\LanguageRepository;
use App\Services\V1\Language\LanguageService;
use Faker\Factory;
use Tests\TestCase;
use Tests\Traits\MigrateFreshSeedOnce;

/**
 * Class LoginHttpTest
 *
 * @package Tests\Product
 */
class LoginHttpTest extends TestCase
{
	
	use  MigrateFreshSeedOnce{
		setUp as public traitSetUp;
	}
	
	/**
	 * @var \Illuminate\Contracts\Foundation\Application
	 */
	private $faker;
	/**
	 * @var
	 */
	private $deviceOs;
	/**
	 * @var
	 */
	private $mobile;
	/**
	 * @var
	 */
	private $languageRepository;
	
	private $language;
	/**
	 * @var string
	 */
	private $userName;
	/**
	 * @var string
	 */
	private $bookingId;
	/**
	 * @var string
	 */
	private $appName;
	
	
	public function setUp():void
	{
		$this->traitSetUp();
		$this->faker = Factory::create();
		$this->deviceOs = ANDROID_DEVICE_NAME;
		//$this->mobile = $this->faker->regexify( '09([0123])\d{8}');
		$this->languageRepository = app(LanguageRepository::class);
		$this->language = $this->languageRepository->getByIso2(DEFAULT_LANGUAGE_ISO2);
		$this->mobile = '97112345600';
		$this->userName =  'turpal test v03';
		$this->bookingId = 'Dtr280420';
		$this->appName = 'miniCs1';
	}
	
	public function testUserCheckMobile()
	{
		$data = [
			"mobile"      => $this->mobile,
			"language_id" => $this->language->id,
		];
		$response = $this->json('POST', route('login.user.check.mobile'),$data,['device_os' => $this->deviceOs]);
		$response->assertStatus(200);
		$result = json_decode($response->getContent(),1);
		$this->assertEquals($result['data']['mobile'], $this->mobile);
	}
	
	public function testLoginGetBookings()
	{
		$data = [
			"mobile"      => $this->mobile,
			"booking_id"  => "",
			"name"        => $this->userName,
		];
		$response = $this->json('POST', route('login.get.bookings'),$data);
		$result = json_decode($response->getContent(),1);
		$this->assertEquals($result['status_code'], 2000);
		$this->assertTrue($result['status']);
		$response->assertStatus(200);
	}

	public function testLogin()
	{
		$data = [
			"booking_id"  => $this->bookingId,
			"name"        => $this->userName,
			"mobile"      => $this->mobile,
			"language_id" => $this->language->id,
		];
		$response = $this->json('POST', route('login.user'),$data,['app-name'=> $this->appName]);
		$response->assertStatus(200);
		$result = json_decode($response->getContent(),1);
		$this->assertEquals($result['status_code'], 2000);
		$this->assertEquals($result['data']['booking']['booking']['id'], strtoupper($this->bookingId));
		$this->assertTrue($result['status']);
		$response->assertStatus(200);
	}
	
	/**
	 * @throws \Throwable
	 */
	public function tearDown():void
	{
		parent::tearDown();
	}
}
