<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


//routes fro open app by hash key from qr code
Route::namespace('\App\Http\Controllers\Auth\User')->group(function() {
	Route::post('/login/{hash_code}', 'UserLoginByHashController@loginByHash')->name('login.hash');
});