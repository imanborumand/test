<?php

//login routes
Route::prefix( 'login')->middleware('logRequestResponse')->group(function() {

	Route::prefix('user')->namespace('\App\Http\Controllers\Auth\User')->group(function() {
			Route::post('/check-mobile', 'UserAuthController@checkMobile')->name('login.user.check.mobile')->middleware('checkDeviceInfo'); //check mobile
			Route::post('/get-bookings', 'UserAuthController@getBookings')->name('login.get.bookings'); // get all bookings from all DMS [wrappers]
			Route::post('/',	         'UserAuthController@login')->name('login.user')->middleware('CheckRequestNumber');
	});

	Route::post('/select-booking/{booking_id}', '\App\Http\Controllers\Auth\User\UserAuthController@selectBooking')
		 ->middleware('auth:users')->name('login.select_booking');

	//routes fro open app by hash key from qr code
	Route::namespace('\App\Http\Controllers\Auth\User')->group(function() {
		Route::get('/open-app/{hash_code}', 'UserLoginByHashController@redirectToStore')->name('redirect.to.store');
		Route::get('/by-hash/{hash_code}', 'UserLoginByHashController@loginByHash')->name('login.by.hash');
	});

});

Route::prefix( 'auth/user')->middleware('logRequestResponse')->namespace('\App\Http\Controllers\Auth\User')->group(function() {
	Route::post('/logout',	         'UserAuthController@logout')->name('logout.user')->middleware('auth:users');
});




//route group v1
Route::namespace('\App\Http\Controllers\V1')->middleware('logRequestResponse')->group(function() {

	//language
	Route::prefix('languages')->namespace('Language')->group(function() {
		Route::get('/', 			     'LanguageController@all')->name('language.all')->middleware('checkDeviceInfo');
		Route::post('/', 			     'LanguageController@store')->name('language.store');
		Route::get('/{language_id}',     'LanguageController@getWith')->name('language.getWith')->middleware('checkDeviceInfo');
	});

	//todo change middleware to admin
	Route::prefix('translates')->namespace('Language')->middleware('auth:users')->group(function() {
		Route::post('/store',			 'TranslateController@storeTranslates')->name('translates.store');
		Route::post('/import-excel',	 'TranslateController@importExcel')->name('translates.import.excel');
	});

	Route::prefix('bookings')->middleware('auth:users')->group(function() {

		//booking routes
		Route::namespace('Booking')->group(function() {
			Route::get('/{booking_id}',  'BookingController@getBooking')->name('booking.get');
			Route::get('/',  			 'BookingController@all')->name('booking.all');
			Route::post('/add',			 'BookingController@addBooking')->name('booking.add');
		});

		//invoice of booking
		Route::namespace('Invoice')->group(function() {
			Route::get('/{booking_id}/invoices', 'InvoiceController@getInvoiceBooking')->name('invoice.booking.get');
		});

	});

    Route::prefix('itinerary')->namespace('Itinerary')->middleware('auth:users')->group(function() {
        Route::get('/{booking_id}','ItineraryController@getItinerary')->name('itinerary.get');
    });

    Route::prefix('contact')->namespace('Contact')->middleware('auth:users')->group(function() {
			Route::get('/',				  'ContactController@all')->name('contact.all');
			Route::get('/{contact_id}',   'ContactController@get')->name('contact.get');
			Route::post('/' ,	  	      'ContactController@store')->name('contact.store');
			Route::put('/{contact_id}',   'ContactController@update')->name('contact.update');
			Route::delete('/{contact_id}','ContactController@delete')->name('contact.delete');

			Route::prefix('user')->middleware('auth:users')->group(function() {
				Route::post('/' , 	 			 'ContactUserController@store')->name('contact.user.store');
				Route::put('/{contact_user_id}', 'ContactUserController@update')->name('contact.user.update');
				Route::delete('/{contact_user_id}', 'ContactUserController@delete')->name('contact.user.delete');
		});
    });

	Route::prefix('experiences')->namespace('Experiences')->middleware('auth:users')->group(function() {
		Route::get('/{booking_id}', 'ExperiencesController@get')->name('experiences.get');
	});

	Route::prefix('firebase')->namespace('Notify')->group(function() {
	    Route::post('notify-to-user/{user_id}/',    'FirebaseController@sendNotifyToUser')->name('firebase.send_notify_to_user');
		Route::post('notify-by-booking/{booking_id}/', 'FirebaseController@notifyByBookingId')->name('firebase.notify_by_booking_id');
		Route::post('notify-by-token/{firebase_token}/', 'FirebaseController@notifyByFirebaseToken')->name('firebase.notify_by_firebase_token');
	});

	Route::prefix('user')->namespace('User')->middleware('auth:users')->group(function() {
	    Route::put('/',                              'UserController@update')->name('user.update');
	    Route::get('/profile',                       'UserController@profile')->name('user.profile');
	    Route::put('/update-language/{language_id}', 'UserController@updateLanguage')->name('user.update.language')->middleware('checkDeviceInfo');
	});

	Route::prefix('country')->namespace('Country')->middleware('checkDeviceInfo')->group(function() {
			Route::get('/',                'CountryController@all')->name('country.all');
			Route::get('/{iso2}',          'CountryController@get')->name('country.get');
	});

	Route::namespace('App')->group(function() {
		Route::get('/init', 'AppController@init')->name('init')->middleware('checkDeviceInfo');
	});

	Route::prefix('notify')->namespace('Notify')->middleware('auth:users')->group(function() {
			Route::get('/',                   'NotifyController@all')->name('notify.all');
			Route::get('/users/{user_id}',    'NotifyController@getUserNotifies')->name('notify.user');
			Route::get('/seen/{notify_id}',   'NotifyController@changeSeenNotify')->name('notify.seen');
			Route::delete('/{notify_id}',     'NotifyController@delete')->name('notify.delete');
	});
	
	Route::prefix('hotel')->namespace('Hotel')->middleware('auth:users')->group(function() {
			Route::get('/by-name/{iso2}/{name}',   'HotelController@getByName')->name('hotel.get.by.name');
	});

});


//add middleware
//todo move to admin route or back office system
Route::prefix('admin')->namespace('\App\Http\Controllers\Admin')->group(function() {
	
    Route::prefix('log')->namespace("Log")->group(function(){
    	Route::get('/notifications', 'AdminLogController@allNotificationsLog')->name('admin.notification.log');
    	Route::get('/request-response', 'AdminLogController@responseRequestLog')->name('admin.response.request.log');
    	Route::get('/user-info-to-wrapper', 'AdminLogController@sendUserInfoToWrapper')->name('admin.send.user.info.to.wrapper.log');
		Route::get('/see-contact-users', 'AdminLogController@seeContactUser');
	});

});
