<?php namespace App\Observers;

use App\Models\Entities\User\User;

/**
 * Class UserObserver
 * observer for User  model
 * @package App\Observers
 */
class UserObserver
{


	public function creating(User $user)
	{

	}


    public function created(User $user)
    {

    }


    public function updating(User $user)
    {

    }


    public function updated(User $user)
    {

    }


    public function saving(User $user)
    {

    }


    public function saved(User $user)
    {

    }

}
