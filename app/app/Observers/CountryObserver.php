<?php namespace App\Observers;

use App\Models\Entities\Country\Country;
use Illuminate\Support\Facades\Cache;

/**
 * Class CountryObserver
 * observer for Country model
 * @package App\Observers
 */
class CountryObserver
{
    public function created(Country $country)
    {

    }


    public function updating(Country $country)
    {

    }


    public function updated(Country $country)
    {
        Cache::forget("allCountriesName"); //use in CountryRepository
    }


    public function saving(Country $country)
    {

    }


    public function saved(Country $country)
    {
        Cache::forget("allCountriesName"); //use in CountryRepository
    }

}
