<?php namespace App\Observers\Device;




use App\CustomStuff\Classes\DetectDeviceInfo;
use App\Models\Entities\Device\DeviceBrand;
use Illuminate\Support\Facades\Cache;

/**
 * Class DeviceBrandObserver
 * observer for DeviceBrandModel
 *
 * @package App\Observers\Device
 */
class DeviceBrandObserver
{

    private $detectDeviceInfo = null;

    public function __construct()
    {
        $this->detectDeviceInfo = app(DetectDeviceInfo::class);
    }


    public function created(DeviceBrand $deviceBrand)
    {
        Cache::forget($this->detectDeviceInfo->deviceBrandCacheName); //todo check after create models and crud!
    }


    public function updating(DeviceBrand $deviceBrand)
    {

    }


    public function updated(DeviceBrand $deviceBrand)
    {
        Cache::forget($this->detectDeviceInfo->deviceBrandCacheName); //todo check after create models and crud!
    }


    public function saving(DeviceBrand $deviceBrand)
    {

    }


    public function saved(DeviceBrand $deviceBrand)
    {
        Cache::forget($this->detectDeviceInfo->deviceBrandCacheName); //todo check after create models and crud!
    }

}
