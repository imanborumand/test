<?php namespace App\Observers\Device;



use App\CustomStuff\Classes\DetectDeviceInfo;
use App\Models\Entities\Device\DeviceType;
use Illuminate\Support\Facades\Cache;

/**
 * Class DeviceTypeObserver
 * observer for DeviceType model
 *
 * @package App\Observers\Device
 */
class DeviceTypeObserver
{

    private $detectDeviceInfo = null;

    public function __construct()
    {
        $this->detectDeviceInfo = app(DetectDeviceInfo::class);
    }


    public function created(DeviceType $deviceType)
    {
        Cache::forget($this->detectDeviceInfo->deviceTypeCacheName); //todo check after create models and crud!
    }



    public function updating(DeviceType $deviceType)
    {
        //todo check after create models and crud!
        Cache::forget($this->detectDeviceInfo->deviceTypeCacheName);
        Cache::forget($this->detectDeviceInfo->deviceTypeCacheName . $deviceType->name);
        Cache::forget($this->detectDeviceInfo->deviceTypeCacheName . $deviceType->id);
    }


    public function updated(DeviceType $deviceType)
    {

    }


    public function saving(DeviceType $deviceType)
    {

    }


    public function saved(DeviceType $deviceType)
    {
        //todo check after create models and crud!
        Cache::forget($this->detectDeviceInfo->deviceTypeCacheName);
        Cache::forget($this->detectDeviceInfo->deviceTypeCacheName . $deviceType->name);
        Cache::forget($this->detectDeviceInfo->deviceTypeCacheName . $deviceType->id);
    }
}
