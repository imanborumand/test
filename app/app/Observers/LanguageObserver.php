<?php namespace App\Observers;

use App\Models\Entities\Device\DeviceType;
use App\Models\Entities\Language\Language;
use App\Services\V1\App\AppService;
use Illuminate\Support\Facades\Cache;

/**
 * Class LanguageObserver
 * observer for Language model
 * @package App\Observers
 */
class LanguageObserver
{
    private $appService;

    public function __construct()
    {
        $this->appService = app(AppService::class);
    }


    public function created(Language $language)
    {

    }


    public function updating(Language $language)
    {

        //remove all cache - this cache Create in AppService in getTranslateCache function
        $deviceTypes = DeviceType::all();
        foreach($deviceTypes as $deviceType) {
            Cache::forget($this->appService->cacheTranslateVersionKey . $language->id . $deviceType->id);
        }
        //End remove all cache - this cache Create in AppService in getTranslateCache function

    }


    public function updated(Language $language)
    {

    }


    public function saving(Language $language)
    {

    }


    public function saved(Language $language)
    {

    }

}
