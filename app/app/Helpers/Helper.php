<?php

use App\Exceptions\CustomResponseException;
use App\Exceptions\CustomValidationException;
use Illuminate\Config\Repository;
use Illuminate\Support\Arr;
////RESPONSE STATUS CODE
const STATUS_CODE_OK       = 2000;
const STATUS_CODE_CREATED  = 2001;
const STATUS_CODE_NOT_FOUND   = 4004;
const STATUS_CODE_INTERNAL_SERVER_ERROR      = 5000;
const STATUS_CODE_UNAUTHENTICATED = 4001;
const STATUS_CODE_METHOD_NOT_ALLOWED   = 4005;
const STATUS_CODE_FORBIDDEN   = 4003;
const STATUS_CODE_BAD_REQUEST   = 4000;
const STATUS_CODE_UNPROCESSABLE_ENTITY  = 4022;


//BOOK STATUS
const BOOK_CONFIRM             			   = 'CONFIRM';
const BOOK_CANCEL			   			   = 'CANCEL';
const BOOK_ON_REQUEST		   			   = 'ON_REQUEST';
const BOOK_VOUCHER             			   = 'VOUCHER';
const BOOK_ISSUE             			   = 'ISSUE';
const BOOK_REJECT						   = 'REJECT';

////CONSTANT NAME
const EN_DEFAULT_WORD   = "default"; //   use ever where when use default key
const DEFAULT_LANGUAGE_ISO2  = "en";
const ANDROID_DEVICE_NAME = "android";
const IPHONE_DEVICE_NAME = "iphone";
const STORAGE_DISK = "local";
const DEVICE_INFO_KEY_IN_REQUEST = "device_info";
const DEFAULT_DEVICE_SETTING = "default_device_setting";
const CACHE_USER_BY_MOBILE   = "userByMobile"; //user data  and mobile contact row save to cache by this name + user_Id

/////CONSTANT PATH
const STORAGE_AVATAR_PATH = "/storage/user/avatars/";
const PUBLIC_AVATAR_PATH = "/public/user/avatars/";
const FLAG_PATH = "/static_files/country_flag/";
const CUSTOM_EXCEPTIONS_PATH ='App\Exceptions';

///CONSTANT ID
const DEFAULT_LANGUAGE_ID  = 1;
const DEFAULT_DEVICE_TYPE_ID   = 1; //to use the default device key

//for send request and availability wrapper from turpl
const APP_STATES_STAGING = 'STAGING';
const APP_STATES_PRODUCTION = 'PRODUCTION';
const APP_STATES_ALL = 'ALL';// wrapper has access from two state


	if(! function_exists('doRequest')) {
		/**
		 *  create curl request by post, get and another methods
		 *
		 * @param string $url
		 * @param string $method
		 * @param array  $payload
		 * @param array  $headers
		 * @param int    $timeOut
		 * @return bool|string
		 * @throws CustomResponseException
		 */
		function doRequest(string $url, string $method = "POST", array $payload = [], array $headers = [], int $timeOut = 40)
		{
			$timeStart = microtime(true); //for log time curl request
			$ch     = curl_init();
			$header = [
				'Content-Type:application/json',
				'token:U7ZXg0flt1m79lUcFqAua3I1CaR6jgiXbEttM33deN83G7sRdKkLr9L2lhuhAD7Y'
			];
			if(count($header) > 0) {
				$header = array_merge($header, $headers);
			}

			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_POSTREDIR, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, $timeOut);
			curl_setopt($ch, CURLOPT_ENCODING, "UTF-8");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_URL, $url);

			if(count($payload) > 0) {
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
				curl_setopt($ch, CURLOPT_POST, TRUE);
				curl_setopt($ch, CURLOPT_POSTFIELDS,  json_encode($payload));
			} else {
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
				curl_setopt($ch, CURLOPT_POST, TRUE);
			}
			
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $response = curl_exec($ch);
			$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			
			$body = substr($response, $header_size);
			if(jsonValidator($body) === false) {
				$message = config('default_messages.response.try_again');
				throw new CustomResponseException($message,400, 9999 );
			}
			
			curl_close($ch);
			
			request()->merge([
				'curl_info' => [
					"curl_time"       => microTimeFormat($timeStart),
					"response_header" => substr($response, 0, $header_size)
				]
			]);
			
			return $body;
		}

	}


	if(! function_exists('doRequestToMs')) {
		/**
		 *  create curl request by post, get and another methods to All Urls
		 *
		 * @param string $url
		 * @param string $method
		 * @param array  $payload
		 * @param array  $headers
		 * @param int    $timeOut
		 * @return bool|string
		 */
		function doRequestToMs(string $url, string $method = "POST", array $payload = [], array $headers = [], int $timeOut = 40)
		{
			$ch     = curl_init();
			return  curlInit($ch, $url, $method, $payload, $headers, $timeOut);
		}
	
	}


	if(! function_exists('jsonValidator')) {
		//JSON Validator function
		function jsonValidator($data = NULL) {
			if (! empty($data)) {
				@json_decode($data);
				return (json_last_error() === JSON_ERROR_NONE);
			}
			return false;
		}
	}

	if(! function_exists('getWrapperConfig')) {
		/**
		 *  return wrapper config
		 *
		 * @return Repository|mixed
		 */
		function getWrapperConfig() : array
		{
			$appState = env('APP_STATE');
			$wrapperConfig =  config('wrapper_config');
			if (! $appState) {
				return $wrapperConfig;
			}
			
			if ($appState == APP_STATES_STAGING) {
				return getAppState($wrapperConfig, $appState, APP_STATES_STAGING);
				
			} else if ($appState == APP_STATES_PRODUCTION) {
				
				return getAppState($wrapperConfig, $appState, APP_STATES_PRODUCTION);
			}
			
			return $wrapperConfig;
		}
	}


	if (! function_exists('getAppState')) {
		
		/**
		 * return app state
		 * staging
		 * production
		 * for access user and send request
		 *
		 * @param array  $wrapperConfig
		 * @param string $appState
		 * @param string $selectedState
		 * @return array
		 */
		function getAppState(array $wrapperConfig, string  $appState, string $selectedState) : array
		{
			return Arr::where($wrapperConfig, function ($value, $key) use ($appState, $selectedState) {
				if(isset($value['app_state']) && $value['app_state'] == $selectedState || $value['app_state'] == APP_STATES_ALL) {
					return true;
				}
				return false;
			});
			
		}
	}
	

	if(! function_exists('getCurrentWrapperConfigFromHeader')) {
		/**
		 *  return config of wrapper by wrapper_key header
		 *
		 * @return Repository|mixed
		 * @throws CustomValidationException
		 */
		function getCurrentWrapperConfigFromHeader() : array
		{

			$appsName = request()->header('app-name'); //get wrapper app name from header
			if(! $appsName) {
				throw new CustomValidationException("Please add app name params to header!");
			}

			//validate app name
			if(! isset(config('wrapper_config')[$appsName])) {
				throw new CustomValidationException("app name not valid!");
			}

			return config('wrapper_config')[$appsName];
		}
	}


	if(! function_exists('bulkInsertOrUpdate')) {

		 function bulkInsertOrUpdate(array $rows, $table){

			$first = reset($rows);
			$columns = implode( ',', array_map( function( $value ) { return "`{$value}`"; } , array_keys($first) ) );
			$values = implode( ',',
						array_map( function( $row ) {
							 return '(' . implode( ',' ,
										   array_map( function( $value ) {
										   	if(gettype($value) != 'integer'){
												return '"' . str_replace( '"' , '""' , $value ) . '"';
											}else{
										   		return $value;
											}
									 	} ,
									 $row ) ) . ')';
					     } , $rows ));

			$updates = implode( ',', array_map( function( $value ) { return "`$value` = VALUES(`$value`)"; } , array_keys($first) ) );

			$sql = "INSERT INTO `{$table}`({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";

			return DB::statement( $sql );
		}
	}

	if (! function_exists('convertPersianNumbersToEnglish')) {

		function convertPersianNumbersToEnglish($number)
		{
			$persian = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
			$num = range(0, 9);
			return str_replace($persian, $num, $number);
		}
	}
	
	
	if(! function_exists('curlInti')) {
		
		/**
		 * initial curl function for use in customize curls
		 *
		 * @param        $ch
		 * @param string $url
		 * @param string $method
		 * @param array  $payload
		 * @param array  $headers
		 * @param int    $timeOut
		 * @return bool|string
		 */
		function curlInit( $ch, string $url, string $method = "POST", array $payload = [], array $headers = [], int $timeOut = 40)
		{
			$header = [];
			
			if(count($header) > 0) {
				$header = array_merge($header, $headers);
			}
			
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_POSTREDIR, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, $timeOut);
			curl_setopt($ch, CURLOPT_ENCODING, "UTF-8");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_URL, $url);
			
			if(count($payload) > 0) {
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
				curl_setopt($ch, CURLOPT_POST, TRUE);
				curl_setopt($ch, CURLOPT_POSTFIELDS,  json_encode($payload));
			} else {
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
				curl_setopt($ch, CURLOPT_POST, TRUE);
			}
			
//			curl_setopt($ch, CURLOPT_HEADER, 1);
			$return =  curl_exec($ch);
//			$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
//			$header = substr($return, 0, $header_size);
//			$body = substr($response, $header_size);
			
			curl_close($ch);
			return $return;
		}
		
	}

	
	if (! function_exists('microTimeFormat')) {
		
		/**
		 *
		 * this function get startTime and show diff by end time[data param] by Seconds
		 * @param      $data
		 * @param null $format
		 * @param null $lng
		 * @return string
		 */
		function microTimeFormat( $data , $format = null , $lng = null)
		{
			$duration = microtime( true ) - $data;
			$hours    = (int)($duration / 60 / 60);
			$minutes  = (int)($duration / 60) - $hours * 60;
			$seconds  = $duration - $hours * 60 * 60 - $minutes * 60;
			
			return number_format( (float)$seconds , 2 , '.' , '' );
		}
	}



	if (! function_exists('cleanString')) {
		
		/**
		 * this function remote special character that send from mobile app
		 * for example if send تست ‏تست from app if html_decode تست &rlm;تست
		 * and ds not response
		 * this function remove special character
		 *
		 * @param $string
		 * @return string|string[]
		 */
		function cleanString( $string)
		{
			$search = [
				'&#8234;',
				'&lrm;',
				'&#8236;',
				'&rlm;'
			];
			return str_replace($search, ['', '', ''], $string);
		}
	}

