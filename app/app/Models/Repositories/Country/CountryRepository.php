<?php namespace App\Models\Repositories\Country;

use App\Models\Entities\Country\Country;
use App\Models\Entities\Language\Translate;
use App\Models\Repositories\Language\LanguageRepository;
use App\Modles\Repositories\RepositoryBase;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Cache;


class CountryRepository extends RepositoryBase
{

	/**
	 * @var Application
	 */
	protected $languageRepository;

	public function __construct(Country $model)
	{
		$this->model = $model;
		$this->languageRepository = app(LanguageRepository::class);
	}

    /**
	 * return country list With Translation
	 * first check cache for data and if does not exist
	 * store list to cache for next requests
     *
     * @param int    $languageId
     * @param int    $deviceTypeId
     * @param string $countryIso2
     * @return Builder
     */
    public function countriesWithTranslation(int $languageId, int $deviceTypeId, string $countryIso2 = null) : object
    {
        if($countryIso2 != null) {
            return  $this->queryTranslateCountry($languageId, $deviceTypeId)->where('iso2', $countryIso2)->first();
        }

        //add all country by translates to cache
        return Cache::get('allCountryCache' . $languageId . $deviceTypeId, function() use ($languageId, $deviceTypeId) {
            $result = $this->queryTranslateCountry($languageId, $deviceTypeId)->get();

            Cache::forever('allCountryCache' . $languageId . $deviceTypeId, $result);
            return $result;
        });
    }


    /**
     * do query for get translate from database
     * @param int $languageId
     * @param int $deviceTypeId
     * @return Builder
     */
    private function queryTranslateCountry(int $languageId, int $deviceTypeId)
    {
        return $this->model->with(['translateKey' => function($query) use ($languageId, $deviceTypeId) {
            $query->with(['translate' => function($query) use ($languageId, $deviceTypeId) {

                //where for check exists lan and device_id in db
                $query->where(function($query)  use ($languageId, $deviceTypeId) {
                    $query->where('language_id', $languageId)->where('device_type_id', $deviceTypeId)

                        //if not found result by lang and device_id orWhere return result rows that lang equal lang_id and device_id equal  default device_id
                          ->orWhere(function($query) use ($languageId, $deviceTypeId) {
                              $query->where('language_id', $languageId)
                                    ->where('device_type_id', DEFAULT_DEVICE_TYPE_ID)
                                    ->whereNotIn('translate_key_id', function($queryWhereNotIn) use ($languageId, $deviceTypeId)  {

                                        $queryWhereNotIn->select('translate_key_id')
                                                        ->from(with((new Translate())->getTable()))
                                                        ->where('language_id', $languageId)
                                                        ->where('device_type_id', $deviceTypeId);
                                    });
                          //if not found result by lang and device and not found by lang and default device return default translate value for row
                          })->orWhere(function($query) use ($languageId, $deviceTypeId) {
                            $query->where('language_id', DEFAULT_LANGUAGE_ID)
                                  ->where('device_type_id', DEFAULT_DEVICE_TYPE_ID)

                                  ->whereNotIn('translate_key_id', function($queryWhereNotIn) use ($languageId, $deviceTypeId)  {

                                      $queryWhereNotIn->select('translate_key_id')
                                                      ->from(with((new Translate())->getTable()))
                                                      ->where('language_id', $languageId)
                                                      ->where('device_type_id', $deviceTypeId);
                                  })->whereNotIn('translate_key_id', function($queryWhereNotIn) use ($languageId, $deviceTypeId)  {

                                    $queryWhereNotIn->select('translate_key_id')
                                                    ->from(with((new Translate())->getTable()))
                                                    ->where('language_id', $languageId)
                                                    ->where('device_type_id', DEFAULT_DEVICE_TYPE_ID);
                                });
                        });
                });

            }]);

        }]);
    }


    /**
     * return all countries name for use in translate_key to get all country name by user language
     *
     * @param string|null $countryIso2
     * @return array
     */
    public function getCountryNames(?string $countryIso2 = null) : array
    {
        if(isset($countryIso2)) {
            return $this->model->whereIso2($countryIso2)->pluck('name')->toArray();
        }
        Cache::forget("allCountriesName"); //todo remove after test
       return Cache::get('allCountriesName', function() {

            $return = $this->model->all()->pluck('name')->toArray();
            Cache::forever("allCountriesName", $return);
            return  $return;
        });
    }


    /**
     * return all countries name for use in translate_key to get all country name by user language
     *
     * @return array
     */
    public function getCountryTranslateKeyIds() : array
    {
        Cache::forget("allCountryTranslateKeyIds"); //todo remove after test
        return Cache::get('allCountryTranslateKeyIds', function() {
            $return = [];
            $query = $this->model->with(['translateKey' => function($q) {
//                $q->select('key'); //todo remove foreach
            }])->get()->toArray();


            foreach( $query as $item ) {
                if(isset($item['translate_key'])) {
                    $return[] = $item['translate_key']['id'];
                }
            }

            Cache::forever("allCountryTranslateKeyIds", $return);
            return  $return;
        });
    }
	
	
	/**
	 * return country by country iso2
	 * @param $iso2
	 * @return mixed
	 */
	public function getByIso2(string $iso2)
	{
		return $this->model->whereIso2($iso2)->first();
	}

}
