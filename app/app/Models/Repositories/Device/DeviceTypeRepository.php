<?php namespace App\Models\Repositories\Device;

use App\CustomStuff\Classes\DetectDeviceInfo;
use App\Exceptions\CustomValidationException;
use App\Models\Entities\Device\DeviceType;
use App\Modles\Repositories\RepositoryBase;
use Dotenv\Exception\ValidationException;


/**
 * Class DeviceTypeRepository
 *
 * @package App\Models\Repositories\Device
 */
class DeviceTypeRepository extends RepositoryBase
{
	/**
	 * DeviceTypeRepository constructor.
	 *
	 * @param DeviceType $model
	 */
	public function __construct(DeviceType $model)
	{
		$this->model = $model;
	}
	
	
	/**
	 * @return mixed
	 * @throws CustomValidationException
	 */
	public function getDeviceTypeInfo()
	{
		if(! isset(request()->all()['device_info'])) {
			throw new ValidationException("check middleware is set!");
		}
		
		$deviceType = app(DetectDeviceInfo::class)
			->getDeviceTypeByIdFromCache(request()->all()['device_info']['device_type']['id']);
		
		if(!isset($deviceType['id'])) {
			throw new CustomValidationException();
		}
		
		return $deviceType;
	}
	///return  deviceType by name and when name is null return  default device type
	public function getByName(string  $name =EN_DEFAULT_WORD)
	{
		return $this->model->whereName($name)->first();
	}
	
	/**
	 * get device type by id and return with deviceBrand and deviceSetting
	 * @param int $deviceTypeId
	 * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
	 */
	public function getDeviceTypeWithBrandAndSetting(int $deviceTypeId)
	{
		return  $this->model->with(['deviceBrand', 'deviceSetting'])->where('id', $deviceTypeId)->first() ;
		
	}
}
