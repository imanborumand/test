<?php namespace App\Modles\Repositories;



use App\Exceptions\CustomResponseException;
use App\Exceptions\CustomQueryErrorException;
use Illuminate\Config\Repository;
use Illuminate\Container\EntryNotFoundException;


/**
 * Class RepositoryBase
 * all repositories must extend this abstract class for user default functions
 * @package App\Modles\Repositories
 */
abstract class RepositoryBase
{
	protected $model = null;

	protected $paginateNumber;

	/**
	 * get one model by Id
	 * @param int $id
	 * @return mixed
	 * @throws EntryNotFoundException
	 */
	public function getById(int $id)
	{
		$object = $this->model->find($id);

		if ($object) {
			return $object;
		}
		throw new EntryNotFoundException("Model not found");
	}

	/**
	 * get one models by relations
	 *
	 * @param array    $params
	 * @param int|null $id
	 * @param bool     $paginate
	 * @return mixed
	 * @throws EntryNotFoundException
	 */
	public function getWith(array $params, int $id = null, bool $paginate = false)
	{
		$object = $this->model->with($params);

		if($id) {
			$object = $object->where('id', $id);
		}

		if ($paginate) {
			$object = $object->paginate($this->getPaginateNumber());
		} else {
			$object = $object->first();
		}

		if ($object) {
			return $object;
		}
		throw new EntryNotFoundException("Relations not found");
	}


    /**
     * @param array $params
     * @return mixed
     * @throws CustomQueryErrorException
     */
    public function create(array $params)
    {
        $create = $this->model->create($params);
        if ($create) {
            return $create;
        }
        throw new CustomQueryErrorException();
    }

	/**
	 * @param array $params
	 * @return mixed
	 */
	public function firstOrCreate(array $params)
	{
		return $this->model->firstOrCreate($params);
	}


	/**
	 * @param array $condition
	 * @param array $params
	 * @return mixed
	 * @throws CustomResponseException
	 */
	public function updateOrCreate(array $condition, array $params)
	{
		$updateOrCreate = $this->model->updateOrCreate($condition, $params);
		if (! $updateOrCreate) {
			$message = config('default_messages.response.try_again');
			throw new CustomResponseException($message,409, 4009);
		}
		return $updateOrCreate;
	}
	
	
	/**
	 * get paginate_number from config
	 * return 20 as paginate_number
	 * @return Repository|mixed
	 *
	 */
	public function getPaginateNumber()
	{
		return $this->paginateNumber = config('general.paginate_number');
	}
}
