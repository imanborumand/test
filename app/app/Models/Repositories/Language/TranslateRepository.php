<?php namespace App\Models\Repositories\Language;



use App\CustomStuff\Classes\Imports\TransLateImport;
use App\Exceptions\CustomQueryErrorException;
use App\Exceptions\CustomResponseException;
use App\Models\Entities\Language\Language;
use App\Models\Entities\Language\Translate;
use App\Models\Repositories\Country\CountryRepository;
use App\Models\Repositories\Device\DeviceTypeRepository;
use App\Modles\Repositories\RepositoryBase;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Facades\Excel;

class TranslateRepository extends RepositoryBase
{

	protected $deviceTypeRepository;
	protected $translateKeyRepository;


	public function __construct( Translate $languages)
	{
	    $this->model = $languages;
	    $this->deviceTypeRepository = app(DeviceTypeRepository::class);
		$this->translateKeyRepository = app(TranslateKeyRepository::class);
	}


	/**
	 * insert list of translates
	 *
	 * @param array $translatesList
	 * @return bool
	 * @throws CustomResponseException
	 */
	public function insert( array $translatesList) : bool
	{
		$store = $this->model->insert($translatesList);
		if (! $store) {
			$message = config('default_messages.response.try_again');
			throw new CustomResponseException($message,409, 4009);
		}
		return $store;
	}


    /**
     * count of key for current language
     *
     * @param int   $languageId
     * @param array $translatesKeyIds
     * @return object
     * @throws CustomQueryErrorException
     */
	public function getTranslateByLanguageIdWhereKeysIn(int $languageId, array $translatesKeyIds) : object
	{
		$query =  $this->model->where(function($query) use ($languageId) {
		    $query->where('language_id', $languageId);
        })->whereIn('translate_key_id', $translatesKeyIds);

		if(! $query) {
		    throw new CustomQueryErrorException();
        }
		return  $query;
	}



	/**
	 *  return all translate values by Keys
	 * if keys equal [] return all translate for selected language
	 * the end of function merge translates to language collect
	 *
	 * @param Language $language
	 * @param int      $deviceTypeId
	 * @param int      $defaultDeviceTypeId
	 * @param array    $translatesKeys
	 * @return Language
	 */
	public function getTranslatesByKeys(Language $language, int $deviceTypeId, int  $defaultDeviceTypeId, array $translatesKeys)
	{
		$translatesKeyIds = [];
		$languageId = $language->id;
		
        $countryRepository = app(CountryRepository::class);
        $countriesTranslateKeyIds = $countryRepository->getCountryTranslateKeyIds();

		if(isset($translatesKeys) && count($translatesKeys) > 0) {
			$translateKeyRepository = app( TranslateKeyRepository::class );
			$translatesKeyArray     = $translateKeyRepository->getTranslatesKeyByName( $translatesKeys );
			$translatesKeyIds       = array_keys( $translatesKeyArray );
		}
		
		$translates = $this->getTranslateQuery($languageId, $translatesKeyIds, $deviceTypeId, $countriesTranslateKeyIds);
		
		$language->setRelation('translates', $translates); // set manual relation for language
		return $language;
	}
	
	
	/**
	 * get all translate whit countries key
	 * @param Language $language
	 * @param int      $deviceTypeId
	 * @param int      $defaultDeviceTypeId
	 * @param array    $translatesKeys
	 * @return Language
	 */
	public function getTranslatesByCountries(Language $language, int $deviceTypeId, int $defaultDeviceTypeId, array $translatesKeys)
	{
		$translatesKeyIds = [];
		$languageId = $language->id;
		if(isset($translatesKeys) && count($translatesKeys) > 0) {
			$translateKeyRepository = app( TranslateKeyRepository::class );
			$translatesKeyArray     = $translateKeyRepository->getTranslatesKeyByName( $translatesKeys );
			$translatesKeyIds       = array_keys( $translatesKeyArray );
		}
		
		$translates = $this->getTranslateQuery($languageId, $translatesKeyIds, $deviceTypeId);
		
		$language->setRelation('translates', $translates); // set manual relation for language
		return $language;
	}
	
	
	/**
	 * query for get translates
	 * @param int   $languageId
	 * @param array $translatesKeyIds
	 * @param int   $deviceTypeId
	 * @param array $countriesTranslateKeyIds
	 * @return Builder[]|Collection
	 */
	private function getTranslateQuery(int $languageId, array $translatesKeyIds, int $deviceTypeId, array $countriesTranslateKeyIds = [])
	{
		$translateTableName = (new Translate())->getTable();
		return  $this->model
			->with('translateKey' )
			->whereHas('translateKey', function($q)use ($countriesTranslateKeyIds)  {
				return $q->whereNotIn('id', $countriesTranslateKeyIds);
			})->where(function($query) use($languageId, $translatesKeyIds, $deviceTypeId, $translateTableName, $countriesTranslateKeyIds) {
				
				$query->where('language_id', $languageId)->where('device_type_id', $deviceTypeId);
				if(count($translatesKeyIds) != 0) {
					$query->whereIn('translate_key_id', $translatesKeyIds);
				}
			})->orWhere(function($q) use($languageId, $translatesKeyIds, $deviceTypeId, $translateTableName, $countriesTranslateKeyIds) {
				
				$q->whereIn('id', function($query)  use($languageId, $translatesKeyIds, $deviceTypeId, $translateTableName, $countriesTranslateKeyIds) {
					
					$query->select('id')->from(with($translateTableName))
						  ->where('language_id', DEFAULT_LANGUAGE_ID)
						  ->where('device_type_id', DEFAULT_DEVICE_TYPE_ID)
						  ->whereNotIn('translate_key_id', function($queryWhereNotIn) use($languageId, $translatesKeyIds, $deviceTypeId, $translateTableName, $countriesTranslateKeyIds) {
						
							  $queryWhereNotIn->select('translate_key_id')
											  ->from(with($translateTableName))
						
											  ->where('language_id', $languageId)
											  ->where('device_type_id', $deviceTypeId);
						  })->whereNotIn('translate_key_id', $countriesTranslateKeyIds);
					
					if(count($translatesKeyIds) != 0) {
						$query->whereIn('translate_key_id', $translatesKeyIds);
					}
				});
			})->orWhere(function($q) use($languageId, $translatesKeyIds, $deviceTypeId, $translateTableName, $countriesTranslateKeyIds) {
				//todo refactor this query
				$q->whereIn('id', function($query)  use($languageId, $translatesKeyIds, $deviceTypeId, $translateTableName, $countriesTranslateKeyIds) {
					
					$query->select('id')->from(with($translateTableName))
						  ->where('language_id', $languageId)
						  ->where('device_type_id', DEFAULT_DEVICE_TYPE_ID)
						  ->whereNotIn('translate_key_id', function($queryWhereNotIn) use($languageId, $translatesKeyIds, $deviceTypeId, $translateTableName, $countriesTranslateKeyIds) {
						
							  $queryWhereNotIn->select('translate_key_id')
											  ->from(with($translateTableName))
						
											  ->where('language_id', $languageId)
											  ->where('device_type_id', $deviceTypeId);
						  })->whereNotIn('translate_key_id', $countriesTranslateKeyIds);
					
					if(count($translatesKeyIds) != 0) {
						$query->whereIn('translate_key_id', $translatesKeyIds);
					}
				});
			})->get()->makeHidden(['language_id', 'translate_key_id']);
	}

	//use Excel package for import data in database
	public function importExcel(object $excelFile)
	{
		Excel::import(new TransLateImport, $excelFile);
	}


}
