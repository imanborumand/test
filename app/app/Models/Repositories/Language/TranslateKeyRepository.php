<?php namespace App\Models\Repositories\Language;


use App\Exceptions\CustomQueryErrorException;
use App\Models\Entities\Language\TranslateKey;
use App\Modles\Repositories\RepositoryBase;

/**
 * Class TranslateKeyRepository
 *
 * @package App\Models\Repositories\Language
 */
class TranslateKeyRepository extends RepositoryBase
{
	/**
	 * TranslateKeyRepository constructor.
	 *
	 * @param TranslateKey $model
	 */
	public function __construct( TranslateKey $model) {
		$this->model = $model;
	}
	
	
	/**
	 * @param array $keys
	 * @return mixed
	 */
	public function getTranslatesKeyByName( array $keys)
	{
		return $this->model->whereIn('key',$keys)->pluck('key','id')->toArray();
	}
	
	
	/**
	 * @param array $translatesKeys
	 * @throws CustomQueryErrorException
	 */
	public function insertNewKeys( array $translatesKeys)
	{
		$newTransLateKey =[];
		foreach($translatesKeys as $key) {
			$newTransLateKey[]= [
				'key' =>$key
			];
		}
		$store = $this->model->insert($newTransLateKey);
		if (! $store) {
			$message = config('default_messages.response.item_created_error');
			throw new CustomQueryErrorException($message);
		}
	}
	
}
