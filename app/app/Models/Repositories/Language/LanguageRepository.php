<?php namespace App\Models\Repositories\Language;

use App\Models\Entities\Language\Language;
use App\Models\Entities\Language\Translate;
use App\Models\Repositories\Device\DeviceTypeRepository;
use App\Modles\Repositories\RepositoryBase;
use Illuminate\Container\EntryNotFoundException;
use Illuminate\Contracts\Foundation\Application;

/**
 * Class LanguageRepository
 *
 * @package App\Models\Repositories\Language
 */
class LanguageRepository extends RepositoryBase
{

	/**
	 * @var Application|mixed
	 */
	protected $deviceTypeRepository;
	protected $translateRepository;
	/**
	 * LanguageRepository constructor.
	 *
	 * @param Language $model
	 */
	public function __construct(Language $model)
	{
		$this->model = $model;
		$this->deviceTypeRepository = app(DeviceTypeRepository::class);
		$this->translateRepository = app(TranslateRepository::class);
	}

	/**
	 * return list of languages
	 *
	 * @return mixed
	 */
	public function allLanguages() : object
	{
		return $this->model->get();
	}

	/**
	 * store new language
	 * @param array $params
	 * @return mixed
	 */
	public function store(array $params)
	{
		return $this->model->firstOrCreate($params);
	}
	
	
	/**
	 * find language by iso2
     * @param $iso2
     * @return mixed
     */
    public function getByIso2(string  $iso2)
    {
        return $this->model->byIso2($iso2)->first();
    }
	
	
	/**
	 * return translates by language id
	 * don`t use get() when call this method in your services
	 * you can use toArray() to change values to array
	 *
	 * @param int   $languageId
	 * @param int   $deviceTypeId
	 * @param int   $defaultDeviceTypeId
	 * @param array $translatesKeys
	 * @return Language
	 * @throws EntryNotFoundException
	 */
	public function getLanguageByIdWithTranslates(
	    int $languageId,
        int $deviceTypeId = DEFAULT_DEVICE_TYPE_ID,
        int $defaultDeviceTypeId = DEFAULT_DEVICE_TYPE_ID,
        array $translatesKeys = []
    ) {
        return $this->translateRepository->getTranslatesByKeys($this->getById($languageId), $deviceTypeId, $defaultDeviceTypeId, $translatesKeys);
	}
	
	
	
	/**
	 * return all countries
	 * @param int   $languageId
	 * @param int   $deviceTypeId
	 * @param int   $defaultDeviceTypeId
	 * @param array $translatesKeys
	 * @return mixed
	 * @throws EntryNotFoundException
	 */
	public function getAllTranslatesWithCountries(
		int $languageId,
		int $deviceTypeId = DEFAULT_DEVICE_TYPE_ID,
		int $defaultDeviceTypeId = DEFAULT_DEVICE_TYPE_ID,
		array $translatesKeys = [])
	{
		return $this->translateRepository->getTranslatesByCountries($this->getById($languageId), $deviceTypeId, $defaultDeviceTypeId, $translatesKeys);
	}

    /**
     * return translates by language iso2
     * don`t use get() when call this method in your services
     * you can use toArray() to change values to array
     * @param string $languageIso2
     * @param int    $deviceTypeId
     * @param int    $defaultDeviceTypeId
     * @param array  $translatesKeys
     * @return Language
     */
    public function getLanguageByIso2WithTranslates(
        string $languageIso2,
        int $deviceTypeId = DEFAULT_DEVICE_TYPE_ID,
        int $defaultDeviceTypeId = DEFAULT_DEVICE_TYPE_ID,
        array $translatesKeys = []
    ){

        return $this->translateRepository->getTranslatesByKeys($this->getByIso2($languageIso2), $deviceTypeId, $defaultDeviceTypeId, $translatesKeys);
    }
	
	/**
	 * return translates whereIn key by languageId
	 *
	 * @param int   $languageId
	 * @param array $translatesKeyIds
	 * @return object
	 */
	public function translateWhereKeyInByLanguageId(int $languageId, array $translatesKeyIds): object
	{
		return  $this->model->where('id', $languageId)->with(['translates' => function($q) use ($translatesKeyIds) {
			$q->whereIn('translate_key_id', $translatesKeyIds);
		}]);
	}
}
