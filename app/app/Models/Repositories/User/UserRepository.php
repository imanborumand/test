<?php namespace App\Models\Repositories\User;

use App\Exceptions\CustomNotFoundException;
use App\Exceptions\CustomQueryErrorException;
use App\Models\Entities\User\User;
use App\Modles\Repositories\RepositoryBase;
use Illuminate\Database\Eloquent\Builder;


class UserRepository extends RepositoryBase
{
	public function __construct(User $model)
	{
		$this->model = $model;
	}


    /**
     * retrieve all special contacts type for user by contactId
     *
     * @param int    $userId
     * @param string $contactId
     * @return Builder
     * @throws CustomNotFoundException
     */
    public function getUserWithContactUserId(int $userId, string $contactId)
    {
    	
        $return = $this->model->with(['contactUser' => function($query) use($contactId) {
                                    $query->where('contact_id', $contactId);
                                }])->where('id', $userId);

        if($return->count() == 0) {
            throw new CustomNotFoundException();
        }

        return  $return;
    }
    
    /**
     * retrieve all special contacts type for  multi users by contactId and userIds
     *
     * @param array   $userIds
     * @param string $contactId
     * @return Builder
     * @throws CustomNotFoundException
     */
    public function getUserWithContactUserIds(array $userIds, string $contactId)
    {
        $return = $this->model->with(['contactUser' => function($query) use($contactId) {
                                    $query->where('contact_id', $contactId);
                                }])->whereIn('id', $userIds);
        if($return->count() == 0) {
            throw new CustomNotFoundException();
        }
        return  $return;
    }


    public function update(User $user,array $params)
	{
		if(! $user->update($params)) {
			$message = config('default_messages.response.item_updated_error');
			throw new CustomQueryErrorException($message);
		}
		return $user;
	}


}
