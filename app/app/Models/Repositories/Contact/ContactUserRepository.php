<?php namespace App\Models\Repositories\Contact;

use App\Exceptions\CustomNotFoundException;
use App\Exceptions\CustomQueryErrorException;
use App\Exceptions\CustomResponseException;
use App\Exceptions\CustomValidationException;
use App\Models\Entities\Contact\ContactUser;
use App\Models\Entities\User\User;
use App\Modles\Repositories\RepositoryBase;

/**
 * Class ContactUserRepository
 *
 * @package App\Models\Repositories\Contact
 */
class ContactUserRepository extends RepositoryBase
{
	/**
	 * ContactUserRepository constructor.
	 *
	 * @param ContactUser $model
	 */
	public function __construct(ContactUser $model)
	{
		$this->model = $model;
	}
	
	
	/**
	 * store new contact user in database if not exist
	 *
	 * @param array $params
	 * @return mixed
	 * @throws CustomQueryErrorException
	 */
	public function store(array $params)
	{
		$contactUser = $this->model->byUserId($params['user_id'])
								   ->byValue($params['value'])
								   ->byContactId($params['contact_id'])
								   ->first();
		if($contactUser) {
			return $contactUser;
		}
		return $this->create($params);
		
	}


    /**
	 * update new contact user
 
	 * @param array $params
     * @param int   $contactUserId
     * @return mixed
     * @throws CustomNotFoundException
     */
	public function update(array $params, int $contactUserId)
	{
		$contactUer = $this->model->byUserId(auth()->user()->id)->whereId($contactUserId)->first();
		if(!$contactUer){
			throw new CustomNotFoundException();
		}
		if(isset($params['status'])){
			$params['status'] = (int)$params['status'];
		}
		$contactUer->update($params);
		return $contactUer;
	}


    /**
	 * return first contact_user by contactId and value
     * return user by contactId and contactValue form contactUser table
     * @param string $contactId
     * @param string $contactValue
     * @return mixed
     */
    public function getUserByContactUser(string $contactId, string $contactValue)
    {
        return $this->model->with('user')
						   ->byContactId($contactId)
						   ->byValue($contactValue)
						   ->first();
    }
    
	/**
	 * return first mobile that saved in contact_user table
	 *
	 * @param User $user
	 * @return mixed
	 * @throws CustomNotFoundException
	 */
    public function getUserMobile(User $user)
	{
		$mobileKey = config('general.contact_type_keys.mobile');
		$contact = app(ContactRepository::class)->getByKey($mobileKey);
		$contactUser = $this->model->byContactId($contact->id)->byUserId($user->id)->orderBy('id', 'asc')->first();
		if(! $contactUser) {
			throw  new CustomNotFoundException();
		}
		return $contactUser;
	}
	
	
	/**
	 *
	 * delete requested mobile
	 * cant not delete first mobile that user register with it
	 * @param object $user
	 * @param int    $contactUserId
	 * @return mixed
	 * @throws CustomNotFoundException
	 * @throws CustomValidationException
	 */
	public function delete(object $user, int $contactUserId)
	{
		$firstMobile = $this->getUserMobile($user);
		if($firstMobile->id == $contactUserId) {
			throw new CustomValidationException('first mobile can not be delete');
		}
		$contactUser = $this->model->find($contactUserId);
		if(! $contactUser){
			throw new CustomNotFoundException();
		}
		$result = $contactUser->delete();
		return $result;
	}
	
	
	/**
	 * return contact_user by user_id and contact_user.value
	 *
	 * @param User   $user
	 * @param string $value
	 * @return ContactUser
	 * @throws CustomNotFoundException
	 */
	public function getContactUserByValue(User $user, string $value) : ContactUser
	{
		$query = $this->model->where('user_id', $user->id)->byValue($value)->first();
		if (! $query) {
			throw new CustomNotFoundException();
		}
		return $query;
	}
	
	
	/**
	 * retrieve user collection by contact info
	 * @param string $value
	 * @param int    $contactId
	 * @return User
	 * @throws CustomValidationException
	 */
	public function retrieveUserByContactUser(string $value, int $contactId) : User
	{
		$query = $this->model->with('user')
							 ->whereHas('user')
							 ->where('contact_id', $contactId)
							 ->where('value', $value);
	
		if($query->count() == 0) {
			throw new CustomValidationException("user not found by contact!");
		}
		
		return $query->first()->user;
	}
	
	/**
	 * create new contact by type FCM when user login if token not exists in DB
	 *
	 * @param array $params
	 * @param User  $user
	 * @param int   $contactId
	 * @return mixed
	 * @throws CustomResponseException
	 */
	public function storeFirebaseTokenInLogin( array $params, User $user, int $contactId)
	{
		$contactUser = $this->model->where('user_id' ,$user->id)
								   ->where('value', $params['firebase_token'])
								   ->where('contact_id', $contactId)
								   ->count();
		
		if($contactUser) {
			return $contactUser;
		}
		return $this->updateOrCreate([
				 'user_id' => $user->id,
				 'value' => $params['firebase_token']
			],
		 ['contact_id' => $contactId]);
	}
}
