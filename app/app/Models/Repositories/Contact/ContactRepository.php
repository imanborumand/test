<?php namespace App\Models\Repositories\Contact;

use App\Exceptions\CustomQueryErrorException;
use App\Models\Entities\Contact\Contact;
use App\Modles\Repositories\RepositoryBase;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;


/**
 * Class ContactRepository
 *
 * @package App\Models\Repositories\Contact
 */
class ContactRepository extends RepositoryBase
{

	/**
	 * ContactRepository constructor.
	 *
	 * @param Contact $model
	 */
	public function __construct(Contact $model)
	{
		$this->model = $model;
	}


	/**
	 * return list of contact
	 * @return mixed
	 */
	public function all()
	{
		return $this->model->latest()->get();
	}

	/**
	 * store new contact type if not exist
	 
	 * @param array $params
	 * @return mixed
	 */
	public function store(array $params)
	{
		return $this->firstOrCreate($params);
	}


	/**
	 * update contact
	 * @param array $params
	 * @param int   $contactId
	 * @return mixed
	 * @throws CustomQueryErrorException
	 */
	public function update(array $params, int $contactId)
	{
		$contact = $this->model->findOrFail($contactId);
		if(! $contact->update($params)) {
			$message = config('default_messages.response.item_updated_error');
			throw new CustomQueryErrorException($message);
		}
		return $contact;
	}


	/**
	 * find and delete contact
	 * @param int $contactId
	 * @return mixed
	 */
	public function delete(int $contactId)
	{
		return $this->model->findOrFail($contactId)->delete();
	}


    /**
     * return contact by key
     * @param $key
     * @return bool
     */
    public function getByKey(string $key)
	{
	    $return = $this->model->getByKey($key)->first();
	    if(! $return) {
	        return false;
        }
		return $return;
	}
	
	
	/**
	 * return all contacts of specific user that have contactUser
	 *
	 * @param $userId
	 * @return Builder[]|Collection
	 * @throws CustomQueryErrorException
	 */
    public function allWithContactUser($userId)
    {
        $query = $this->model->whereHas('contactUser' , function($query) use($userId){
            $query->byUserId($userId);
        })->with(['contactUser'=> function($q) use($userId) {
			$q->byUserId($userId)->orderBy('id', 'asc');
		}])->get();

        if(! $query) {
            throw new CustomQueryErrorException();
        }
        return $query;
    }

}
