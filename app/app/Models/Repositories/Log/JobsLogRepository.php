<?php namespace App\Models\Repositories\Log;


use App\Models\Entities\Log\JobsLogModel;
use App\Modles\Repositories\RepositoryBase;

class JobsLogRepository extends RepositoryBase
{
	protected $paginateNumber = 50;


	public function __construct(JobsLogModel $model)
	{
		$this->model = $model;
	}
	
	
	/**
	 * @return mixed
	 */
	public function getUserInfoToWrapperLog()
	{
		return $this->model->latest()->where('resolveName', 'SendUserInfoToWrapperJob')->paginate($this->paginateNumber);
	}

	
	public function deleteAllNotify()
	{
		
		return $this->model->truncate();
	}
}
