<?php namespace App\Models\Repositories\Log;


use App\Models\Entities\Log\LogRequestResponseModel;
use App\Modles\Repositories\RepositoryBase;

class LogRequestResponseRepository extends RepositoryBase
{
	protected $paginateNumber = 40;


	public function __construct(LogRequestResponseModel $model)
	{
		$this->model = $model;
	}
	
	
	/**
	 * return log list of response and request
	 * @return mixed
	 */
	public function all()
	{
		return $this->model
			->latest()
			->paginate($this->paginateNumber);
	}
	
	
	/**
	 * delete all response and request logs
	 * @return mixed
	 */
	public function deleteAllResponseRequestLog()
	{
		return $this->model->truncate();
	}
}
