<?php namespace App\Models\Repositories\Notify;

use App\Exceptions\CustomNotFoundException;
use App\Exceptions\CustomQueryErrorException;
use App\Exceptions\CustomResponseException;
use App\Models\Entities\Log\JobsLogModel;
use App\Models\Entities\User\User;
use App\Modles\Repositories\RepositoryBase;
use Illuminate\Container\EntryNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class NotifyRepository
 *
 * @package App\Models\Repositories\Notify
 */
class NotifyRepository extends RepositoryBase
{
	
	/*
	 * keys that show in response
	 */
	public $keysShowInResponseLog = [
		'_id',
		'sendParams.notificationData.notification',
		'created_at',
		'seen'
	];
	
	/**
	 * NotifyRepository constructor.
	 *
	 * @param JobsLogModel $model
	 */
	public function __construct(JobsLogModel $model)
	{
		$this->model = $model;
		$this->paginateNumber = $this->getPaginateNumber();
	}
	
	
	/**
	 * read data from mongo and get job that run for send notification
	 *
	 * @return mixed
	 */
	public function all()
	{
		$paginator = $this->model->latest()
			->where('resolveName', '=', 'SendNotificationJob')
			->paginate($this->paginateNumber);
		
		$data = $paginator->makeHidden(['jobPayload']);
		$paginator->data = $data;
		return $paginator;
	}
	
	
	/**
	 * return notify user by user_id and paginate
	 *
	 * @param int $userId
	 * @return mixed
	 */
	public function getNotifyByUserId( int $userId)
	{
		return $this->model->where('resolveName','=','SendNotificationJob')
						   ->where('user.id', $userId)
						   ->where('notifyResponse.success', '!=', 0) // if response form firebase != 0 message was sended
						   ->select($this->keysShowInResponseLog)
				 	       ->latest()
						   ->paginate($this->paginateNumber);
	}
	
	
	/**
	 *  get one notify with notify_id
	 *  if you can select special keys use in $keys
	 *
	 * @param string $notifyId
	 * @param array  $keys
	 * @return JobsLogModel
	 * @throws CustomNotFoundException
	 */
	public function getNotifyByIdWithSelectKeys(string $notifyId, array $keys = []) : JobsLogModel
	{
		if(count($keys) > 0) {
			$this->keysShowInResponseLog = $keys;
		}
		
		$object = $this->model->where('_id', $notifyId)
							  ->select($this->keysShowInResponseLog)
							  ->first();
		
		if ($object) {
			return $object;
		}
		throw new CustomNotFoundException("model not found" );
	}
	
	
	/**
	 * delete notify
	 * @param JobsLogModel $notify
	 * @return bool
	 * @throws CustomQueryErrorException
	 */
	public function deleteNotify(JobsLogModel $notify) : bool
	{
		if($notify->delete()) {
			return true;
		}
		throw new CustomQueryErrorException();
	}
	
	
	/**
	 * update notify by notify model
	 *
	 * @param JobsLogModel $notify
	 * @param array        $keyValues
	 * @return bool
	 * @throws CustomQueryErrorException
	 */
	public function updateNotify(JobsLogModel $notify, $keyValues = []) : bool
	{
		$update = $notify->update($keyValues);
		if($update) {
			return true;
		}
		throw new CustomQueryErrorException();
	}
	
	
	/**
	 * return number of notify (message) that user not seen
	 * @param int $userId
	 * @return mixed
	 */
	public function getUserNotSeenNotifyCount(int $userId) : int
	{
		return $this->model->where('user.id', $userId)
						   ->where('resolveName','=','SendNotificationJob')
						   ->where('notifyResponse.success', '!=', 0) // if response form firebase != 0 message was sended
						   ->where('seen', 0)
						   ->count();
		
	}
	
	
	/**
	 * return number of notify that send to user form firebase
	 * @param int $userId
	 * @return mixed
	 */
	public function getUserSuccessNotifyCount(int $userId) : int
	{
		return $this->model->where('user.id', $userId)
						   ->where('resolveName','=','SendNotificationJob')
						   ->where('notifyResponse.success', '!=', 0) // if response form firebase != 0 message was sended
						   ->count();
	}
	
	
	/**
	 * return count aff all user notify success and failed
	 *
	 * @param int $userId
	 * @return mixed
	 */
	public function getUserTotalNotifyCount(int $userId) : int
	{
		return $this->model->where('resolveName','=','SendNotificationJob')
						   ->where('user.id', $userId)
						   ->where('notifyResponse.success', '!=', 0) // if response form firebase != 0 message was sended
						   ->count();
	}
	
	
	/**
	 * update all seen notify of one user to 1
	 * when call notify/users/{user_id} use this method
	 * and update seen of all notify to 1
	 *
	 * @param int $userId
	 * @return
	 */
	public function updateSeenAllNotify(int $userId)
	{
		return  $this->model
			->where('user.id', $userId)
			->where('resolveName','=','SendNotificationJob')
			->where('notifyResponse.success', '!=', 0) // if response form firebase != 0 message was sended
			->update(['seen' => 1]);
	}
	
	
	/**
	 * delete all notification log
	 * @return mixed
	 */
	public function deleteAllNotify()
	{
		return $this->model->where('resolveName','=','SendNotificationJob')->delete();
	}
}
