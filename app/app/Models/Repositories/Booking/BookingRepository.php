<?php namespace App\Models\Repositories\Booking;

use App\Exceptions\CustomResponseException;
use App\Models\Entities\Booking\Booking;
use App\Models\Entities\User\User;
use App\Models\Repositories\Country\CountryRepository;
use App\Modles\Repositories\RepositoryBase;
use App\Exceptions\CustomBadRequestException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class BookingRepository
 *
 * @package App\Models\Repositories\User
 */
class BookingRepository extends RepositoryBase
{
	/**
	 * BookingRepository constructor.
	 *
	 * @param Booking $model
	 */
	public function __construct(Booking $model)
	{
		$this->model = $model;
	}


    /**
	 * send Request Wrapper to get booking ifo then store booking data in turpal database
     * @param User  $user
     * @param array $params
     * @return array
     * @throws CustomBadRequestException
     * @throws CustomResponseException
     */
	public function addBooking(User $user, array $params)
	{
		$login = $this->sendRequestToWrapperLogin($params);
		if(! isset($login->status)) {
			$message = config('default_messages.response.try_again');
			throw new CustomBadRequestException($message);
		}

		if($login->status === true) {
			$this->storeBooking($user, $login->data->booking);
			return ((array) $login->data);
		}
		throw new NotFoundHttpException();
	}

	/**
	 * @param array $params
	 * @return object
	 * @throws CustomResponseException
	 */
	private function sendRequestToWrapperLogin( array  $params) : object
	{
		return json_decode(doRequest(getWrapperConfig()['baseUrl'] . 'login', "POST", $params));
	}




    /**
     * //validate ds booking data and store it to user booking table
     * @param User   $user
     * @param object $booking
     */
	public function storeBooking(User $user, object $booking)
	{
		$country = app(CountryRepository::class)->getByIso2($booking->source_country);
		//TODO add validate
		$this->model->updateOrCreate(
			['user_id'=> $user->id, 'ds_booking_id'=> $booking->id],
			['country_id'=> $country->id ?? 0]
		 );

	}
	/**
	 * return booking by booking ds id
	 *
	 * @param string $bookingId
	 * @return mixed
	 */
	
	public function getBookingByDsId(string $bookingId)
	{
		return $this->model->byDsId($bookingId)->get();
	}
	
	
	/**
	 * return last booking of user that inserted in database
	 * @param int $userId
	 * @return mixed
	 */
	public function getLastBookingOfUser(int $userId)
	{
		$query = $this->model->where('user_id', $userId)->first();
		if(! $query) {
			return null;
		}
		return $query;
	}
}
