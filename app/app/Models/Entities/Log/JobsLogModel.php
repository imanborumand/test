<?php namespace App\Models\Entities\Log;


use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class JobsLogModel extends Model
{
	use SoftDeletes;
	
	protected $collection = 'jobs';
	
	protected $primaryKey = '_id';
	
	protected $connection = 'mongodb';
	
	protected $guarded = [];
	
	protected $dates = ['deleted_at'];
}
