<?php namespace App\Models\Entities\Log;


use Jenssegers\Mongodb\Eloquent\Model;

class LogRequestResponseModel extends Model
{
    protected $collection = 'request_response';
    protected $primaryKey = '_id';
    protected $connection = 'mongodb';
    protected $guarded = [];
}
