<?php namespace App\Models\Entities\User;


use App\Models\Entities\Booking\Booking;
use App\Models\Entities\Contact\ContactUser;
use App\Models\Entities\Country\Nationality;
use App\Models\Entities\Language\Language;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
	use Notifiable,
		HasApiTokens;


	protected $table = 'users';
	protected $fillable = [
        //| id          | bigint(20) unsigned | NO   | PRI | NULL    | auto_increment |
        'language_id',   //| bigint(20) unsigned | NO   | MUL | NULL    |                |
        'first_name',    //| varchar(80)         | YES  |     | NULL    |                |
        'middle_name',   //| varchar(80)         | YES  |     | NULL    |                |
        'last_name',     //| varchar(80)         | YES  |     | NULL    |                |
        'ds_name',       //| varchar(100)        | YES  |     | NULL    |                |
        'avatar',        //| varchar(255)        | YES  |     | NULL    |                |
        'nationality_id',//| bigint(20) unsigned | YES  | MUL | NULL    |    			/
		'birth_date'
		//| created_at  | timestamp           | YES  |     | NULL    |                |
        //| updated_at  | timestamp           | YES  |     | NULL    |                |
	];

	protected $hidden = ['ds_name'];
	
	protected $with = ['nationality'];
	
	
	/////////////relations
	public function language()
	{
		return $this->belongsTo(Language::class);
	}

    public function contactUser()
    {
        return $this->hasMany(ContactUser::class);
    }
    
    public function nationality()
    {
        return $this->hasOne(Nationality::class,'id','nationality_id');
    }
    
    public function bookings()
	{
		return $this->hasMany(Booking::class);
	}

	/////////////accessors & mutator
	public function getAvatarAttribute($value)
	{
		//creates a symlink from public/storage to storage/app/public
		return  (isset($value) && $value != null) ? url(STORAGE_AVATAR_PATH).'/'.$value : null;
	}

	/////////////scope
	/**
	 * return first of user_contact by contactId
	 * @param $query
	 * @param $contactId
	 * @return mixed
	 */
	public function scopeWithFirstContact( $query, $contactId)
	{
		return $query->with(['contactUser' => function($q) use ($contactId) {
			$q->where('contact_id', $contactId);
		}])->first();
	}
}
