<?php namespace App\Models\Entities\Booking;

use App\Models\Entities\Contact\Contact;
use App\Models\Entities\ModelBase;

class Booking extends ModelBase
{
	/**
	 * @var string
	 */
	protected $table = 'bookings';
	/**
	 * @var bool
	 */
	public $timestamps = false;
	
	/**
	 * @var array
	 */
	protected $fillable = [
		  //id             | bigint(20) unsigned                                              | NO   | PRI | NULL    | auto_increment |
		 'user_id',       // | bigint(20) unsigned                                              | NO   | MUL | NULL    |              |
		 'ds_booking_id', //| varchar(255)                                                     | NO   |     | NULL    |              |
		 'country_id',    //| bigint(20) unsigned                                             | NO   | MUL | NULL    |               |
		 //created_at     | timestamp                                                         | YES  |     | NULL    |                |
		 //updated_at    | timestamp                                                         | YES  |     | NULL    |               |
	];
	
	/////////relation
	public function user()
	{
		return $this->belongsTo(User::class);
		
	}
	
	public function country()
	{
		return $this->belongsTo(Contact::class);
	}
	
	public function scopeByDsId($q, $bookingId)
	{
		return $q->where('ds_booking_id',$bookingId);
	}
	
}
