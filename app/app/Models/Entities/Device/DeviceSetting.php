<?php namespace App\Models\Entities\Device;


use App\Models\Entities\Language\Language;
use App\Models\Entities\ModelBase;

class DeviceSetting extends ModelBase
{
    protected $table    = 'device_settings';
    public $timestamps  = false;
    protected $fillable = [
        //| id          | bigint(20) unsigned | NO   | PRI | NULL    | auto_increment |
        'device_type_id',    //| bigint(20) unsigned | NO   | MUL | NULL    |                |
        'language_id',  //| bigint(20) unsigned | NO   | MUL | NULL    |                |
        'font_size',    //| varchar(255)        | YES  |     | NULL    |                |
        'font_family',  //| varchar(255)        | YES  |     | NULL    |                |
    ];

    public function deviceType()
    {
        return $this->belongsTo( DeviceType::class);
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }
}
