<?php namespace App\Models\Entities\Device;


use App\Models\Entities\ModelBase;

class DeviceType extends ModelBase
{
    protected $table    = 'device_types';
    public $timestamps  = false;
    protected $fillable = [
            //| id              | bigint(20) unsigned                                                                                 | NO   | PRI | NULL    | auto_increment |
            'device_brand_id', //| bigint(20) unsigned                                                                                 | NO   | MUL | NULL    |                |
            'name',            //| varchar(255)                                                                                        | NO   | MUL | NULL    |                |
            'os',              //| enum('android','iphone','linux','blackBerry','ubuntu','windows','windowsPhone','mac','iPod','iPad') | NO   | MUL | NULL    |                |
    ];


    public function deviceBrand()
    {
        return $this->belongsTo(DeviceBrand::class);
    }

    public function deviceSetting()
    {
        return $this->hasOne(DeviceSetting::class);
    }
}
