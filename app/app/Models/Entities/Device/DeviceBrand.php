<?php namespace App\Models\Entities\Device;


use App\Models\Entities\ModelBase;

class DeviceBrand extends ModelBase
{
    protected $table    = 'device_brands';
    public $timestamps  = false;
    protected $fillable = [
        //| id    | bigint(20) unsigned | NO   | PRI | NULL    | auto_increment |
        'name'  //| varchar(255)        | NO   | MUL | NULL    |                |
    ];


    public function deviceTypes()
    {
        return $this->hasMany( DeviceType::class);
    }

}
