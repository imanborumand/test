<?php namespace App\Models\Entities\Contact;

use App\Models\Entities\ModelBase;
use App\Models\Entities\User\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactUser extends ModelBase
{
	use SoftDeletes;
	
	protected $table = 'contact_user';

	protected $fillable = [
        //| id           | bigint(20) unsigned | NO   | PRI | NULL    | auto_increment |
        'user_id',      //| bigint(20) unsigned | NO   | MUL | NULL    |                |
        'contact_id',   //| bigint(20) unsigned | NO   | MUL | NULL    |                |
        'value',        //| varchar(255)        | YES  |     | NULL    |                |
        'status',       //| tinyint(1)          | NO   |     | 1       |                |
        'confirmed_at', //| timestamp           | YES  |     | NULL    |                |
        //| created_at   | timestamp           | YES  |     | NULL    |                |
        //| updated_at   | timestamp           | YES  |     | NULL    |                |

	];


	protected $hidden = [
		'created_at',
		'updated_at',
        'user_id'
	];

	/////////////relation
	/**
	 * @return BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * @return BelongsTo
	 */
	public function contact()
	{
		return $this->belongsTo(Contact::class);
	}

	/////////////scope
	/**
	 * @param $q
	 * @param $value
	 * @return mixed
	 */
	public function scopeByValue($q, $value)
	{
		return $q->where('value', $value);
	}


	/**
	 * @param $q
	 * @param $userId
	 * @return mixed
	 */
	public function scopeByUserId($q, $userId)
	{
		return $q->where('user_id',$userId);
	}
	
	/**
	 * @param $q
	 * @param $contactId
	 * @return mixed
	 */
	public function scopeByContactId( $q, $contactId)
	{
		return $q->where('contact_id',$contactId);
	}

}
