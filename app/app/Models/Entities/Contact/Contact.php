<?php namespace App\Models\Entities\Contact;

use App\Models\Entities\ModelBase;

/**
 * Class Contact
 *
 * @package App\Models\Entities\User
 */
class Contact extends ModelBase
{
	/**
	 * @var string
	 */
	protected $table    = 'contacts';
	/**
	 * @var array
	 */
	protected $fillable = [
		//| id            | bigint(20) unsigned                                | NO   | PRI | NULL    | auto_increment |
         'title',          //| varchar(255)                                       | YES  |     | NULL    |                |
         'key',            //| varchar(50)                                        | NO   | UNI | NULL    |                |
         'displayable',    //| tinyint(1)                                         | NO   |     | 1       |                |
         'keyboard_type', //| enum('Phone','Normal','Email','Number','dateTime') | NO   |     | Normal  |                |
         'regex',          //| varchar(255)                                       | YES  |     | NULL    |                |
        //| created_at    | timestamp                                          | YES  |     | NULL    |                |
        //| updated_at    | timestamp                                          | YES  |     | NULL    |                |

	];

	/**
	 * @var array
	 */
	protected $hidden = [
		'created_at',
		'updated_at'
	];


	/////////////relation
	public function contactUser()
    {
        return $this->hasMany(ContactUser::class);
    }

	/////////////scope
	/**
	 * @param $q
	 * @param $key
	 * @return mixed
	 */
	public function scopeGetByKey( $q, $key)
	{
		return $q->where('key',$key);
	}
}
