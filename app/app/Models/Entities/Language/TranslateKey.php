<?php namespace App\Models\Entities\Language;


use App\Models\Entities\ModelBase;

class TranslateKey extends ModelBase
{
    protected $table = 'translate_keys';
    public $timestamps  = false;
    protected $fillable = [
        //| id          | bigint(20) unsigned | NO   | PRI | NULL    | auto_increment |
        'key',           //| varchar(255)        | NO   | MUL | NULL    |                |
        'description',   //| text                | YES  |     | NULL    |                |
    ];

    public function translate()
    {
        return $this->hasMany(Translate::class);
    }

}
