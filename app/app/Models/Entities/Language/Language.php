<?php namespace App\Models\Entities\Language;

use App\Models\Entities\ModelBase;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Language
 *
 * @package App\Models\Entities\Language
 */
class Language extends ModelBase
{
	
	/**
	 * @var string
	 */
	protected $table      = 'languages';
	/**
	 * @var bool
	 */
	public $timestamps = false;
	
	/**
	 * @var array
	 */
	protected $fillable = [
        //| id        | bigint(20) unsigned | NO   | PRI | NULL    | auto_increment |
         'status',      //| tinyint(1)          | NO   |     | 1       |                |
         'iso2',        //| varchar(2)          | NO   | MUL | NULL    |                |
         'title',       //| varchar(50)         | YES  |     | NULL    |                |
         'direction',   //| enum('rtl','ltr')   | NO   |     | ltr     |                |
         'flag',   //| varchar(255)        | YES  |     | NULL    |                |
	];

	/**
	 * @var array
	 */
	protected $hidden = [
		'created_at',
		'updated_at'
	];

	/////////////relations
	/**
	 * @return HasMany
	 */
	public function translates()
	{
		return $this->hasMany( Translate::class);
	}


	/////////////scope
	/**
	 * @param $q
	 * @param $iso2
	 * @return mixed
	 */
	public function scopeByIso2($q, $iso2)
	{
		return $q->where('iso2', $iso2);
	}


	/**
	 * @param $q
	 * @param $id
	 * @return mixed
	 */
	public function scopeById( $q, $id)
	{
		return $q->where('id', $id);
	}
	
	
	/**
	 * @param $value
	 * @return \Illuminate\Contracts\Routing\UrlGenerator|string
	 */
	public function getFlagAttribute($value)
	{
		return url(FLAG_PATH.strtolower($value).'.svg');
	}
}
