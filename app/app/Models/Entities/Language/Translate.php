<?php namespace App\Models\Entities\Language;

use App\Models\Entities\ModelBase;

class Translate extends ModelBase
{
    protected $table = 'translates';

    protected $fillable = [
            //| id               | bigint(20) unsigned | NO   | PRI | NULL    | auto_increment |
             'language_id',       //| bigint(20) unsigned | NO   | MUL | NULL    |                |
             'translate_key_id',  //| bigint(20) unsigned | NO   | MUL | NULL    |                |
             'device_type_id',         //| bigint(20) unsigned | NO   | MUL | NULL    |                |
             'value',             //| varchar(255)        | NO   |     | NULL    |                |
            //| created_at       | timestamp           | YES  |     | NULL    |                |
            //| updated_at       | timestamp           | YES  |     | NULL    |                |
	];

    protected $hidden = [
    	'created_at',
		'updated_at'
	];

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function translateKey()
    {
        return $this->belongsTo(TranslateKey::class);
    }
}
