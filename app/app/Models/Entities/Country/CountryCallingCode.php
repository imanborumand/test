<?php namespace App\Models\Entities\Country;

use App\Models\Entities\ModelBase;

class CountryCallingCode extends ModelBase
{
	protected $table    = 'country_calling_codes';
    public $timestamps  = false;
	protected $fillable = [
        //| id           | bigint(20) unsigned | NO   | PRI | NULL    | auto_increment |
        'country_id', //| bigint(20) unsigned | NO   | MUL | NULL    |                |
        'code',       //| varchar(20)         | NO   |     | NULL    |                |
	];



}
