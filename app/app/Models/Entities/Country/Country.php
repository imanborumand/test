<?php namespace App\Models\Entities\Country;

use App\Models\Entities\Language\TranslateKey;
use App\Models\Entities\ModelBase;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Country
 *
 * @package App\Models\Entities\Country
 */
class Country extends ModelBase
{
	/**
	 * @var string
	 */
	protected $table      = 'countries';
	/**
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * @var array
	 */
	protected $fillable = [
        //| id    | bigint(20) unsigned | NO   | PRI | NULL    | auto_increment |
        'iso2',  //| varchar(2)          | YES  | MUL | NULL    |                |
        'iso3',  //| varchar(3)          | YES  | MUL | NULL    |                |
        'name',  //| varchar(80)         | YES  | MUL | NULL    |                |
        'idd',   //| varchar(25)         | YES  |     | NULL    |                |
	];

	/**
	 * @var array
	 */
	protected $appends = ['flag'];


	/////////////////relation
	/**
	 * @return HasMany
	 */
	public function countryCallingCodes()
    {
        return $this->hasMany(CountryCallingCode::class);
    }


	/**
	 * @return HasOne
	 */
	public function translateKey()
    {
        return $this->hasOne(TranslateKey::class,'key','name');
    }


	////////scope and mutator

	/**
	 * @return UrlGenerator|string
	 */
	public function getFlagAttribute()
	{
		return url(FLAG_PATH.strtolower($this->iso2).'.svg');
	}

}
