<?php namespace App\Models\Entities\Country;

use App\Models\Entities\ModelBase;
use App\Models\Entities\User\User;

/**
 * Class Nationality
 *
 * @package App\Models\Entities\Country
 */
class Nationality extends ModelBase
{
	/**
	 * @var string
	 */
	protected $table = 'countries';
	/**
	 * @var bool
	 */
	public $timestamps = false;
	
	/**
	 * @var array
	 */
	protected $fillable = [
        //| id    | bigint(20) unsigned | NO   | PRI | NULL    | auto_increment |
        'iso2',  //| varchar(2)          | YES  | MUL | NULL    |                |
        'iso3',  //| varchar(3)          | YES  | MUL | NULL    |                |
        'name',  //| varchar(80)         | YES  | MUL | NULL    |                |
        'idd',   //| varchar(25)         | YES  |     | NULL    |                |
	];
	
	/**
	 * @var array
	 */
	protected $appends = ['flag'];
	
	
	/////////relation
	public function user()
	{
		return $this->hasMany(User::class);

	}
	
	////////scope and mutator
	public function getFlagAttribute()
	{
		return url(FLAG_PATH.strtolower($this->iso2).'.svg');
	}
	
	
	
}
