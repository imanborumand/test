<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;
use App\CustomStuff\Traits\ApiResponseTrait;

/**
 * Class CustomNotifyNotFoundException
 *
 * @package App\Exceptions
 */
class CustomNotifyNotFoundException extends Exception
{
    use  ApiResponseTrait;
	
	/**
	 * @var string
	 */
	public $errorMessage;
	/**
	 * @var int
	 */
	public $errorHttpCode;
	/**
	 * @var int
	 */
	public $errorStatusCode;
	
	
	/**
	 * CustomNotFoundException constructor.
	 *
	 * @param string|null $message
	 * @param int         $httpCode
	 * @param int         $statusCode
	 */public function __construct(string $message = 'Notify not found', int $httpCode = Response::HTTP_NOT_FOUND , int $statusCode = STATUS_CODE_NOT_FOUND)
    {
         parent::__construct($message, $httpCode);
		$this->errorMessage = $message;
		$this->errorHttpCode = $httpCode;
		$this->errorStatusCode = $statusCode;
	}
	
	
	/**
	 *
	 */
	public function report()
    {
        //
    }
	
	
	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function render()
    {
		return $this->notFoundResponse($this->errorMessage)
					->setHttpCode($this->errorHttpCode)
					->setStatusCode($this->errorStatusCode)
					->response();
    }

}
