<?php namespace App\Exceptions;

use App\CustomStuff\Traits\ApiResponseTrait;
use Exception;
use Illuminate\Http\Response;


/**
 * Class CustomResponseException
 *
 * @package App\Exceptions
 */
class CustomResponseException extends Exception
{
	/**
	 * this custom exception class return customize response by HttpStatusCode and StatusCode
	 */

	use ApiResponseTrait;
	
	/**
	 * @var string
	 */
	protected $errorMessage;
	/**
	 * @var int
	 */
	protected $errorHttpCode;
	/**
	 * @var int
	 */
	protected $errorStatusCode;
	
	
	/**
	 * CustomResponseException constructor.
	 *
	 * @param string $message
	 * @param int    $httpCode
	 * @param int    $statusCode
	 */
	public function __construct( string $message = 'error!', int $httpCode = Response::HTTP_BAD_REQUEST, int $statusCode =STATUS_CODE_BAD_REQUEST)
	{
		parent::__construct($message, $httpCode);
		$this->errorMessage = $message;
		$this->errorHttpCode = $httpCode;
		$this->errorStatusCode = $statusCode;
	}
	
	
	/**
	 *
	 */
	public function report()
	{
		//
	}
	
	
	/**
	 * @param $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function render( $request)
	{
		return $this->customResponse([], false, $this->errorStatusCode, $this->errorHttpCode, $this->errorMessage)
			->response();
	}

}
