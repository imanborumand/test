<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;
use App\CustomStuff\Traits\ApiResponseTrait;


/**
 * Class CustomUploadFileException
 *
 * @package App\Exceptions
 */
class CustomUploadFileException extends Exception
{
	use  ApiResponseTrait;
	
	/**
	 * @var \Illuminate\Config\Repository|mixed|string
	 */
	public $errorMessage;
	/**
	 * @var int
	 */
	public $errorHttpCode;
	/**
	 * @var int
	 */
	public $errorStatusCode;
	
	
	/**
	 * CustomUploadFileException constructor.
	 *
	 * @param string|null $message
	 * @param int         $httpCode
	 * @param int         $statusCode
	 */public function __construct( string $message = null, int $httpCode = Response::HTTP_INTERNAL_SERVER_ERROR , int $statusCode = STATUS_CODE_INTERNAL_SERVER_ERROR)
	{
		parent::__construct($message, $httpCode);
		$this->errorMessage = $message ?? config('default_messages.response.uploading_file_error');
		$this->errorHttpCode = $httpCode;
		$this->errorStatusCode = $statusCode;
	}
	
	
	/**
	 *
	 */
	public function report()
	{
		//
	}
	
	
	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function render()
	{
		return $this->internalErrorResponse($this->errorMessage)
					->setHttpCode($this->errorHttpCode)
					->setStatusCode($this->errorStatusCode)
					->response();
	}
}
