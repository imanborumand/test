<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;
use App\CustomStuff\Traits\ApiResponseTrait;


/**
 * Class CustomPermissionDeniedException
 *
 * @package App\Exceptions
 */
class CustomPermissionDeniedException extends Exception
{
	use  ApiResponseTrait;
	
	/**
	 * @var string
	 */
	public $errorMessage;
	/**
	 * @var int
	 */
	public $errorHttpCode;
	/**
	 * @var int
	 */
	public $errorStatusCode;
	
	
	/**
	 * CustomPermissionDeniedException constructor.
	 *
	 * @param string|null $message
	 * @param int         $httpCode
	 * @param int         $statusCode
	 */public function __construct( string $message = null, int $httpCode = Response::HTTP_FORBIDDEN , int $statusCode = STATUS_CODE_FORBIDDEN)
	{
		parent::__construct($message, $httpCode);
		$this->errorMessage = $message;
		$this->errorHttpCode = $httpCode;
		$this->errorStatusCode = $statusCode;
	}
	
	
	/**
	 *
	 */
	public function report()
	{
		//
	}
	
	
	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function render()
	{
		return $this->notAuthorizedResponse($this->errorMessage)
					->setHttpCode($this->errorHttpCode)
					->setStatusCode($this->errorStatusCode)
					->response();
	}
}
