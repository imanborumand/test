<?php

namespace App\Exceptions;

use App\CustomStuff\Traits\ApiResponseTrait;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Handler extends ExceptionHandler
{
	use ApiResponseTrait;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param Exception $exception
     * @return void
     * @throws Exception
	 */
    public function report(Exception $exception)
    {
		if (app()->bound('sentry') && $this->shouldReport($exception)) {
			app('sentry')->captureException($exception);
		}
	
		parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  Request $request
     * @param  Exception                $exception
     * @return Response
     * @throws Exception
	 */
	public function render($request, Exception $exception)
    {
        $request->merge([ 'exception' => $exception->getTraceAsString()]); // add exception to request for insert in logModel
		$className = str_replace('"', "", get_class($exception));

		if($this->isHttpException($exception) || !(isset($className) && substr($className,0,14) == CUSTOM_EXCEPTIONS_PATH)) {
			$httpCode = $this->getResponseHttpCode($exception);
			$statusCode = $this->getStatusCode($httpCode);
			throw  new  CustomApiException($exception->getMessage(),$httpCode,$statusCode);
		}

		return parent::render($request, $exception);
    }

	private function getResponseHttpCode($e)
	{
		$httpCodeArray = config('general.http_code');
		$exceptionCode = $e->getCode();
		
		$notHttpExceptionCode = $this->checkException($e);
		
		if($notHttpExceptionCode){
			return $notHttpExceptionCode;
		}
		
		if($this->isHttpException($e) && $e->getStatusCode() != 0 && in_array($e->getStatusCode(),$httpCodeArray) ) {
			return $e->getStatusCode();

		}elseif(isset($exceptionCode) && $exceptionCode != 0  && in_array($exceptionCode,$httpCodeArray)) {
			return $exceptionCode;
		}
		return Response::HTTP_INTERNAL_SERVER_ERROR;
	}

	private function getStatusCode($httpCode)
	{
		switch ($httpCode) {
			case 200:
				return STATUS_CODE_OK;
				break;
			case 201:
				return STATUS_CODE_CREATED;
				break;
			case 404:
				return STATUS_CODE_NOT_FOUND;
				break;
			case 500:
				return STATUS_CODE_INTERNAL_SERVER_ERROR;
			break;
			case 401:
				return STATUS_CODE_UNAUTHENTICATED;
			break;
			case 405:
				return STATUS_CODE_METHOD_NOT_ALLOWED;
				break;
			case 403:
				return STATUS_CODE_FORBIDDEN;
				break;
			case 400:
				return STATUS_CODE_BAD_REQUEST;
				break;
			case 422:
				return STATUS_CODE_UNPROCESSABLE_ENTITY;
				break;
			default:
				return STATUS_CODE_INTERNAL_SERVER_ERROR;
		}
	}
	
	private function checkException($e)
	{
		if ($e instanceof AuthenticationException) {
			return 401;
		}
		/*elseif ($e instanceof ValidationException) {
			//return $this->convertValidationExceptionToResponse($e, $request);
		}*/
		return  false;
	}
	
}
