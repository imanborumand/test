<?php namespace App\CustomStuff\Traits;


use App\Models\Entities\Log\JobsLogModel;
use Illuminate\Support\Facades\Log;

/**
 * Trait JobsLogTrait
 * this trait log all jobs to mongodb
 * @package App\CustomStuff\Traits
 */
trait JobsLogTrait
{

	public function doLogResponseAndRequest($params, $request)
	{
	        $result =  array_merge($params, $request);
			JobsLogModel::create($result);
	}

}
