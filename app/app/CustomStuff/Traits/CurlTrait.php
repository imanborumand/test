<?php namespace App\CustomStuff\Traits;


use Illuminate\Support\Facades\Log;

trait CurlTrait
{
	
	
	/**
	 * this function send request to all wrapper in check mobile and login step
	 * and return response form all wrappers (DS)
	 * wrapper params get from wrapper_config
	 * stepName -> equal login or check-mobile for every step
	 * @param array  $wrappers
	 * @param array  $params
	 * @param string $stepName
	 * @return array
	 */
	public function doRequestToMultiWrapperInLoginStep(array $wrappers, array $params, string $stepName = 'check_mobile') : array
	{
		$timeStart = microtime(true); //for get time curl request to DS
		$chs   = [];
		$mh    = curl_multi_init(); //create the array of cURL handles and add to a multi_curl
		$key   = 0;
		
		foreach($wrappers as $wrapper) {
			$baseUrl = $wrapper['base_url'] . $wrapper[$stepName]['path'];
			
			$chs[$key] = curl_init($baseUrl);
			curl_setopt( $chs[$key] , CURLOPT_FOLLOWLOCATION , TRUE);
			curl_setopt( $chs[$key] , CURLOPT_HTTPHEADER , $wrapper[$stepName]['header']);
			
			curl_setopt( $chs[$key] , CURLOPT_POSTREDIR , 3);
			curl_setopt( $chs[$key] , CURLOPT_TIMEOUT , 5);
			curl_setopt( $chs[$key] , CURLOPT_ENCODING , isset($wrapper[$stepName]['encoding']) ? $wrapper[$stepName]['encoding'] : 'utf-8');
			curl_setopt( $chs[$key] , CURLOPT_RETURNTRANSFER , TRUE);
			curl_setopt( $chs[$key] , CURLOPT_SSL_VERIFYHOST , 0);
			curl_setopt( $chs[$key] , CURLOPT_SSL_VERIFYPEER , 0);
			curl_setopt( $chs[$key] , CURLOPT_URL , $baseUrl);
			
			curl_setopt( $chs[$key] , CURLOPT_CUSTOMREQUEST , "POST");
			curl_setopt( $chs[$key] , CURLOPT_POST , TRUE);
			curl_setopt( $chs[$key] , CURLOPT_POSTFIELDS , json_encode($params));
			curl_multi_add_handle( $mh , $chs[$key] );
			$key++;
		}
		
		//running the requests
		$running = NULL;
		do {
			curl_multi_exec($mh , $running);
		} while($running);
		
		$response = [];
		$key   = 0;
		
		//getting the responses
		foreach($wrappers as $wrapperKey => $wrapper) {
			$response[$wrapperKey] = [
				'wrapper' => $wrapperKey,
				'response_time' => (string) curl_getinfo($chs[$key] , CURLINFO_TOTAL_TIME), //get response time
				'status_code' => (int) curl_getinfo($chs[$key], CURLINFO_HTTP_CODE), // get http status code
				'result'    => (string) curl_multi_getcontent($chs[$key]) ,
			];
			
			curl_multi_remove_handle($mh , $chs[$key]);
			$key++;
		}
		
		curl_multi_close($mh);
		
		//add curls time to request for see in Logs
		request()->merge([
			 'curl_info' => [
				"curl_time" => microTimeFormat($timeStart)
			 ]
		]);
		
		return $response;
	}
}
