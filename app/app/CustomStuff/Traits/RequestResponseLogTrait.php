<?php namespace App\CustomStuff\Traits;


use App\Jobs\LogResponseAndRequestJob;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 * Trait RequestResponseLogTrait
 * this trait log all request and response to mongodb
 * @package App\CustomStuff\Traits
 */
trait RequestResponseLogTrait
{

	public function doLogResponseAndRequest($response)
	{
	    $guard = $this->getGuard();
	    if ($guard !== null) {
	        $request = [
                'request' => request()->all(),
                'exception' => request()->exception ?? null,
                'url' => request()->fullUrl(),
                'method' => request()->getMethod(),
                'ip' => request()->getClientIp(),
                'request_time' => time()
            ];
	       dispatch(new LogResponseAndRequestJob($guard,$request,$response));
        }
	}


    /**
     * @return array|null
     */
    private function getGuard() : ?array
    {
        $auth = Auth::guard('users');
        if ($auth->check()) {
            return [
                'guard' => "users",
                'id' => $auth->user()->id
            ];
        }

        //for check request that receive from Wrapper add guard
        if (isset(request()->token) && request()->token == 'VikTplgucVCtd21jVXsLGMW3H95Ezr66QRnApU19dxUc9uBrbDjCcICkKjvY') {
            return [
                'guard' => "wrapper",
                'id' => 0
            ];
        }
        
        //if user not login and request not receive from wrapper
		return [
			'guard' => "no-guard",
			'id' => 0
		];
    }

}
