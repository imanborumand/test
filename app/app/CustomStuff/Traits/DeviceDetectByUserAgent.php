<?php namespace App\CustomStuff\Traits;

use App\Exceptions\CustomValidationException;

trait DeviceDetectByUserAgent
{
	/**
	 * this trait detect user os type  by $_SERVER['HTTP_USER_AGENT'] and return os
	 * current valid os type is android and ios
	 *
	 * @return int|string
	 * @throws CustomValidationException
	 */
	function osInfo()
	{
		// the order of this array is important
		// the order of this array is important
		preg_match("/iPhone|Android|iPad|iPod|webOS|PostmanRuntime/", $_SERVER['HTTP_USER_AGENT'], $matches);
		$os = current($matches);
		
		if($os != false) {
			return $os;
		}
		
		return 'Unknown';
	}
}
