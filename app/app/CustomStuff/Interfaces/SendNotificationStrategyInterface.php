<?php namespace App\CustomStuff\Interfaces;



/**
 * interface for impalement in all notify class by strategy pattern
 *
 */
interface SendNotificationStrategyInterface
{
    public function sendNotification() : array ;
}
