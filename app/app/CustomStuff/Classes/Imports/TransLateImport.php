<?php namespace App\CustomStuff\Classes\Imports;

use App\Models\Repositories\Device\DeviceTypeRepository;
use App\Models\Repositories\Language\TranslateKeyRepository;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class TransLateImport implements   WithHeadingRow ,WithValidation, ToCollection
{
	//OnEachRow,ToModel
	use Importable;

	protected  $keyNames = [];
	protected  $data ;
	protected  $language ;

	///insert translate keys then get key ids and insert all of translate data in database
	public function collection(Collection $rows)
	{

		$this->data = $rows->slice(1);
		$this->language = array_filter($rows[0]->toArray());
		$this->insertKeys($this->data);

		$translateKeysWithId = array_flip(app(TranslateKeyRepository::class)->getTranslatesKeyByName($this->keyNames));
		$device= app(DeviceTypeRepository::class)->getByName();

		$insertData = [];
		foreach ($this->data as $row) {
			foreach($this->language as $languageName => $languageId) {
				if(isset($row[$languageName]) && $row[$languageName]){
                        $insertData[] = [
                            'language_id'      => $languageId,
                            'translate_key_id' => $translateKeysWithId[$row['key']] ,
                            'device_type_id'   => $device->id ,
                            'value'            => $row[$languageName] ,
                        ];

				}//if
			}//for each
		}

		bulkInsertOrUpdate($insertData,'translates');

	}

	///insert translate key in table
	private function insertKeys($rows)
	{
		$keyArrays = [];
		$keyNames = [];


		foreach ($rows as $row)
		{
			$keyArrays[]        = [
				'key'		  =>  $row['key'],
				'description' => "", //todo $row['description'] ? trim($row['description']) : ""
			];
			$keyNames[] = $row['key'];
		}
		bulkInsertOrUpdate($keyArrays,'translate_keys');
		return $this->keyNames = $keyNames;
	}

	//determine that first  row is labels
	public function headingRow(): int
	{
		return 1;
	}

	public function rules(): array
	{
		return [
			///'id' => 'exists:languages,id',
		];
	}

}
