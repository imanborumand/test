<?php namespace App\CustomStuff\Classes;

use App\Models\Entities\Device\DeviceBrand;
use App\Models\Entities\Device\DeviceType;
use App\Models\Repositories\Device\DeviceTypeRepository;
use Exception;
use Illuminate\Support\Facades\Cache;

/**
 * Class DetectDeviceInfo
 * this class detect user device info [ brand, type, os]
 * for use this class and get device info user add CheckDeviceInfoMiddleware to your route
 * for get brand and type and os use deviceInfo() method
 * for get cached of device tables in database use methods thad contains Cache
 *
 * @package App\CustomStuff\Classes
 */
class DetectDeviceInfo
{

    private $deviceBrand = EN_DEFAULT_WORD;

    private $deviceType  = EN_DEFAULT_WORD;

    private $deviceOsInRequest = null;

    public $deviceBrandCacheName = "device_brands"; //set public for use in observer and other location that remove cache

    public $deviceTypeCacheName = "device_types";

    private $header = null;

    public function __construct()
    {
        $this->header = request()->headers;
    }

    /**
     * get device info
     * @return array
     */
    public function deviceInfo() : array
    {
        return [
            'device_brand' => $this->getDeviceBrand(),
            'device_type' => $this->getDeviceType(),
            'device_os' => $this->getDeviceOsInRequest(),
        ];
    }


    public function getDeviceOsInRequest()
    {
       if($this->header->get('device-os') && strlen($this->header->get('device-os')) > 2) {
          return  $this->deviceOsInRequest = strtolower($this->header->get('device-os'));
       }

       return $this->deviceOsInRequest;
    }

    /**
     * @return array
     */
    public function getDeviceBrand() : array
    {
        $deviceBrandCache = $this->getAllDeviceBrandFromCache();

        //if device_brand key not exist in header and not set deviceBrand run this condition
        if($this->header->get('device-brand') && $this->deviceBrand == EN_DEFAULT_WORD) {
            $this->deviceBrand = $this->header->get('device-brand');
            return $this->checkDeviceBrandQuery($deviceBrandCache);
        }
        
        return $this->checkDeviceBrandQuery($deviceBrandCache);
    }


    /**
     * @param object $deviceBrandCache
     * @return array
     */
    private function checkDeviceBrandQuery(object $deviceBrandCache) : array
    {

        $result = $this->getFirstQueryBrand($deviceBrandCache, $this->deviceBrand);
        if(count($result) != 0) {
            return  $result;
        }

        $resultDefault = $this->getFirstQueryBrand($deviceBrandCache, EN_DEFAULT_WORD);
        if(count($resultDefault) != 0) {
            return  $resultDefault;
        }
        return [];
    }


    /**
     *  get first by value and fieldName from collection
     *
     * @param object $query
     * @param string $value
     * @param string $fieldName
     * @return array
     */
    private function getFirstQueryBrand(object  $query, string $value, string $fieldName = 'name') : array
    {
        $deviceBrandQuery = $query->where($fieldName, $value)->first();
        if($deviceBrandQuery) {
            return [
                'id' => $deviceBrandQuery->id,
                'name' => $deviceBrandQuery->name
            ];
        }
        return [];
    }


    /**
     * @return array
     */
    public function getDeviceType() : array
    {
        $deviceTypeCache = $this->getAllDeviceTypeFromCache();
        //if device_brand key not exist in header and not set deviceBrand run this condition
        if($this->header->get('device-type') && $this->deviceType == EN_DEFAULT_WORD) {

            $this->deviceType = $this->header->get('device-type');
            return $this->checkDeviceTypeQuery($deviceTypeCache);
        }

        return $this->checkDeviceTypeQuery($deviceTypeCache);
    }

    /**
     *  check type of device and return array of type
     * @param object $query
     * @return array
     */
    private function checkDeviceTypeQuery(object $query) : array
    {
        $result = $this->getFirstQueryType($query, $this->deviceType);
        if(count($result) != 0) {
            return  $result;
        }

        $resultDefault = $this->getFirstQueryType($query, EN_DEFAULT_WORD);
        if(count($resultDefault) != 0) {
            return  $resultDefault;
        }
        return [];
    }

    /**
     *  get first by value and fieldName from collection
     *
     * @param object $query
     * @param string $value
     * @param string $fieldName
     * @return array
     */
    private function getFirstQueryType(object  $query, string $value, string $fieldName = 'name') : array
    {
        $deviceBrandQuery = $query->where($fieldName, $value)->first();
        if($deviceBrandQuery) {
            return [
                'id' => $deviceBrandQuery->id,
                'name' => $deviceBrandQuery->name,
                'os' => $deviceBrandQuery->os
            ];
        }
        return [];
    }


    /**
     * @param string $deviceBrand
     * @return DetectDeviceInfo
     */
    public function setDeviceBrand(string $deviceBrand) : DetectDeviceInfo
    {
        $this->deviceBrand = $deviceBrand;
        return $this;
    }

    /**
     * @param string $deviceType
     * @return DetectDeviceInfo
     */
    public function setDeviceType(string $deviceType) : DetectDeviceInfo
    {
        $this->deviceType = $deviceType;
        return $this;
    }


    /**
     * @param string $os
     * @return $this
     */
    public function setDeviceOsInRequest(string $os)
    {
        $this->deviceOsInRequest= $os;
        return $this;
    }

    /**
     * add all row of brands to cache
     * @return mixed
     */
    public function getAllDeviceBrandFromCache()
    {
        Cache::forget($this->deviceBrandCacheName); //todo remove
        return Cache::get($this->deviceBrandCacheName, function() {
           $return = DeviceBrand::all();

            Cache::forever($this->deviceBrandCacheName, $return);
            return $return;
        });
    }


    /**
     * add all row of types to cache
     * @return mixed
     */
    public function getAllDeviceTypeFromCache()
    {
        Cache::forget($this->deviceTypeCacheName); //todo remove
        return Cache::get($this->deviceTypeCacheName, function() {
            $return = DeviceType::all();

            Cache::forever($this->deviceTypeCacheName, $return);
            return $return;
        });
    }


    /**
     * add row of types by relations  to cache
     *
     * @param string $name
     * @return mixed
     */
    public function getDeviceTypeByNameFromCache(string $name = EN_DEFAULT_WORD) : array
    {
        $typeName = strtolower(trim(ucwords($name)));

        Cache::forget($this->deviceTypeCacheName . $typeName); //todo remove

        return Cache::get($this->deviceTypeCacheName  . $typeName, function() use ($typeName, $name) {
            $return = DeviceType::with('deviceBrand', 'deviceSetting')
                                ->where('name',$name)
                                ->first()
                                ->toArray();

            Cache::forever($this->deviceTypeCacheName . $typeName, $return);
            return $return;
        });
    }


    /**
     * add row of types by relations  to cache
     *
     * @param int $deviceTypeId
     * @return mixed
     */
    public function getDeviceTypeByIdFromCache(int $deviceTypeId) : array
    {
        Cache::forget($this->deviceTypeCacheName . '_id_' . $deviceTypeId); //todo remove
        return Cache::get($this->deviceTypeCacheName  . '_id_' . $deviceTypeId, function() use ($deviceTypeId) {

            try {
				$return = app(DeviceTypeRepository::class)->getDeviceTypeWithBrandAndSetting($deviceTypeId);
                if(! $return) {
                    return [];
                }
                $return = $return->toArray();

                $defaultSetting = DeviceType::with('deviceSetting')->where('name', EN_DEFAULT_WORD)->first()->toArray();

                //add default setting device to response
                $return = array_merge($return, [DEFAULT_DEVICE_SETTING => $defaultSetting['device_setting']]);

                //if not set setting for device requested- add default setting to return array
                if($return['device_setting'] == null) {
                    $return = array_merge($return, ['device_settings' => $defaultSetting['device_setting'] ]);
                }

                Cache::forever($this->deviceTypeCacheName . '_id_' . $deviceTypeId, $return);
                return $return;
            } catch(Exception $exception) {
                return [];
            }
        });
    }

}
