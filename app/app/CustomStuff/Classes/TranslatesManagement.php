<?php

namespace App\CustomStuff\Classes;

use App\Models\Repositories\Language\TranslateKeyRepository;

class TranslatesManagement
{
	
	/**
	 * get json file from public folder that contain translate of request language
	 * if json file of language exist then :
	 * 1-read translate key of file and  store new key to database if not exist
	 * 2-get translate keys ids
	 * 3-save new  translate in translate table
	 *
	 * @param $language
	 * @param $device
	 */
	
	public function import(object $language,object $device)
	{
		$path = public_path( "/languages_json/{$language->iso2}.json" );
		if(file_exists($path)){
			$translates = json_decode( file_get_contents($path) , true );
			$keyNames = $this->insertKeys($translates);
			$translateKeysWithId = app(TranslateKeyRepository::class)->getTranslatesKeyByName($keyNames);
			
			$data =[];
			foreach($translateKeysWithId as  $translateKeyId => $translateKeyName) {
				$data[] = [
					'language_id'      => $language->id ,
					'translate_key_id' => $translateKeyId,
					'device_type_id'   => $device->id ,
					'value'            => $translates[$translateKeyName] ,
				];
			}//foreach
			bulkInsertOrUpdate($data,'translates');
		}//if file_exists
	}
	
	
	/**
	 * bulk insert new key to database
	 *
	 * @param $translates
	 * @return array
	 */
	private function insertKeys($translates)
	{
		$keyNames = array_keys($translates);
		$keysData =[];
		foreach($keyNames as $name) {
			$keysData[]['key'] =$name;
		}
		bulkInsertOrUpdate($keysData,'translate_keys');
		return $keyNames;
	}
}


