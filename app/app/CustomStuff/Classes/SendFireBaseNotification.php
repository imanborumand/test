<?php  namespace App\CustomStuff\Classes;


use App\CustomStuff\Interfaces\SendNotificationStrategyInterface;

/**
 * Class SendFireBaseNotification
 * this class send notification to fireBase
 * use sendNotification() method for send
 *
 * @package App\CustomStuff\Classes
 */

class SendFireBaseNotification implements SendNotificationStrategyInterface
{

    private $notifyTitle;

    private $notifyBody;

    private $userToken = null; // firebase user token

    private $multiTokens = []; //to send notify to multi users

    private $notifyImage = null; //todo add default image

    private $notifySound = null; //todo add default sound

    private $firebaseAPiUrl = 'https://fcm.googleapis.com/fcm/send'; //todo set in helper or config

    private $firebaseAuthorizationKey = "AAAAez7Q0eY:APA91bFJ0BXtmVhccy68cgJzmzsy4JijqZx1DfV_idpM-Q8d9NPf2KZ9HsbVS7pkj3kqPgtpD4JvUrdDiQA6C1MrwWwjSSy9KASQS4UoVuyXwjFyQULdBZqGFmmsGXdbUSmfxVM8LaAW"; //test
//    private $firebaseAuthorizationKey = "AAAA5zUlZmI:APA91bGYskCFJXV3O2VrNVxWioMC56InCX8_ti9tqs08oW_OCWe3xTzZa5RSfxZAR_VDiFkA3g7YHvCEj3fRfJNGra1QcY5Id_DjOXvVQrq4kHSU-q8ckBwmVF6f_YHQGb96pWD-7GJQ"; //todo set in helper or config

    private $notificationArray = [];

    private $notificationData = [];

    private $firebaseHeader = [];
    
    private $user = null;
    

    /**
     * SendFireBaseNotification constructor.
     *
     * @param string $notifyTitle
     */
    public function  __construct(string $notifyTitle)
    {
        $this->notifyTitle = $notifyTitle;
        $this->notifyBody = config('default_messages.notification.custom_notify_body');
    }


    /**
     * public method to call and send notification
     *
     * @return array
     */
    public function sendNotification() :array
    {
        $this->setNotifyInfo();
        return $this->curl();
    }


    /**
     * set custom notification data
     */
    private function setNotifyInfo()
    {

        $this->notificationArray = [
            'title' => $this->notifyTitle,
            'body' => $this->getNotifyBody()
        ];

        if($this->getNotifySound() != null) {
            $this->notificationArray = array_merge($this->notificationArray, ['sound' => $this->getNotifySound()]);
        }

        if($this->getNotifyImage() != null) {
            $this->notificationArray = array_merge($this->notificationArray, ['image' => $this->getNotifyImage()]);
        }
        
        
        if($this->getUserToken()) {
			 return $this->notificationData = [
				'to'        => $this->getUserToken() ,
				'notification' => $this->notificationArray,
				'data' => [
					"message" => $this->notificationArray,
				]
			];
		}
        //send to multi token
        $this->notificationData = [
            'registration_ids' =>  $this->getMultiTokens(),
            'notification' => $this->notificationArray,
            'data' => [
				"message" => $this->notificationArray,
			]
        ];
    
    }


    /**
     *  send http request to firebase api url
     *
     * @return array
     */
    private function curl() : array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->firebaseAPiUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getFirebaseHeader());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->notificationData));
        $result = curl_exec($ch);
        curl_close($ch);
        return (array) $result;
    }


    /**
     * @param array $firebaseHeader
     */
    public function setFirebaseHeader(array $firebaseHeader) : void
    {
        $this->firebaseHeader = array_merge($this->firebaseHeader, $firebaseHeader);
    }



    /**
     * @param mixed $notifyBody
     * @return SendFireBaseNotification
     */
    public function setNotifyBody(string $notifyBody) : SendFireBaseNotification
    {
        $this->notifyBody = $notifyBody;
        return $this;
    }


    /**
     * @param mixed $userToken
     * @return SendFireBaseNotification
     */
    public function setUserToken(string $userToken) : SendFireBaseNotification
    {
        $this->userToken = $userToken;
        return $this;
    }


    /**
     * @param array $Tokens
     * @return SendFireBaseNotification
     */
    public function setMultiTokens(array $Tokens = []) : SendFireBaseNotification
    {
        $this->multiTokens = $Tokens;
        return $this;
    }


    /**
     * @param mixed $notifyImage
     * @return SendFireBaseNotification
     */
    public function setNotifyImage(string $notifyImage) : SendFireBaseNotification
    {
        $this->notifyImage = $notifyImage;
        return $this;
    }


    /**
     * @param mixed $notifySound
     * @return SendFireBaseNotification
     */
    public function setNotifySound(string $notifySound) : SendFireBaseNotification
    {
        $this->notifySound = $notifySound;
        return $this;
    }

    /**
     * @return array
     */
    public function getFirebaseHeader() : array
    {
        $this->firebaseHeader = array_merge($this->firebaseHeader, [
            'Content-Type: application/json',
            'Authorization: key=' . $this->firebaseAuthorizationKey
        ]);

        return $this->firebaseHeader;
    }



    /**
     * @return mixed
     */
    public function getNotifyBody()
    {
        return $this->notifyBody;
    }


    /**
     * @return mixed
     */
    public function getUserToken()
    {
        return $this->userToken;
    }



    /**
     * @return array
     */
    public function getMultiTokens() : array
    {
        return $this->multiTokens;
    }


    /**
     * @return mixed
     */
    public function getNotifyImage()
    {
        return $this->notifyImage;
    }


    /**
     * @return mixed
     */
    public function getNotifySound()
    {
        return $this->notifySound;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }
	
	
	/**
	 * @param object|null $user
	 * @return null |null
	 */
	public function setUser(object $user)
    {
        $this->user = $user;
        return $this;
    }
    
}
