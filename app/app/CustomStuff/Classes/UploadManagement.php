<?php

namespace App\CustomStuff\Classes;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

/**
 * Class UploadManagement
 *
 * @package App\CustomStuff\Classes
 */
class UploadManagement
{
	
	/**
	 * first delete old user avatar if exist
	 * then store new avatar
	 * @param $user
	 * @param $newAvatar
	 * @return string
	 */
	public function updateUserAvatar( $user, $newAvatar)
	{
		if(isset($user->avatar) && $user->avatar != null)
		{
			$this->deleteOldAvatar($user);
		}
		return $this->uploadAvatar($newAvatar);
	}
	
	
	/**
	 * generate hash for user avatar name
	 * and store uploaded file to public directory by hash name
	 * //avatars path -> /public/user/avatars/
	 * @param $file
	 * @return string
	 */
	private function uploadAvatar( $file)
	{
		$fileName = Md5(time().$file->getClientOriginalName()).'.'.$file->getClientOriginalExtension();
		Storage::disk(STORAGE_DISK)->putFileAs(PUBLIC_AVATAR_PATH, $file,$fileName);
		return $fileName;
	}
	
	
	/**
	 * get user avatar path and delete old avatar
	 * @param $user
	 * @return bool
	 */
	private function deleteOldAvatar( $user)
	{
		$path = PUBLIC_AVATAR_PATH.$user->getOriginal('avatar');
		return Storage::disk(STORAGE_DISK)->delete($path);
	}
}
