<?php namespace App\Services\Admin\Log;


use App\Models\Entities\Contact\ContactUser;
use App\Models\Entities\User\User;
use App\Models\Repositories\Log\JobsLogRepository;
use App\Models\Repositories\Log\LogRequestResponseRepository;
use App\Models\Repositories\Notify\NotifyRepository;
use App\Services\ServiceBase;

class AdminLogService extends ServiceBase
{
	protected $jobsLogRepository;
	
	public function __construct(LogRequestResponseRepository $repository)
	{
		$this->repository = $repository;
		$this->jobsLogRepository = app(JobsLogRepository::class);
	}
	
	
	/**
	 * response and request log
	 * @return mixed
	 */
	public function responseRequestLog()
	{
		//for delete log //todo this condition for debug -> remove it!
		if(request()->has('delete')) {
			return [
				'status' => $this->repository->deleteAllResponseRequestLog()
			];
		}
		return $this->repository->all();
	}
	
	
	/**
	 * return logs of SendUserInfoToWrapperJob class
	 * @return mixed
	 */
	public function sendUserInfoToWrapper()
	{
		//for delete log //todo this condition for debug -> remove it!
		if(request()->has('delete')) {
			return [
				'status' => $this->jobsLogRepository->deleteAllNotify()
			];
		}
		return $this->jobsLogRepository->getUserInfoToWrapperLog();
	}
	
	
	/**
	 * return all notifications log
	 * @return array
	 */
	public function allNotificationsLog()
	{
		$notificationRepository = app(NotifyRepository::class);
		//for delete log //todo this condition for debug -> remove it!
		if(request()->has('delete')) {
			return [
				'status' => $notificationRepository->deleteAllNotify()
			];
		}
		
		return $notificationRepository->all();
	}
	
	
	public function seeContactUser()
	{
		if(request()->has('user_id')) {
			return ContactUser::where('user_id', request('user_id'))->get();
		} else if(request()->has('token') && request()->has('user_id')) {
			return ContactUser::where('value', request('token'))->where('user_id', request('user_id'))->get();
		} else if(request()->has('token')) {
			return ContactUser::where('value', request('token'))->get();
		} else if(request()->has('delete')) {
			return [ ContactUser::truncate()];
		}
		
		return ContactUser::all()->groupBy('user_id');
	}
}
