<?php namespace App\Services\V1\Notify;

use App\Exceptions\CustomNotFoundException;
use App\Exceptions\CustomNotifyNotFoundException;
use App\Exceptions\CustomQueryErrorException;
use App\Exceptions\CustomUnAuthenticatedException;
use App\Exceptions\CustomUnAuthorizedException;
use App\Models\Entities\Log\JobsLogModel;
use App\Services\ServiceBase;
use App\Models\Repositories\Notify\NotifyRepository;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class NotifyService
 *
 * @package App\Services\V1\Notify
 */
class NotifyService extends ServiceBase
{
	
	/**
	 * NotifyService constructor.
	 *
	 * @param NotifyRepository $repository
	 */
	public function __construct(NotifyRepository $repository)
	{
		$this->repository = $repository;
	}
	
	
	/**
	 *  return list of notification state
	
	 * @return mixed
	 */
	public function all()
	{
		return $this->repository->all();
	}
	
	
	/**
	 * get all notify by user_id
	 *
	 * @param int $userId
	 * @return mixed
	 * @throws CustomUnAuthenticatedException
	 */
	public function getUserNotifies(int $userId)
	{
		$auth = Auth::guard('users');
		//request from user and return this user notifies
		if (! $auth->check()) {
			throw new CustomUnAuthenticatedException();
		}
		
		$user = $auth->user();
		if ($userId != $user->id) {
			throw new CustomUnAuthenticatedException();
		}
		
		//update all notify for this user => seen = 1
		$this->repository->updateSeenAllNotify($user->id);
		
		return $this->repository->getNotifyByUserId($userId);
		
	}
	
	
	/**
	 *  change seen notify to 1
	 *
	 * @param string $notifyId
	 * @return JobsLogModel
	 * @throws CustomNotFoundException
	 * @throws CustomNotifyNotFoundException
	 * @throws CustomQueryErrorException
	 * @throws CustomUnAuthorizedException
	 */
	public function changeSeenNotify(string $notifyId)
	{
		$notify = $this->checkAuthNotify($notifyId);
		
		if($this->repository->updateNotify($notify, ['seen' => 1]) === true) {
			return $notify;
		}
		
		throw new CustomQueryErrorException();
	}
	
	
	/**
	 * delete notify by id
	 *
	 * @param string $notifyId
	 * @return bool
	 * @throws CustomNotFoundException
	 * @throws CustomNotifyNotFoundException
	 * @throws CustomQueryErrorException
	 * @throws CustomUnAuthorizedException
	 */
	public function delete(string $notifyId)
	{
		$notify = $this->checkAuthNotify($notifyId);
		if($this->repository->deleteNotify($notify) === true) {
			return true;
		}
		
		throw new NotFoundHttpException();
	}
	
	
	/**
	 *  check validate notify_id and notify belong to logged user
	 *
	 * @param string $notifyId
	 * @return JobsLogModel
	 * @throws CustomNotFoundException
	 * @throws CustomNotifyNotFoundException
	 * @throws CustomUnAuthorizedException
	 */
	private function checkAuthNotify(string  $notifyId) : JobsLogModel
	{
		$notify = $this->repository->getNotifyByIdWithSelectKeys($notifyId, array_merge($this->repository->keysShowInResponseLog, ['user']));
		if (! $notify) {
			throw new CustomNotifyNotFoundException();
		}
		
		//check if notify not the user return error
		$user = Auth::guard('users')->user();
		if($user->id != $notify->user['id']) {
			throw new CustomUnAuthorizedException();
		}
		
		return $notify;
	}
	
}
