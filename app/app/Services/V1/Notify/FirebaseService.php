<?php namespace App\Services\V1\Notify;


use App\CustomStuff\Classes\SendFireBaseNotification;
use App\Exceptions\CustomNotFoundException;
use App\Jobs\SendNotificationJob;
use App\Models\Repositories\Booking\BookingRepository;
use App\Models\Repositories\Contact\ContactRepository;
use App\Models\Repositories\Contact\ContactUserRepository;
use App\Models\Repositories\Language\LanguageRepository;
use App\Models\Repositories\Language\TranslateRepository;
use App\Models\Repositories\User\UserRepository;
use App\Services\ServiceBase;
use App\Services\V1\Language\TranslateService;


class FirebaseService extends ServiceBase
{
    protected $userRepository;
    protected $contactRepository;
    protected $languageRepository;
    protected $bookingRepository;
    private $countUserHasToken = 0;
    protected $translateService;
	private $tokens = [];
	
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
		$this->contactRepository  = app(ContactRepository::class);
		$this->languageRepository = app(LanguageRepository::class);
		$this->bookingRepository  = app(BookingRepository::class);
		$this->translateService = app(TranslateService::class);
    }

    /**
	 * get message from ds then translate ds params in turpal
	 * find user firebase token and then notify translate message
	 * to user  firebase token
	 *
     * @param array $params
     * @return array
     * @throws CustomNotFoundException
     */
    public function sendNotifyToUser(array $params)
    {
        //get contact
        $contact = $this->getFireBaseContact();
        
        //retrieve user by contacts
        $user = $this->userRepository->getUserWithContactUserId($params['user_id'], $contact->id)->first();
        if(! $user || $user->contactUser->count() == 0) {
            throw new CustomNotFoundException('firebase token for some users not Found');
        }
        
		$this->tokens =  array_unique($user->contactUser->pluck('value')->toArray()); //set user token
		$this->executeNotify($params, $user);
		
        return [
            'message' => "add to queue"
        ];
    }
    
	/**
	 * this function send notification to firebase by create job
	 * for send to every user create one job
	 * @param array $params
	 * @return array
	 * @throws CustomNotFoundException
	 */
	public function notifyByBookingId(array $params)
	{
		//get booking user ids
		$userIds = $this->getBookingUserIds($params['booking_id']);
		
		//retrieve users by contacts
		$users = $this->userRepository->getUserWithContactUserIds($userIds, $this->getFireBaseContact()->id)->get();
		
		//for every user fire job
		foreach($users as $user){
			if($user->contactUser->count() != 0) {
				$this->tokens = array_unique($user->contactUser->pluck('value')->toArray()); //set user token
				$this->executeNotify($params, $user);
				$this->countUserHasToken += $user->contactUser->count();
			}
		}
		
		return [
			'message' => "send to " . $this->countUserHasToken . " user"
		];
	}
	
	
	/**
	 * send notification by token
	 * @param array $params
	 * @throws CustomNotFoundException
	 */
	public function notifyByFirebaseToken(array $params)
	{
		$contact = $this->getFireBaseContact();
		$contactUserRepository = app(ContactUserRepository::class);
		$user = $contactUserRepository->retrieveUserByContactUser($params['firebase_token'], $contact->id);
		$this->tokens = (array) $params['firebase_token'];
		$this->executeNotify($params, $user);
	}
	
	
	/**
	 * add notification to Job
	 * @param array $params
	 * @param       $user
	 */
	private function executeNotify( array $params, $user)
	{
		$wrapperParams = $params;
		//get userLanguage
		$params = $this->translateDsParams($params,$user);
		
		//get notifyClass object to send for SendNotificationJob
		$notifyClass = $this->getNotifyClass($params,$user);
		//params send wrapper request data as extra params for log in mongo
		$extraParams = [
			'tokens' => $this->tokens,
			'wrapperParams' => $wrapperParams
		];
		dispatch(new SendNotificationJob($notifyClass, $extraParams, $this->getRequestPrams()));
	}
	
	/**
	 * return fcm contact record
	 *
	 * @throws CustomNotFoundException
	 */
	public function getFireBaseContact()
	{
		$contactConfig  = config('general.contact_type_keys.fcm');
		//get contact
		$contact = $this->contactRepository->getByKey($contactConfig);
		if(! $contact) {
			throw new CustomNotFoundException();
		}
		return $contact;
	}
	
	/**
	 * return  Booking Users id
	 * @param string $bookingId
	 * @return mixed
	 */
	private function getBookingUserIds(string $bookingId)
	{
		//todo ndea: send this function to booking service
		$bookings= $this->bookingRepository->getBookingByDsId($bookingId);
		$usersIds =  $bookings->pluck('user_id')->toArray();
		return $usersIds;
	}
	
	
	/**
	 * translateDsParams keys to user language in turpal
	 * @param array  $params
	 * @param object $user
	 * @return array
	 */
	private function translateDsParams(array $params, object $user)
	{
		//get userLanguage
		$translateCollection = $this->languageRepository
			->getAllTranslatesWithCountries(
				$user->language_id,
				DEFAULT_DEVICE_TYPE_ID,
				DEFAULT_DEVICE_TYPE_ID,
				$this->getTranslateKeysFromRequest($params)
			);
		
		return $this->translateService->translateWithReplace($params, $translateCollection->translates);
	}
	
	/**
	 * return notifyClass object to send for SendNotificationJob
	 * @param array  $params
	 * @param object $user
	 * @return SendFireBaseNotification
	 */
	private function getNotifyClass(array $params,object $user)
	{
		$notifyClass = new SendFireBaseNotification($params['title']);
		$notifyClass->setMultiTokens($this->tokens)
					->setNotifyBody($params['body'])
					->setUser($user);
		return $notifyClass;
	}
	
	/**
	 * this function get keys form incoming request for use in getLanguageByIdWithTranslates in languageRepository
	 * @param array $requestParams
	 * @return array
	 */
	private function getTranslateKeysFromRequest(array $requestParams) : array
	{
		$keys = [];
		foreach ($requestParams as $param) {
			if (is_array($param)) {
				if (isset($param['key'])) {
					if (isset($param['data'])) {
						foreach($param['data'] as $data) {
							$keys[] = $data;
						}
					}
					$keys[] = $param['key'];
				}
			}
		}
		return $keys;
	}
	
	
	
	/**
	 * get request params to use in notification job
	 * @return array
	 */
	private function getRequestPrams()
	{
		return [
			'request'      => request()->all() ,
			'url'          => request()->fullUrl() ,
			'method'       => request()->getMethod() ,
			'ip'           => request()->getClientIp() ,
			'request_time' => time(),
		];
	}
	
	
}
