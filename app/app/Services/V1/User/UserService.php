<?php namespace App\Services\V1\User;


use App\CustomStuff\Classes\UploadManagement;
use App\Exceptions\CustomNotFoundException;
use App\Exceptions\CustomPermissionDeniedException;
use App\Exceptions\CustomQueryErrorException;
use App\Exceptions\CustomUnAuthenticatedException;
use App\Http\Resources\V1\Language\LanguageResource;
use App\Models\Entities\User\User;
use App\Models\Repositories\Contact\ContactRepository;
use App\Models\Repositories\Language\LanguageRepository;
use App\Models\Repositories\User\UserRepository;
use App\Services\ServiceBase;
use App\Services\V1\Language\LanguageService;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class UserService extends ServiceBase
{
	public function __construct(UserRepository $repository)
	{
		$this->repository = $repository;
	}
	
	
	/**
	 * return user profile and user contact (mobile, ....)
	 *
	 * @return array
	 * @throws CustomUnAuthenticatedException
	 */
	public function profile()
    {
        $guard = Auth::guard('users');
        if(! $guard->check()) {
            throw new CustomUnAuthenticatedException();
        }

        $contactRepository = app(ContactRepository::class);
        $user = $guard->user();
        return [
            'user' => $user,
            'contact' => $contactRepository->allWithContactUser($user->id)
        ];

    }

    /**
     * update user info
     *
     * @param array $params
     * @return User|Authenticatable|null
     * @throws CustomQueryErrorException
     */
    public function update(array $params)
	{
		$user= auth()->user();
		if (isset($params['avatar'])) {
			$params['avatar'] = app(UploadManagement::class)->updateUserAvatar($user,$params['avatar']);
		}
		
		//filter null values - if null from client remove from params for save only changed params to db
		$params = Arr::where($params, function ($value, $key) {
			return !empty($value);
		});
		
		return $this->repository->update($user, $params);
	}
	
	/**
	 * update language by language_id
	 *
	 * @param int $languageId
	 * @return array
	 * @throws CustomQueryErrorException
	 * @throws CustomUnAuthenticatedException
	 */
    public function updateLanguage(int $languageId)
    {
        $user = Auth::guard('users')->user();
        if(! $user) {
            throw new CustomUnAuthenticatedException();
        }

        $languageRepository = app(LanguageRepository::class);
        $language = $languageRepository->getById($languageId);
        $languageService = app(LanguageService::class); //bind languageService to get method
        return [
            'user' => $this->repository->update($user, ['language_id' => $languageId]),
            'language' => new LanguageResource($languageService->getLanguageByIso2WithTranslates($language->iso2))
        ];
    }
}
