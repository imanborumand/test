<?php namespace App\Services\V1\Invoice;


use App\Exceptions\CustomNotFoundException;
use App\Exceptions\CustomResponseException;
use App\Services\ServiceBase;
use Illuminate\Support\Facades\Auth;

class InvoiceService extends ServiceBase
{


    private $user;

    public function __construct()
    {
    	//check if user auth, get languages
    	$auth =  Auth::guard('users')->user();
    	if ($auth) {
			$this->user = $auth->with('language')->first();
		}
    }
	
	
	/**
	 * send request to wrapper by booking id to get invoice of booking
	 *
	 * @param string $bookingId
	 * @return array
	 * @throws CustomNotFoundException
	 * @throws CustomResponseException
	 */
	public function getInvoiceBooking(string $bookingId) : array
	{
		$sendRequestToWrapper = $this->sendRequestToWrapperGetInvoiceBooking($bookingId);
		if($sendRequestToWrapper->status === false) {
			throw new CustomNotFoundException();
		}

        return (array) $sendRequestToWrapper;
	}


    /**
     * send request to wrapper to get invoice of booking
     * @param string $bookingId
     * @return object
     * @throws CustomResponseException
     */
    public function sendRequestToWrapperGetInvoiceBooking( string $bookingId) : object
	{
		return json_decode(doRequest(getCurrentWrapperConfigFromHeader()['base_url'] . 'booking/' . $bookingId . '/invoice?language=' . $this->user->language->iso2 , "GET"));
	}
}
