<?php namespace App\Services\V1\Hotel;

use App\Exceptions\CustomNotFoundException;
use App\Exceptions\CustomResponseException;
use App\Services\ServiceBase;
use function GuzzleHttp\Psr7\build_query;

class HotelService extends ServiceBase
{
	private $msHotelUrl;
	
	public function __construct()
	{
	
	}
	
	
	/**
	 * send request to ms hotel and get hotel
	 *
	 * @param array $params
	 * @return bool|string
	 * @throws CustomNotFoundException
	 * @throws CustomResponseException
	 */
	public function getByName(array $params)
	{
		//check result if status == false means that hotel not found
		$result =  json_decode($this->sendRequestToMsHotel($params), true);
		if(! isset($result['res'])) {
			throw new CustomNotFoundException("hotel not found!");
		}
		return 	$result['res'];
	}
	
	
	/**
	 * send request to ms hotel by curl function
	 *
	 * @param array $params
	 * @return bool|string
	 * @throws CustomResponseException
	 */
	private function sendRequestToMsHotel( array $params)
	{
	
		//$this->msHotelUrl =  env("MS_HOTEL_URL"); //get ms-hotel base url from env //todo get url from env
		$result = doRequestToMs("http://64.227.121.34:8080/GetHotelsByIso2/" . $params['iso2'] . "/" . rawurlencode( $params['name']), "GET");
		//check result and validate json
		if(jsonValidator($result) === true) {
			return $result;
		}
		
		$message = config('default_messages.response.try_again');
		throw new CustomResponseException($message . "!!",400, 9999 );
	}
	
}
