<?php namespace App\Services\V1\Country;

use App\Http\Resources\V1\Country\CountryCollection;
use App\Models\Repositories\Device\DeviceTypeRepository;
use App\Services\ServiceBase;
use App\Models\Repositories\Country\CountryRepository;
use Illuminate\Support\Facades\Auth;

class CountryService extends ServiceBase
{

	public function __construct(CountryRepository $repository)
	{
		$this->repository = $repository;
	}
	
	
	/**
	 * get all countries by iso2 and translated name
	 *
	 * @return CountryCollection
	 */
    public function all()
	{
		$data = $this->returnCountryResult();
        return (new CountryCollection($data));
	}


    /**
     * get one country by iso2 and translated name
	 *
     * @param string $iso2
     * @return array
     */
    public function get(string $iso2)
	{
        return $this->returnCountryResult($iso2);
	}


    /**
     *  create result of country query
	 *
     * @param string|null $iso2
     * @return array
     */
    private function returnCountryResult(string $iso2 = null) : array
    {
        $auth = Auth::guard('users');
        $deviceTypeClass = app(DeviceTypeRepository::class);
        $deviceType = $deviceTypeClass->getDeviceTypeInfo();

        if($auth->check()) {
            return  $this->repository->countriesWithTranslation($auth->user()->language_id, $deviceType['id'], $iso2)->toArray();
        }

        return  $this->repository->countriesWithTranslation(DEFAULT_LANGUAGE_ID, $deviceType['id'], $iso2)->toArray();
    }

}
