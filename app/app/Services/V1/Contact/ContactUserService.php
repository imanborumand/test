<?php namespace App\Services\V1\Contact;

use App\Exceptions\CustomNotFoundException;
use App\Exceptions\CustomQueryErrorException;
use App\Exceptions\CustomResponseException;
use App\Exceptions\CustomUnAuthorizedException;
use App\Exceptions\CustomValidationException;
use App\Jobs\SendUserInfoToWrapperJob;
use App\Models\Entities\User\User;
use App\Models\Repositories\Booking\BookingRepository;
use App\Models\Repositories\Contact\ContactUserRepository;
use App\Services\ServiceBase;
use App\Services\V1\Notify\FirebaseService;
use Carbon\Carbon;

/**
 * Class ContactUserService
 *
 * @package App\Services\V1\Contact
 */
class ContactUserService extends ServiceBase
{
	/**
	 * ContactUserService constructor.
	 *
	 * @param ContactUserRepository $repository
	 */
	public function __construct(ContactUserRepository $repository)
	{
		$this->repository = $repository;
	}
	
	
	/**
	 * store new contact info for user
	 * return contact user info
	 *
	 * @param array $params
	 * @return mixed
	 * @throws CustomQueryErrorException
	 * @throws CustomUnAuthorizedException
	 */
	public function store(array $params)
	{
		$user = auth()->user();
		if(! $user) {
			throw new CustomUnAuthorizedException();
		}
		
		$params =[
			'user_id'      => $user->id ,
			'contact_id'   => $params['contact_id'],
			'value'        => $params['value'],
			'confirmed_at' => Carbon::now() ,
			'status'	   => true ,
		];
		
		$this->sendUserInfoToWrapper($params, $user);
		
		$contactUser = $this->repository->store($params);
		if(! $contactUser) {
			$message = config('default_messages.response.item_created_error');
			throw new CustomQueryErrorException($message);
		}
		return $contactUser;
	}
	
	
	/**
	 *  update contact of user
	 * @param array $params
	 * @param int   $contactUserId
	 * @return mixed
	 * @throws CustomQueryErrorException
	 * @throws CustomNotFoundException
	 */
	public function update(array $params, int $contactUserId)
	{
		$contactUser = $this->repository->update($params,$contactUserId);
		if(! $contactUser) {
			$message = config('default_messages.response.item_updated_error');
			throw new CustomQueryErrorException($message);
		}
		return $contactUser;
	}
	
	/**
	 * delete request mobile for user
	 * user cant not delete first mobile that register
	 * @param int $contactUserId
	 * @return mixed
	 * @throws CustomNotFoundException
	 * @throws CustomQueryErrorException
	 * @throws CustomValidationException
	 */
	public function delete(int $contactUserId)
	{
		$user = auth()->user();
		$deleteContactUser = $this->repository->delete($user,$contactUserId);
		if(! $deleteContactUser) {
			$message = config('default_messages.response.item_deleted_error');
			throw new CustomQueryErrorException($message);
		}
		return $deleteContactUser;
	}
	
	
	/**
	 * send user info [params] to DMC for save info and send welcome notification sms or push notify or every things
	 *
	 * @param array $params
	 * @param User  $user
	 */
	private function sendUserInfoToWrapper(array $params, User $user)
	{
		if (
			!$this->repository->getUserByContactUser($params['contact_id'], $params['value'])
			&& $params['contact_id'] == app(FirebaseService::class)->getFireBaseContact()->id
		) {
			
			$bookingRepository = app(BookingRepository::class);
			$booking = $bookingRepository->getLastBookingOfUser($user->id);
		
			if (! $booking == null) {
				$paramsToWrapper = [
					'booking_id' => $user->id,
					'turpal_user_id' => $booking->id,
					'token' => $params['value']
				];
				dispatch(new SendUserInfoToWrapperJob($paramsToWrapper, request()->header( 'app-name')));
			}
		}
	}
	
	
	/**
	 * send user info [params] to DMC for save info and send welcome notification sms or push notify or every things
	 *
	 * @param array $params
	 * @param User  $user
	 * @throws CustomResponseException
	 */
	public function storeFirebaseTokenInLogin(array $params, User $user)
	{
		$this->repository->storeFirebaseTokenInLogin($params,  $user, app(FirebaseService::class)->getFireBaseContact()->id);
		$paramsToWrapper = [
			'booking_id' => $params['booking_id'],
			'turpal_user_id' => $user->id,
			'token' => $params['firebase_token']
		];
		
		dispatch(new SendUserInfoToWrapperJob($paramsToWrapper, request()->header( 'app-name')));
	}
}
