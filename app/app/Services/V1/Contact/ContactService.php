<?php namespace App\Services\V1\Contact;

use App\Exceptions\CustomNotFoundException;
use App\Exceptions\CustomQueryErrorException;
use App\Models\Repositories\Contact\ContactRepository;
use App\Services\ServiceBase;
use Illuminate\Container\EntryNotFoundException;

/**
 * Class ContactService
 *
 * @package App\Services\V1\Contact
 */
class ContactService extends ServiceBase
{
	
	/**
	 * ContactService constructor.
	 *
	 * @param ContactRepository $repository
	 */
	public function __construct(ContactRepository $repository)
	{
		$this->repository = $repository;
	}
	
	
	/**
	 * get list of contact type
	 * mobile , token ....
	 */
	public function all()
	{
		return $this->repository->all();
	}
	
	/**
	 * store new contact type
	 *
	 * @param array $params
	 * @return mixed
	 * @throws CustomQueryErrorException
	 */
	public function store(array $params)
	{
		$contact = $this->repository->store($params);
		if(! $contact) {
			$message = config('default_messages.response.item_created_error');
			throw new CustomQueryErrorException($message);
		}
		
		return $contact;
	}
	
	
	/**
	 * update request contact type
	 * @param array $params
	 * @param int   $contactId
	 * @return mixed
	 * @throws CustomQueryErrorException
	 */
	public function update(array $params, int $contactId)
	{
		$contact = $this->repository->update($params,$contactId);
		return $contact;
	}
	
	
	/**
	 * delete  contact by contactId
	 * @param int $contactId
	 * @throws CustomQueryErrorException
	 */
	public function delete(int $contactId)
	{
		$delete = $this->repository->delete($contactId);
		if(! $delete) {
			$message = config('default_messages.response.item_deleted_error');
			throw new CustomQueryErrorException($message);
		}
	}
}
