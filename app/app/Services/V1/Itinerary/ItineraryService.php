<?php namespace App\Services\V1\Itinerary;


use App\Exceptions\CustomResponseException;
use App\Models\Repositories\Language\LanguageRepository;

class ItineraryService
{

    /**
	 * get request language info and send request to ds to get Itinerary from ds
     * @param array $params
     * @return array
     * @throws CustomResponseException
     */
    public function getItinerary( array $params)
    {
        $languageRepository = app(LanguageRepository::class);
        $language = $languageRepository->getById($params['language_id']);
        return $this->sendRequestToWrapperGetItinerary($params['booking_id'], $language->iso2);
    }


    /**
     * send request to wrapper to get data
     * @param string $bookingId
     * @param string $languageIso2
     * @return array
     * @throws CustomResponseException
     */
    public function sendRequestToWrapperGetItinerary( string $bookingId, string $languageIso2) : array
    {
        return json_decode(doRequest(getCurrentWrapperConfigFromHeader()['base_url'] . 'itinerary/?booking_id=' . $bookingId . '&language=' . $languageIso2 , "GET"), true);
    }

}
