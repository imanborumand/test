<?php namespace App\Services\V1\Language;


use App\Exceptions\CustomBadRequestException;
use App\Exceptions\CustomNotFoundException;
use App\Exceptions\CustomQueryErrorException;
use App\Models\Entities\Language\Language;
use App\Models\Repositories\Device\DeviceTypeRepository;
use App\Models\Repositories\Language\LanguageRepository;
use App\Models\Repositories\Language\TranslateKeyRepository;
use App\Models\Repositories\Language\TranslateRepository;
use App\Services\ServiceBase;
use Illuminate\Container\EntryNotFoundException;
use Illuminate\Contracts\Foundation\Application;

class LanguageService extends ServiceBase
{
	/**
	 * @var Application|mixed
	 */
	protected $translateRepository;
	/**
	 * @var Application
	 */
	protected $translateKeyRepository;

	protected $deviceTypeRepository;

	public function __construct(LanguageRepository $repository)
	{
		$this->repository = $repository;
		$this->translateRepository = app(TranslateRepository::class);
		$this->translateKeyRepository = app(TranslateKeyRepository::class);
		$this->deviceTypeRepository = app(DeviceTypeRepository::class);
	}

	/**
	 * return list of languages
	 * @return mixed
	 * @throws CustomNotFoundException
	 */
	public function all()
	{
	    $listOfLanguages = $this->repository->allLanguages();
		if ($listOfLanguages) {
			return $listOfLanguages->toArray();
		}

		throw new CustomNotFoundException();
	}


	/**
	 * store new language
	 * @param array $params
	 * @return mixed
	 * @throws CustomQueryErrorException
	 */
	public function store(array $params)
	{
		$store = $this->repository->store($params);
		if(! $store) {
			$message = config('default_messages.response.item_created_error');
			throw new CustomQueryErrorException($message);
		}

		return $store->toArray();
	}


	/**
	 * find Language By Iso2 and return language by it translates
	 *
	 * @param string $languageIso2
	 * @return mixed
	 * @throws CustomNotFoundException
	 */
	public function getLanguageByIso2WithTranslates(string $languageIso2)
	{

        $deviceType = $this->deviceTypeRepository->getDeviceTypeInfo();
        $language = $this->repository->getLanguageByIso2WithTranslates($languageIso2, $deviceType['id'], $deviceType['default_device_setting']['device_type_id']);

        if (!$language) {
			throw new CustomNotFoundException();
		}
		return $language;
	}
	
	
	/**
	 * 	find Language By id and return language by it translates
	 *
	 * @param int $languageId
	 * @return mixed
	 * @throws CustomNotFoundException
	 * @throws EntryNotFoundException
	 */
    public function getLanguageByIdWithTranslates(int $languageId)
    {
        $deviceType = $this->deviceTypeRepository->getDeviceTypeInfo();
        $language = $this->repository->getLanguageByIdWithTranslates($languageId, $deviceType['id'], $deviceType['default_device_setting']['device_type_id']);

        if (!$language) {
            throw new CustomNotFoundException();
        }
        return $language;
    }

}
