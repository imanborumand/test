<?php namespace App\Services\V1\Language;

use App\Exceptions\CustomBadRequestException;
use App\Exceptions\CustomNotFoundException;
use App\Exceptions\CustomQueryErrorException;
use App\Exceptions\CustomResponseException;
use App\Models\Entities\Language\Language;
use App\Models\Entities\Language\Translate;
use App\Models\Repositories\Language\LanguageRepository;
use App\Models\Repositories\Language\TranslateKeyRepository;
use App\Services\ServiceBase;
use App\Models\Repositories\Language\TranslateRepository;
use Illuminate\Container\EntryNotFoundException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

/**
 * Class TranslateService
 *
 * @package App\Services\V1\Language
 */
class TranslateService extends ServiceBase
{
	protected $translateKeyRepository;
	protected $languageRepository;

	public function __construct(TranslateRepository $repository)
	{
		$this->repository = $repository;
		$this->translateKeyRepository = app(TranslateKeyRepository::class);
		$this->languageRepository = app(LanguageRepository::class);
	}

	//get excel_file from request and import data i related tables
	public function importExcel(array $params)
	{
		$excelFile = $params['excel_file'];
		return $this->repository->importExcel($excelFile);
	}


    /**
     * store translates of one language by language id
     *
     * @param array $params
     * @return array
     * @throws CustomBadRequestException
     * @throws CustomNotFoundException
     * @throws CustomQueryErrorException
     * @throws CustomResponseException
     */
	public function storeTranslates(array $params)
	{
		$language = $this->languageRepository->getById($params['language_id']);
		if (! $language) {
			throw new CustomNotFoundException();
		}

		$this->checkDuplicateKeys($params,$language);
		//and values is translate for that language
		//array_keys($params['translates']) return the index of array that is  keys of translates
		//return array that index are ids and values are keys
		$translatesKey = $this->translateKeyRepository->getTranslatesKeyByName(array_keys($params['translates']));
		$this->insertTranslateData($translatesKey,$params);

		//return language by translates
		//array_key get $translatesKeyIds for this function
		return $this->languageRepository->translateWhereKeyInByLanguageId($params['language_id'] , array_keys($translatesKey))->first();
	}

	//check keys of translate in this language for not access to add duplicate key
	// if there is any duplicate key for requested language return exception
	/**
	 * @param array    $params
	 * @param Language $language
	 * @return array
	 * @throws CustomBadRequestException
	 * @throws CustomQueryErrorException
	 */
	private function checkDuplicateKeys( array $params , Language $language)
	{
		$keys = array_keys($params['translates']);
		$translatesKeyIds =array_keys($this->translateKeyRepository->getTranslatesKeyByName($keys));
		if(isset($translatesKeyIds) && count($translatesKeyIds) > 0){
			if ($this->repository->getTranslateByLanguageIdWhereKeysIn($language->id, array_values($translatesKeyIds))->count() > 0) {
				throw new CustomBadRequestException("Some keys are duplicate for this language");
			}
		}else{
			$this->translateKeyRepository->insertNewKeys($keys);
		}

		return $keys;
	}


	/**
	 * bulk insert TranslateData to database
	 * @param array $translatesKey
	 * @param array $params
	 * @throws CustomResponseException
	 */
	private function insertTranslateData(array $translatesKey, array $params)
	{
		$translatesList = [];
		//prepare data that must insert to translate table
		foreach($translatesKey as $keyId => $keyName) {
			$translatesList[] = [
				'language_id'      => $params['language_id'] ,
				'translate_key_id' => $keyId ,
				'device_type_id'   => $params['device_type_id'][0] ,
				'value'            => $params['translates'][$keyName] ,
			];
		}
		$this->repository->insert($translatesList);
	}
	
	
	/**
	 * this function translate and replace text in [] when receive DMC
	 * for example
	 * input like:
	 * "title": {
			"key": "your_welcome_to_new_transfer",
			"data": {
			"name": "iman",
			"country": "iran",
			"city": "tehran"
			}
		}
	 *  text value like:
	 * hello [name], welcome to [country] and city [city]!
	 * result:
	 * hello iman, welcome to iran and city tehran
	 *
	 * @param array      $params
	 * @param Collection $translates
	 * @return array
	 */
	public function translateWithReplace( array $params, Collection $translates) : array
	{
		$result = $params;
		//todo refactor and short codes!!!
		$translates = $translates->toArray();
		
		foreach ($params as $paramKey => $param) {
			//if param has key go to translate
			if (is_array($param) || isset($param['key'])) {
				
				//if param key in translate values go to translate
				if (in_array( $param['key'], Arr::pluck($translates, 'translate_key.key')) ) {
					foreach ($translates as $translatesKey => $translate) {
						
						if($translate['translate_key']['key'] == $param['key']) {
							$replaceArray = [];
							preg_match_all("/\[([^\]]*)\]/i", $translate['value'], $values);
							//check data and translate
							foreach ($values[1] as $translateValue) {
								if(isset($param['data'][$translateValue])) {
									
									foreach($translates as $tran) {
										if($tran['translate_key']['key'] == $param['data'][$translateValue]) {
											$param['data'][$translateValue] = $tran['value'];
										}
									}
									$replaceArray['[' . $translateValue . ']'] = $param['data'][$translateValue];
								}
							}
							$result[$paramKey] = strip_tags(strtr($translate['value'],$replaceArray)); //remove html tags that come from DMC
						}
					}
					
				} else {
					//if param not exists in translates and has key return key value
					$result[$paramKey] = $param['key'];
				}
				
			}
		}
		return (array) $result;
	}
}
