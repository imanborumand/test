<?php namespace App\Services\V1\App;

use App\CustomStuff\Classes\DetectDeviceInfo;
use App\Exceptions\CustomValidationException;
use App\Http\Resources\V1\Language\LanguageResource;
use App\Models\Repositories\Contact\ContactRepository;
use App\Models\Repositories\Language\LanguageRepository;
use App\Models\Repositories\Notify\NotifyRepository;
use App\Models\Repositories\User\UserRepository;
use App\Services\ServiceBase;
use App\Services\V1\Language\LanguageService;
use Carbon\Carbon;
use Dotenv\Exception\ValidationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;


/**
 * Class AppService
 *
 * @package App\Services\V1\App
 */
class AppService extends ServiceBase
{
	/**
	 * @var array|string|null
	 */
	protected $deviceType;

    //cache name of language and version use in LanguageObserver
    public $cacheTranslateVersionKey = 'translateVersionCache';

    protected $detectDeviceInfoClass = null;

    protected $user = null;

    protected $versionInfo = [];


    /**
     * AppService constructor.
     *
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
	{
		$this->repository = $repository;
        $this->detectDeviceInfoClass = app(DetectDeviceInfo::class);
	}


	/**
	 * check user is login or not and then return related information
	 *
	 * @return array
	 * @throws CustomValidationException
	 */
	public function init()
	{
		$auth = Auth::guard('users');
		if($auth->check()) {
			$this->user = $auth->user();
			return $this->userLoginInit();
		}

		return $this->guestInit();
	}


	/**
	 * when user is login return
	 * 1-user profile
	 * 2-version data  for check force update
	 * 3-language with translate
	 *
	 * @return array
	 * @throws CustomValidationException
	 */
	private function userLoginInit()
	{
		$contactRepository = app(ContactRepository::class);
        $TranslateCache = $this->getTranslateCache();
		return [
			'versions'=> $TranslateCache['versions'],
			'notifyInfo' => $this->getUserNotifyInfo($this->user->id),
			'user'=>[
				'id'         => $this->user->id,
				'first_name' => $this->user->first_name,
				'last_name'  => $this->user->last_name,
				'avatar'     => $this->user->avatar,
				'contact'    => $contactRepository->allWithContactUser($this->user->id),
				'language'   => $TranslateCache['translates'],
			],
		];
	}


	/**
	 * when user is not login
	 * return version info and languages data
	 *
	 * @return array
	 * @throws CustomValidationException
	 */
	private function guestInit()
	{
	    $TranslateCache = $this->getTranslateCache();
		return [
			'versions' => $TranslateCache['versions'],
			'notifyInfo' => $this->getUserNotifyInfo(),
			'countries' =>[],
			'default_language' => $TranslateCache['translates'],
			'all_languages'	   => $TranslateCache['all_languages'],
		];
	}


	/**
	 * return notifyInfo
	 *
	 * @param int|null $userId
	 * @return array
	 */
	public function getUserNotifyInfo(int $userId = null) : array
	{
		$total = 0;
		$unseen = 0;

		if ($userId != null) {
			$notifyRepository = app(NotifyRepository::class);
			$total = $notifyRepository->getUserTotalNotifyCount($userId);
			$unseen = $notifyRepository->getUserNotSeenNotifyCount($userId);
		}

		return [
			'total'   => $total,
			'unseen' => $unseen,
		];
	}


    /**
	 * detect user device info and return version info
     * @return array|mixed
     */
    private function getVersionsDevice()
	{
        $deviceInfoInRequest = $this->detectDeviceInfoClass->deviceInfo();
        //if os not valid return empty array
		if (! isset($deviceInfoInRequest['device_os']) || strlen($deviceInfoInRequest['device_os']) < 2) {
            return [];
		}

		//check os validate
        $appVersions = config('general.app_version');
        if (is_array($appVersions) && array_key_exists($deviceInfoInRequest['device_os'], $appVersions)) {
            $this->versionInfo = $appVersions[$deviceInfoInRequest['device_os']];
            return $this->versionInfo;
        }
        return [];
	}


    /**
     * cache languages and translates
     *
     * @return mixed
     * @throws CustomValidationException
     */
    private function getTranslateCache()
    {
        $this->getVersionsDevice();
        if(count($this->versionInfo) ==0 ){
        	throw new CustomValidationException('please send device os and device type');
		}
        $this->deviceType = $this->detectDeviceInfoClass->getDeviceTypeByIdFromCache(request()->all()['device_info']['device_type']['id']);
        $deviceTypeId = $this->deviceType['device_setting']['device_type_id'];
        $languageId = DEFAULT_LANGUAGE_ID;

        if($this->user != null) {
            $languageId = $this->user->language_id;
            if($languageId == null || $languageId == 0) {
                throw new ValidationException("user language id not found!");
            }
        }

        Cache::forget($this->cacheTranslateVersionKey . $languageId . $deviceTypeId); //todo remove after testing

        //this cache forget when language model update - forget function in LanguageObserver
        return Cache::get($this->cacheTranslateVersionKey . $languageId . $deviceTypeId  , function() use ($deviceTypeId, $languageId) {
            $languageService = app(LanguageService::class);
            $return = [
                'versions' => $this->translateVersionMessageAndTitleKeys($deviceTypeId, $languageId),
                'all_languages' => $languageService->all(),
                'translates' => new LanguageResource($languageService->getLanguageByIdWithTranslates($languageId)),
            ];

            Cache::put($this->cacheTranslateVersionKey . $languageId . $deviceTypeId , $return, Carbon::now()->addDays(3));
            return $return;
        });
    }


    /**
     * translate message and title key in version array to language user
     *
     * @param int $deviceTypeId
     * @param int $languageId
     * @return array|mixed
     * @throws CustomValidationException
     */
    private function translateVersionMessageAndTitleKeys(int $deviceTypeId, int $languageId)
    {
        $languageRepository = app(LanguageRepository::class);
        $languageIso2 = $languageRepository->getById($languageId);
        if(!$languageIso2) {
            throw new CustomValidationException("language not found!");
        }

        $versionMessageTitleKeys = ['app_version_message', 'app_version_title'];
        $queryGetTranslates = $languageRepository->getLanguageByIdWithTranslates($languageIso2->id, $deviceTypeId, DEFAULT_DEVICE_TYPE_ID, $versionMessageTitleKeys);

        if($queryGetTranslates) {
            $translates = $queryGetTranslates->translates->toArray();

            if(count($translates) != 0) {
                foreach($translates as $translate) {
                    if(in_array($translate['translate_key']['key'], $versionMessageTitleKeys)) {
                        $this->versionInfo[$translate['translate_key']['key']] = $translate['value'];
                    }
                }
            }
        }
        return  $this->versionInfo;
    }




}
