<?php namespace App\Services\V1\Booking;


use App\Exceptions\CustomBadRequestException;
use App\Exceptions\CustomResponseException;
use App\Exceptions\CustomUnAuthenticatedException;
use App\Exceptions\CustomValidationException;
use App\Models\Repositories\Booking\BookingRepository;
use App\Models\Repositories\Contact\ContactRepository;
use App\Models\Repositories\Contact\ContactUserRepository;
use App\Services\ServiceBase;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BookingService extends ServiceBase
{
	protected $user;

	public function __construct(BookingRepository $repository)
	{
		$this->user = Auth::guard('users')->user();
		$this->repository = $repository;
	}
	
	/**
	 * return all bookings of one user
	 * send mobile to wrapper for find bookings
	 *
	 * @return array
	 * @throws CustomBadRequestException
	 * @throws CustomResponseException
	 * @throws CustomValidationException
	 */
	public function all()
	{
        $configMobile = config('general.contact_type_keys.mobile'); //get mobile key form general config
        $contactRepository = app(ContactRepository::class);
        $this->user = $this->user->withFirstContact($contactRepository->getByKey($configMobile)->id);

		return $this->sendRequestToWrapperGetBookings();
	}
	
	/**
	 * get one booking by booking id
	 * get mobile and name from users table and add to request
	 * send to wrapper
	 *
	 * @param string $bookingId
	 * @return array
	 * @throws CustomResponseException
	 * @throws CustomValidationException
	 */
	public function getBooking(string $bookingId)
	{
		//get first mobile in contact_user table //this mobile saved in login step
		$contactUserMobile = app(ContactUserRepository::class)->getUserMobile($this->user);
		$params = [
			'mobile' => $contactUserMobile->vaule,
			'booking_id' => $bookingId,
			'name' => urlencode($this->user->ds_name)
		];

		return $this->sendRequestToWrapperGetBooking($params);
	}

	/**
	 * send request to wrapper to get booking data by booking_id
	 *
	 * @param array $params
	 * @return array
	 * @throws CustomResponseException
	 * @throws CustomValidationException
	 */
	public function sendRequestToWrapperGetBooking(array $params) : array
	{
		$params = array_merge(['app_state' => env('APP_STATE')], $params); //add app_state for check and save in wrapper
		$result = json_decode(doRequest(getCurrentWrapperConfigFromHeader()['base_url'] . 'booking' . '?' . http_build_query($params), "GET"), true);
		if($result['status'] != true) {
			$message = config('default_messages.response.booking_not_found');
			throw new NotFoundHttpException($message);
		}
		return (array) $result;
	}

	/**
	 *    send request to wrapper to get one bookings data
	 *
	 * @return array
	 * @throws CustomBadRequestException
	 * @throws CustomResponseException
	 * @throws CustomValidationException
	 */
	public function sendRequestToWrapperGetBookings() : array
	{
	    $mobileContact =  $this->user->contactUser->first();
	    if(! $mobileContact) {
			$message = config('default_messages.response.try_again');
	        throw new CustomBadRequestException($message);
        }

		return json_decode(doRequest(getCurrentWrapperConfigFromHeader()['base_url'] . 'bookings' . '?mobile=' . $mobileContact->value, "GET"), true);
	}

	/**
	 * get mobile for auth user  anb ds booking id from request
	 * then send this params to login ds root
	 * and insert booking info to user booking table
	 *
	 * @param string $booking_id
	 * @return array
	 * @throws CustomBadRequestException
	 * @throws CustomResponseException
	 * @throws CustomUnAuthenticatedException
	 */
	public function addBooking(string $booking_id)
	{
		$user = Auth::guard('users')->user();
		if(! $user) {
			throw new CustomUnAuthenticatedException();
		}
		$mobile = app(ContactUserRepository::class)->getUserMobile($user);
		$params = [
			'booking_id' => $booking_id ,
			'name'       => $user->ds_name,
			'mobile'	 => $mobile->value,
		];
		return $this->repository->addBooking($user, $params);
	}

}
