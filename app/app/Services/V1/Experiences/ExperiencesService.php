<?php namespace App\Services\V1\Experiences;

use App\Exceptions\CustomResponseException;
use App\Models\Repositories\Language\LanguageRepository;
use App\Services\ServiceBase;
use Illuminate\Support\Facades\Auth;

/**
 * Class ExperiencesService
 *
 * @package App\Services\V1\Experiences
 */
class ExperiencesService extends ServiceBase
{
	/**
	 * @var
	 */
	protected $user;


	/**
	 * ExperiencesService constructor.
	 */
	public function __construct()
	{
		$this->user = Auth::guard('users')->user();
	}


    /**
	 * get language iso2 and send request params  to Wrapper and get experiences
     * @param array $params
     * @return mixed
     * @throws CustomResponseException
     */
	public function get(array $params)
	{
		$params = $this->getLanguageIso2($params);
		return $this->sendRequestToWrapperStoreExperience($params);
	}


	/**
	 * send Request To Wrapper experiences route
	 * @param array $params
	 * @return mixed
	 * @throws CustomResponseException
	 */
	private function sendRequestToWrapperStoreExperience(array $params)
	{
		return json_decode(doRequest(getCurrentWrapperConfigFromHeader()['base_url'] .'experiences'. '?' . http_build_query($params) , "GET"), true);
	}


	/**
	 * @param $params
	 * @return mixed
	 */
	private function getLanguageIso2( $params)
	{
		if(isset($params['language_id'])) {
			$params['language'] = app(LanguageRepository::class)->getById($params['language_id'])->iso2;
		}
		return $params;
	}

}
