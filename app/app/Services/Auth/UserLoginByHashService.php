<?php namespace App\Services\Auth;


use App\CustomStuff\Traits\CurlTrait;
use App\CustomStuff\Traits\DeviceDetectByUserAgent;
use App\Exceptions\CustomBadRequestException;
use App\Exceptions\CustomNotFoundException;
use App\Exceptions\CustomResponseException;
use App\Exceptions\CustomValidationException;
use App\Services\ServiceBase;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Cache;
use Laravel\Passport\Bridge\UserRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UserLoginByHashService
 *
 * @package App\Services\Auth
 */
class UserLoginByHashService extends ServiceBase
{
	use DeviceDetectByUserAgent,CurlTrait;

	/**
	 * UserLoginByHashService constructor.
	 *
	 * @param UserRepository $repository
	 */
	public function __construct( UserRepository $repository)
	{
		$this->repository = $repository;
	}
	
	
	/**
	 *
	 *   this function use DeviceDetectByUserAgent to detect user os type
	 *   then redirect user to app Store or googlePlay to download app
	 *
	 *
	 */
	public function redirectToStore()
	{
		//os from browser header and redirect to store
//		$os = strtolower($this->osInfo());
//		if($os == ANDROID_DEVICE_NAME || $os == IPHONE_DEVICE_NAME) {
//			$url = config( 'general.app_version.' . $os . '.store_url' );
//
//			return redirect($url);
//		}
		
		//if type not detect device type show apple and android dl link
		$appVersion = config( 'general.app_version');

		return view('v1/redirect_app', compact('appVersion'));
	}


	/**
	 * get decode ds params form request then get related wrapper from config
	 * and request to wrapper to login user and return user and booking info
	 *client get this function response and can send request to selectBooking api
	 *to save other user information such as name nationality in database
	 *
	 * @param array $params
	 * @return array
	 * @throws CustomNotFoundException
	 */
	public function loginByHash(array $params)
	{
		$wrapperConfig = config('wrapper_config.'. $params['app_name']);
		if(! isset($wrapperConfig)) {
			throw new CustomNotFoundException('wrapper config not found');
		}

		$userLoginService = app(UserLoginService::class);

		// put cache name by wrapper value to cache to use in loginService->login method
		Cache::put($userLoginService->wrappersCacheName . $params['mobile'], [$params['app_name'] => $wrapperConfig], Carbon::now()->addSeconds(5));

		//send request for login
        return $userLoginService->login($params);
	}



}
