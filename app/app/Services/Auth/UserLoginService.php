<?php namespace App\Services\Auth;


use App\CustomStuff\Traits\CurlTrait;
use App\Exceptions\CustomBadRequestException;
use App\Exceptions\CustomNotFoundException;
use App\Exceptions\CustomQueryErrorException;
use App\Exceptions\CustomResponseException;
use App\Exceptions\CustomUnAuthenticatedException;
use App\Exceptions\CustomUnAuthorizedException;
use App\Http\Resources\V1\Language\LanguageResource;
use App\Models\Repositories\Booking\BookingRepository;
use App\Models\Repositories\Contact\ContactRepository;
use App\Models\Repositories\Contact\ContactUserRepository;
use App\Models\Repositories\Country\CountryRepository;
use App\Models\Repositories\User\UserRepository;
use App\Services\ServiceBase;
use App\Services\V1\App\AppService;
use App\Services\V1\Booking\BookingService;
use App\Services\V1\Contact\ContactUserService;
use App\Services\V1\Language\LanguageService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class UserLoginService extends ServiceBase
{
	use CurlTrait;

	private $languageId = 0;
	private $mobile = 0;
	public $wrappersCacheName = 'wrappers';
	private $user;
	private $mobileKey;
	private $contactRepository;
    private $contactUserRepository;
    private $contactUser;
    private $contact;
    private $foundNumbers = 0; //count off DMC[wrapper] that found mobile number in check-mobile step
	private $successResponse = []; // all succeeded response from wrappers save to this var

	public function __construct(UserRepository $repository)
	{
		$this->repository = $repository;
        $this->mobileKey = config('general.contact_type_keys.mobile'); //get mobile key form general config
        $this->contactRepository = app(ContactRepository::class);
        $this->contactUserRepository = app(ContactUserRepository::class);
	}


	/**
	 * receive mobile number and check by wrapper
	 *
	 * @param array $params
	 * @return array
	 * @throws CustomNotFoundException
	 */
	public function checkMobile(array $params)
	{
		$this->mobile =  (int) $params['mobile'];
		$this->languageId = (int) $params['language_id'];

        $contact = $this->contactRepository->getByKey($this->mobileKey);
		$user = $this->contactUserRepository->getUserByContactUser($contact->id, $this->mobile); //check user in turpal Database
		if ($user) {
			return $this->checkMobileResponse();
		}

		//if true that means one or more have mobile then response or
		if ($this->sendRequestToWrapperCheckMobile()) {
			return $this->checkMobileResponse();
		}

		return $this->checkMobileResponse('AND');
	}


	/**
	 * login step
	 * this function also use in UserLoginByHashService Class every change to this function checked in UserLoginByHashService->loginByHash function
	 *
	 * @param array $params
	 * @return array
	 * @throws CustomBadRequestException
	 */
	public function getBookings(array $params)
	{
		$login = $this->sendRequestToWrapperLogin($params);
		if (count($login) === 0) {
			$message = config('default_messages.response.try_again');
			throw new CustomBadRequestException($message);
		}
		return array_merge(['bookings' => $login]);
	}
	
	/**
	 * after login user click on one booking item
	 * this func get booking_id and wrapper_key to send request to get full booking info and save other params like nationality and confirm mobile number
	 *
	 * @param array $params
	 * @return mixed
	 * @throws CustomBadRequestException
	 * @throws CustomNotFoundException
	 * @throws CustomQueryErrorException
	 * @throws CustomResponseException
	 * @throws CustomUnAuthenticatedException
	 */
	public function login(array $params)
	{
        $login = app(BookingService::class)->sendRequestToWrapperGetBooking($params);
		$user = $this->userObjectByToken($params, (object) $login);
		if(! $user) {
			throw new CustomUnAuthenticatedException();
		}
		if(! isset($login['data']['booking'])) {
		    throw new CustomNotFoundException("booking not found!");
        }

        $booking = $login['data']['booking'];
		$this->setUserInfo($user['user'],$booking, $params);

		
		app(BookingRepository::class)->storeBooking($user['user'], (object) $booking); //save booking and nationality and confirmed_at store in storeSelected route
        return array_merge(['booking' => (array) $login['data']], $user);
	}
 
	/**
	 * logout user
	 * this route receive token user and delete token by logout user
	 * for not send token if user logout from turpal app
	 * @param array $params
	 * @return array
	 * @throws CustomNotFoundException
	 * @throws CustomQueryErrorException
	 * @throws CustomUnAuthorizedException
	 */
	public function logout( array $params)
	{
		$user = Auth::guard('users')->user();
		if (! $user) {
			throw new CustomUnAuthorizedException();
		}

		$query = $this->contactUserRepository->getContactUserByValue($user, $params['firebase_token']);
		if (! $query) {
			throw new CustomNotFoundException();
		}
  
  
		if (! $query->delete()) {
			throw new CustomQueryErrorException();
		}

		return ["exit" => true];
	}
 
 
	/**
	 * save user nationality and  ds name
	 * and if client mobile is equal to ds mobile, set confirmed_at time for user contact
	 *
	 * @param object $user
	 * @param array  $booking
	 * @param array  $params
	 * @throws CustomQueryErrorException
	 * @throws CustomResponseException
	 */
	private function setUserInfo(object $user, array $booking, array $params)
	{
		//save nationality
		$country = app(CountryRepository::class )->getByIso2($booking['source_country']);
		if($country) {
			$user->nationality_id = $country->id;
		}
  
		$user->ds_name = $booking['name']; //save ds_name
		if (! $user->save()) {
			throw new CustomQueryErrorException();
		}

  
		//if mobile form wrapper not equal null change confirmed_at to now
		if(isset($params['mobile']) &&  $params['mobile'] == $booking['mobile']) {
			$contactUserMobile               = app( ContactUserRepository::class )->getUserMobile( $user );
			$contactUserMobile->confirmed_at = Carbon::now();
			//save contactUser
			if( !$contactUserMobile->save() ) {
				$message = config( 'default_messages.response.try_again' );
				throw new CustomResponseException( $message , 409 , 4009 );
			}
		}

		//send firebase token that get from this step to wrapper for save user_id turpal and user token and send welcome notify
		if ($params['firebase_token'] && strlen($params['firebase_token'] ) > 0 ) {
			app(ContactUserService::class)->storeFirebaseTokenInLogin($params, $user);
		}
	}

	/**
	 * create response for check mobile step
	 *
	 * @param string $type
	 * @return array
	 */
	private function checkMobileResponse(string $type = 'OR') : array
	{
		$languageService = app(LanguageService::class);
		return [
			'check_type' => $type,
			'mobile'     => $this->mobile,
			'language'   => new LanguageResource($languageService->getLanguageByIdWithTranslates($this->languageId))
		];
	}


	/**
	 * create contact_user and user model and return user object by token
	 * to use in login function
	 *
	 * @param array  $params
	 * @param object $login
	 * @return array
	 * @throws CustomBadRequestException
	 * @throws CustomQueryErrorException
	 */
	public function userObjectByToken(array $params, object $login) : array
	{
		$this->checkUserAndContactUserLogin($params , $login);
		$user = Auth::loginUsingId($this->user->id);

		//if params lan not equal user lang change user lang and save it
		if ($params['language_id'] != $user->language_id) {
			$user->language_id = (int) $params['language_id'];

			if (! $user->save()) {
				throw new CustomQueryErrorException();
			}
		}
  
		return [
			'user'       => $user,
			'notifyInfo' => app(AppService::class)->getUserNotifyInfo($user->id), //return user notify info from mongo
			'token'      => [
				'access' => $this->user->createToken('Turpal')->accessToken,
				'type'   => 'Bearer'
			]
		];
	}


	/**
	 * send request to all wrappers in check mobile step
	 * if one wrappers (DS) return true (find mobile) return true else return false
	 * if return equal true -> response OR else response AND
	 *
	 * @return mixed
	 * @throws CustomNotFoundException
	 */
	private function sendRequestToWrapperCheckMobile() : bool
	{
		Cache::forget($this->wrappersCacheName . $this->mobile); //forget key for create new cache if exist
		$results = $this->doRequestToMultiWrapperInLoginStep(getWrapperConfig(), ['mobile' => $this->mobile], 'check_mobile'); //send request to all wrappers
		if(count($results) == 0) {
			throw new CustomNotFoundException();
		}

		try {
			return $this->checkWrapperResponses($results);
		} catch(\Exception $exception) {
			return false;
		}
	}


	/**
	 * send request to wrapper in login step
	 *
	 * @param array $params
	 * @return mixed
	 */
	private function sendRequestToWrapperLogin(array  $params) : array
	{
		$response = [];
		try {
			$cache = Cache::get($this->wrappersCacheName . $params['mobile']);
			$wrappers = getWrapperConfig();
			//separate wrappers name when saved in check mobile step
			if ($cache) {
				$wrappers = array_intersect_key($wrappers, $cache);
			}

			//send request to wrappers
			$result = $this->doRequestToMultiWrapperInLoginStep($wrappers, $params, 'login');
			if (count($result) == 0) {
				throw new NotFoundHttpException('Booking not found');
			}

			//create struct
			foreach ($result as $item) {
				if ($item['status_code'] === 200) {
					$response[] = [
						'app_name' => $item['wrapper'],
						'bookings_found' => json_decode($item['result'], true)['data']['bookings']
					];
				}
			}

			if (count($response) == 0) {
				throw new NotFoundHttpException('Booking not found!');
			}

		} catch(\Exception $exception) {
			throw new NotFoundHttpException();
		}
		return $response;
	}


	/**
	 *  check user and contact user, if not exists create models
	 *
	 * @param array  $params
	 * @param object $login
	 * @throws CustomBadRequestException
	 */
    private function checkUserAndContactUserLogin(array $params, object $login)
    {
        $this->mobile =  $params['mobile'];

        $this->contact = $this->contactRepository->getByKey($this->mobileKey); //get contact by mobile key
        if(! $this->contact) {
			throw new CustomBadRequestException( "this contact not found!");
        }
        //if contact_user not found, by mobile create new user and create new contact for created user
        $this->contactUser = $this->contactUserRepository->getUserByContactUser($this->contact->id, $this->mobile);
        if (! $this->contactUser) {

        	//save user and contact by type mobile
            DB::transaction(function() use ($params,  $login) {

                $this->user = $this->repository->create([
                    'language_id' => $params['language_id'],
                ]);

                $this->contactUser = $this->contactUserRepository->firstOrCreate([
                     'contact_id' => $this->contact->id,
                     'user_id'    => $this->user->id,
                     'value' 	  => $this->mobile,
                ]);
            });

            //send user_id to wrapper for save this params
        } else {
            //if contactUser exists get user from relation of contactUser to use login
            $this->user =  $this->contactUser->user;
        }
        
    }
    
	/**
	 * separate success response and save in cache
	 * for use in login step - only request to this wrappers
	 *
	 * @param array $results
	 * @return bool
	 */
	private function checkWrapperResponses(array $results)
	{
		foreach($results as $item) {
			if($item['status_code'] === 200) {
				$wrapperResult = json_decode($item['result'], true);

				if($wrapperResult['status'] === true) {
					if(isset($wrapperResult['data']['exists']) and $wrapperResult['data']['exists'] === true) {
						$this->successResponse[$item['wrapper']] = $item;
						$this->foundNumbers ++;
					}
				}
			}
		}
  
		//if found number big of 1 that means one DS have user mobile number and send OR to front
		if($this->foundNumbers > 0) {
			Cache::put($this->wrappersCacheName . $this->mobile, $this->successResponse, Carbon::now()->addMinutes(5)); //add to redis
			return true;
		}
  
		return false;
	}
}


