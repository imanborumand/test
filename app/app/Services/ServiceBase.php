<?php namespace App\Services;


use App\Exceptions\CustomNotFoundException;

/**
 * Class ServiceBase
 * all services must extend this abstract class for user default functions
 *
 * @package App\Services
 */
abstract class ServiceBase
{
	/**
	 * @var null
	 */
	protected $repository = null;
	
	
	/**
	 * get one model by Id
	 *
	 * @param int $id
	 * @return mixed
	 * @throws CustomNotFoundException
	 */
	public function getById(int $id)
	{
		$item =  $this->repository->getById($id);
		if (! $item) {
			throw new CustomNotFoundException();
		}
		return $item;
	}

	/**
	 * get one model by relations
	 * @param array    $params
	 * @param int|null $id
	 * @param bool     $paginate
	 * @return mixed
	 */
	public function getWith(array $params, int $id = null , bool $paginate = false)
	{
		return $this->repository->getWith($params, $id, $paginate);
	}
	
	/**
	 * @param string $device
	 * @return string
	 */
	public function setDevice(string  $device)
	{
		return $this->device = $device;
	}
	
}
