<?php namespace App\Http\Middleware;

use App\CustomStuff\Traits\RequestResponseLogTrait;
use App\Models\Entities\User\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LogRequestResponseMiddleware
{
    use RequestResponseLogTrait;
    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
		$data = $response->getData();
        $this->doLogResponseAndRequest($data); //add response and request to log
    }

}
