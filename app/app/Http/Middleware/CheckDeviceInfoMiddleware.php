<?php namespace App\Http\Middleware;

use App\CustomStuff\Classes\DetectDeviceInfo;
use Closure;
use Illuminate\Http\Request;

/**
 * Class CheckDeviceInfoMiddleware
 * if you can get user device info from App\CustomStuff\Classes\DetectDeviceInfo add this middleware to your route
 *
 * @package App\Http\Middleware
 */
class CheckDeviceInfoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param Closure                   $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $deviceInfo = new DetectDeviceInfo();
        $request[DEVICE_INFO_KEY_IN_REQUEST] = $deviceInfo->deviceInfo();
        return $next($request);
    }
}
