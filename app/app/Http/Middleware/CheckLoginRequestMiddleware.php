<?php

namespace App\Http\Middleware;

use App\Exceptions\CustomInternalErrorException;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

class CheckLoginRequestMiddleware
{
	
	
	/**
	 * Handle an incoming request.
	 *
	 * @param Request $request
	 * @param Closure $next
	 * @return mixed
	 * @throws CustomInternalErrorException
	 */
	
	public function handle($request, Closure $next)
	{
		$hashKey = base64_encode($request->url() . $request->ip());
		if (Cache::get($hashKey)) {
			throw new CustomInternalErrorException("only 1 request in 10 second");
		}
		
		Cache::add($hashKey, $hashKey, Carbon::now()->addSeconds(15));
		
		return $next($request);
	}
	
	
	
}
