<?php

namespace App\Http\Resources\V1\Language;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LanguageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
		return [
			'id' => $this->id,
			'status' => $this->status,
			'iso2' => $this->iso2,
			'title' => $this->title,
			'direction' => $this->direction,
			'flag' => (string) $this->flag,
			'translates' => $this->getTranslates($this->translates)
		];
    }


    private function getTranslates($translates)
	{
		$data = [];
		if(isset($translates) && count($translates) > 0) {
			foreach($translates as $translate){
			    if(isset($translate->translateKey->key)) {
				    $data = array_merge($data,[$translate->translateKey->key => $translate->value]);
                }
			}
		}
		return $data;
	}
}
