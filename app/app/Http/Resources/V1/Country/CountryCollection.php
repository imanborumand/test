<?php

namespace App\Http\Resources\V1\Country;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CountryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
    	
    	return $this->getData();
        //return parent::toArray($request);
    }
	
	/**
	 * @return array
	 */
	private function getData()
	{
		$collections = [];
		foreach ($this->collection as $collection) {
			$collections[] = [
				'id'   => $collection['id'] ,
				"iso2" => $collection['iso2'],
				"iso3" => $collection['iso3'],
				"name" => $collection['translate_key']['translate'][0]['value'] ??  $collection['name'],
				"idd"  => $collection['idd'],
				"flag" => $collection['flag'],
			];
		}
		return $collections;
	}
	
}
