<?php namespace App\Http\Controllers\Auth\User;


use App\Exceptions\CustomNotFoundException;
use App\Exceptions\CustomValidationException;
use App\Http\Controllers\ControllerBase;
use App\Http\Requests\Auth\User\LoginByHashRequest;
use App\Http\Requests\Auth\User\RedirectToStoreRequest;
use App\Services\Auth\UserLoginByHashService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

/**
 * this controller use when user wants login to mobile app by booking hash [rq code]
 * Class UserLoginByHashController
 *
 * @package App\Http\Controllers\Auth\User
 */
class UserLoginByHashController extends ControllerBase
{

	public function __construct(UserLoginByHashService $service)
	{
		$this->service = $service;
	}
	
	
	/**
	 * this route when call that app not install in user mobile and open browser- get os type from browser and redirect user to store
	 *
	 * @param RedirectToStoreRequest $request
	 * @return array|RedirectResponse|Redirector
	 * @throws CustomValidationException
	 */
	public function redirectToStore(RedirectToStoreRequest $request)
	{
		return $this->service->redirectToStore();
	}
	
	
	/**
	 * when app install in user mobile- app send request this route by has_key [info param] to get booking
	 *
	 * @param LoginByHashRequest $request
	 * @return JsonResponse
	 * @throws CustomNotFoundException
	 */
	public function loginByHash(LoginByHashRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->loginByHash($request->validated()))
					->response();
	}
	
}
