<?php namespace App\Http\Controllers\Auth\User;


use App\Exceptions\CustomBadRequestException;
use App\Exceptions\CustomNotFoundException;
use App\Exceptions\CustomQueryErrorException;
use App\Exceptions\CustomResponseException;
use App\Exceptions\CustomUnAuthenticatedException;
use App\Exceptions\CustomUnAuthorizedException;
use App\Http\Controllers\ControllerBase;
use App\Http\Requests\Auth\User\CheckMobileInLoginRequest;
use App\Http\Requests\Auth\User\UserLoginRequest;
use App\Http\Requests\Auth\User\LoginGetBookingsRequest;
use App\Http\Requests\Auth\User\UserLogoutRequest;
use App\Services\Auth\UserLoginService;
use Exception;
use Illuminate\Http\JsonResponse;

class UserAuthController extends ControllerBase
{

	public function __construct(UserLoginService $service)
	{
		$this->service = $service;
	}


	/**
	 * check mobile number in login
	 * @param CheckMobileInLoginRequest $request
	 * @return JsonResponse
	 * @throws Exception
	 */
	public function checkMobile(CheckMobileInLoginRequest $request)
	{
		return $this->successResponse()
			->setData($this->service->checkMobile($request->validated()))
			->response();
	}


	/**
	 * generate access token for user and login
	 *
	 * @param LoginGetBookingsRequest $request
	 * @return JsonResponse
	 * @throws CustomBadRequestException
	 */
	public function getBookings(LoginGetBookingsRequest $request)
	{
		return $this->successResponse()
			->setData($this->service->getBookings($request->validated()))
			->response();
	}
	
	
	/**
	 * after login user click on one booking item
	 * this func get booking_id and wrapper_key to send request to get full booking info and save other params like nationality and confirm mobile number
	 *
	 * @param UserLoginRequest $request
	 * @return JsonResponse
	 * @throws CustomBadRequestException
	 * @throws CustomNotFoundException
	 * @throws CustomQueryErrorException
	 * @throws CustomResponseException
	 * @throws CustomUnAuthenticatedException
	 */
	public function login(UserLoginRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->login($request->validated()))
					->response();
	}
	
	
	/**
	 * @param UserLogoutRequest $request
	 * @return JsonResponse
	 * @throws CustomNotFoundException
	 * @throws CustomQueryErrorException
	 * @throws CustomUnAuthorizedException
	 */
	public function logout( UserLogoutRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->logout($request->validated()))
					->response();
	}
}
