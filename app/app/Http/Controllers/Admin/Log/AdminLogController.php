<?php namespace App\Http\Controllers\Admin\Log;


use App\Http\Controllers\ControllerBase;
use App\Services\Admin\Log\AdminLogService;
use Illuminate\Http\JsonResponse;

class AdminLogController extends ControllerBase
{
	
	public function __construct(AdminLogService $service)
	{
		$this->service = $service;
	}
	
	
	/**
	 * @return JsonResponse
	 */
	public function responseRequestLog()
	{
		return $this->successResponse()
					->setData($this->service->responseRequestLog())
					->response();
	}
	
	
	/**
	 *  return logs of SendUserInfoToWrapperJob class
	 * @return JsonResponse
	 */
	public function sendUserInfoToWrapper()
	{
		return $this->successResponse()
					->setData($this->service->sendUserInfoToWrapper())
					->response();
	}
	
	
	/**
	 * list of notifications log
	 * @return JsonResponse
	 */
	public function allNotificationsLog()
	{
		return $this->successResponse()
					->setData($this->service->allNotificationsLog())
					->response();
	}
	
	public function seeContactUser()
	{
		return $this->successResponse()
					->setData($this->service->seeContactUser())
					->response();
	}
	
}
