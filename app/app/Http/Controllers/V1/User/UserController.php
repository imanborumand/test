<?php namespace App\Http\Controllers\V1\User;


use App\Exceptions\CustomQueryErrorException;
use App\Exceptions\CustomUnAuthenticatedException;
use App\Http\Controllers\ControllerBase;
use App\Http\Requests\V1\User\UserUpdateLanguageRequest;
use App\Services\V1\User\UserService;
use App\Http\Requests\V1\User\UpdateUserRequest;
use Illuminate\Http\JsonResponse;

class UserController extends ControllerBase
{
	public function __construct(UserService $service)
	{
		$this->service = $service;
	}


    /**
	 * return user profile
	 *
     * @return JsonResponse
     * @throws CustomUnAuthenticatedException
     */
    public function profile()
    {
        return $this->successResponse()
                    ->setData($this->service->profile())
                    ->response();
    }

    /**
	 * update user profile info
     *
     * @param UpdateUserRequest $request
     * @return JsonResponse
     * @throws CustomQueryErrorException
     */
    public function update(UpdateUserRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->update($request->validated()))
					->response();
	}
	
	
	/**
	 * update language by language_id
	 *
	 * @param UserUpdateLanguageRequest $request
	 * @return JsonResponse
	 * @throws CustomQueryErrorException
	 * @throws CustomUnAuthenticatedException
	 */
    public function updateLanguage(UserUpdateLanguageRequest $request)
    {
        return $this->successResponse()
                    ->setData($this->service->updateLanguage($request->language_id))
                    ->response();
    }

}
