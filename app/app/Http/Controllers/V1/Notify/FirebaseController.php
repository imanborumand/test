<?php namespace App\Http\Controllers\V1\Notify;


use App\Exceptions\CustomNotFoundException;
use App\Http\Controllers\ControllerBase;
use App\Http\Requests\V1\Notify\ReceiveNotifyByBookingRequest;
use App\Http\Requests\V1\Notify\ReceiveNotifyByTokenRequest;
use App\Http\Requests\V1\Notify\SendNotificationToUserRequest;
use App\Services\V1\Notify\FirebaseService;
use Illuminate\Http\JsonResponse;

/**
 * Class FirebaseController
 *
 * @package App\Http\Controllers\V1\Notify
 */
class FirebaseController extends ControllerBase
{
	
	/**
	 * FirebaseController constructor.
	 *
	 * @param FirebaseService $service
	 */
	public function __construct( FirebaseService $service)
    {
        $this->service = $service;
    }


    /**
	 * send notify to user token
     *
     * @param SendNotificationToUserRequest $request
     * @return JsonResponse
     * @throws CustomNotFoundException
     */
    public function sendNotifyToUser(SendNotificationToUserRequest $request)
    {
        return $this->successResponse()
                    ->setData($this->service->sendNotifyToUser($request->validated()))
                    ->response();
    }
	
	
	/**
	 * get booking user token and send notify to all of them
	 * @param ReceiveNotifyByBookingRequest $request
	 * @return JsonResponse
	 * @throws CustomNotFoundException
	 */
	public function notifyByBookingId(ReceiveNotifyByBookingRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->notifyByBookingId($request->validated()))
					->response();
	}
	
	
	/**
	 * @param ReceiveNotifyByTokenRequest $request
	 * @return JsonResponse
	 * @throws CustomNotFoundException
	 */
	public function notifyByFirebaseToken(ReceiveNotifyByTokenRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->notifyByFirebaseToken($request->validated()))
					->response();
	}
}
