<?php namespace App\Http\Controllers\V1\Notify;

use App\Exceptions\CustomNotFoundException;
use App\Exceptions\CustomNotifyNotFoundException;
use App\Exceptions\CustomQueryErrorException;
use App\Exceptions\CustomUnAuthenticatedException;
use App\Exceptions\CustomUnAuthorizedException;
use App\Http\Controllers\ControllerBase;
use App\Http\Requests\V1\Notify\ChangeSeenNotifyRequest;
use App\Http\Requests\V1\Notify\DeleteNotifyRequest;
use App\Http\Requests\V1\Notify\GetUserNotifiesRequest;
use App\Services\V1\Notify\NotifyService;
use Illuminate\Container\EntryNotFoundException;
use Illuminate\Http\JsonResponse;

/**
 * Class NotifyController
 *
 * @package App\Http\Controllers\V1\Notify
 */
class NotifyController extends ControllerBase
{
	/**
	 * @var NotifyService
	 */
	protected $service;
	
	
	/**
	 * NotifyController constructor.
	 *
	 * @param NotifyService $service
	 */public function __construct(NotifyService $service)
	{
		$this->service = $service;
	}
	
	
	/**
	 * return list of notification state
	 * @return mixed
	 */
	public function all()
	{
		return $this->successResponse()
					->setData($this->service->all())
					->response();
	}
	
	
	/**
	 * return notify log of user
	 *
	 * @param GetUserNotifiesRequest $request
	 * @return JsonResponse
	 * @throws CustomUnAuthenticatedException
	 */
	public function getUserNotifies(GetUserNotifiesRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->getUserNotifies($request->user_id))
					->response();
	}
	
	
	/**
	 * change seen notify seen status
	 *
	 * @param ChangeSeenNotifyRequest $request
	 * @return JsonResponse
	 * @throws CustomNotFoundException
	 * @throws CustomQueryErrorException
	 * @throws CustomUnAuthorizedException
	 * @throws CustomNotifyNotFoundException
	 */
	public function changeSeenNotify(ChangeSeenNotifyRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->changeSeenNotify($request->notify_id))
					->response();
	}
	
	
	/**
	 * delete notify by id
	 *
	 * @param DeleteNotifyRequest $request
	 * @return JsonResponse
	 * @throws CustomNotFoundException
	 * @throws CustomNotifyNotFoundException
	 * @throws CustomQueryErrorException
	 * @throws CustomUnAuthorizedException
	 */
	public function delete( DeleteNotifyRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->delete($request->notify_id))
					->response();
	}
	
}
