<?php namespace App\Http\Controllers\V1\Country;

use App\Http\Controllers\ControllerBase;
use App\Http\Requests\V1\Country\GetCountryRequest;
use App\Services\V1\Country\CountryService;
use Illuminate\Http\JsonResponse;

class CountryController extends ControllerBase
{

	public function __construct(CountryService $service)
	{
		$this->service = $service;
	}


    /**
     * get list of all country
	 *
     * @return JsonResponse
     */
    public function all()
	{
		return $this->successResponse()
					->setData($this->service->all())
					->response();
	}


    /**
     * return country data by countryIso
     * @param GetCountryRequest $request
     * @return JsonResponse
     */
    public function get(GetCountryRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->get($request->iso2))
					->response();
	}

}
