<?php namespace App\Http\Controllers\V1\Contact;

use App\Exceptions\CustomNotFoundException;
use App\Exceptions\CustomQueryErrorException;
use App\Exceptions\CustomUnAuthorizedException;
use App\Exceptions\CustomValidationException;
use App\Http\Controllers\ControllerBase;
use App\Http\Requests\V1\Contact\DeleteContactUserRequest;
use App\Http\Requests\V1\Contact\StoreContactUserRequest;
use App\Http\Requests\V1\Contact\UpdateContactUserRequest;
use App\Services\V1\Contact\ContactUserService;
use Illuminate\Http\JsonResponse;

/**
 * Class ContactUserController
 *
 * @package App\Http\Controllers\V1\Contact
 */
class ContactUserController extends ControllerBase
{
	
	/**
	 * ContactUserController constructor.
	 *
	 * @param ContactUserService $service
	 */
	public function __construct(ContactUserService $service)
	{
		$this->service = $service;
	}
	
	
	/**
	 * @param StoreContactUserRequest $request
	 * @return JsonResponse
	 * @throws CustomQueryErrorException
	 * @throws CustomUnAuthorizedException
	 */
	public function store(StoreContactUserRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->store($request->getData()))
					->response();
	}
	
	/**
	 * @param UpdateContactUserRequest $request
	 * @param  $contactUserId
	 * @return JsonResponse
	 * @throws CustomQueryErrorException
	 * @throws CustomNotFoundException
	 */
	public function update(UpdateContactUserRequest $request, $contactUserId)
	{
		return $this->successResponse()
					 ->setData($this->service->update($request->getData(),$contactUserId))
					 ->response();
	}
	
	
	/**
	 * @param DeleteContactUserRequest $request
	 * @return JsonResponse
	 * @throws CustomNotFoundException
	 * @throws CustomQueryErrorException
	 * @throws CustomValidationException
	 */
	public function delete(DeleteContactUserRequest $request)
	{
		$this->service->delete($request->contact_user_id);
		return $this->successResponse()->response();
	}
	
}
