<?php namespace App\Http\Controllers\V1\Contact;

use App\Exceptions\CustomNotFoundException;
use App\Exceptions\CustomQueryErrorException;
use App\Http\Controllers\ControllerBase;
use App\Http\Requests\V1\Contact\DeleteContactRequest;
use App\Http\Requests\V1\Contact\GetContactRequest;
use App\Http\Requests\V1\Contact\StoreContactRequest;
use App\Http\Requests\V1\Contact\UpdateContactRequest;
use App\Services\V1\Contact\ContactService;
use Illuminate\Container\EntryNotFoundException;
use Illuminate\Http\JsonResponse;

/**
 * Class ContactController
 *
 * @package App\Http\Controllers\V1\Contact
 */
class ContactController extends ControllerBase
{
	/**
	 * ContactController constructor.
	 *
	 * @param ContactService $service
	 */
	public function __construct(ContactService $service)
	{
		$this->service = $service;
	}
	
	/**
	 * @return JsonResponse
	 * @throws \Exception
	 */
	public function all()
	{
		return $this->successResponse()
					->setData($this->service->all())
					->response();
	}
	
	/**
	 * find contact by contactId
	 * @param GetContactRequest $request
	 * @return JsonResponse
	 * @throws CustomNotFoundException
	 * @throws EntryNotFoundException
	 */
	public function get(GetContactRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->getById($request->contact_id))
					->response();
	}
	
	/**
	 * @param StoreContactRequest $request
	 * @return JsonResponse
	 * @throws CustomQueryErrorException
	 */
	public function store(StoreContactRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->store($request->validated()))
					->response();
	}
	
	/**
	 * @param UpdateContactRequest $request
	 * @param                      $contactId
	 * @return JsonResponse
	 * @throws \Exception
	 */
	public function update(UpdateContactRequest $request, $contactId)
	{
		return $this->successResponse()
					->setData($this->service->update($request->validated(),$contactId))
					->response();
	}
	
	/**
	 * @param DeleteContactRequest $request
	 * @return JsonResponse
	 * @throws CustomQueryErrorException
	 */
	public function delete(DeleteContactRequest $request)
	{
		return $this->successResponse()
					  ->setData($this->service->delete($request->contact_id))
					  ->response();
	}

}
