<?php namespace App\Http\Controllers\V1\Booking;


use App\Exceptions\CustomBadRequestException;
use App\Exceptions\CustomResponseException;
use App\Exceptions\CustomUnAuthenticatedException;
use App\Exceptions\CustomValidationException;
use App\Http\Controllers\ControllerBase;
use App\Http\Requests\V1\Booking\GetBookingByBookingIDRequest;
use App\Services\V1\Booking\BookingService;
use Exception;
use Illuminate\Http\JsonResponse;

class BookingController extends ControllerBase
{
	protected $service;
	public function __construct(BookingService $service)
	{
		$this->service = $service;
	}


	/**
	 * @return JsonResponse
	 * @throws Exception
	 */
	public function all()
	{
		return $this->successResponse()
					->setData($this->service->all())
					->response();
	}


    /**
     * @param GetBookingByBookingIDRequest $request
     * @return JsonResponse
     * @throws CustomResponseException
     * @throws CustomValidationException
     */
	public function getBooking(GetBookingByBookingIDRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->getBooking($request->booking_id))
					->response();

	}


	/**
	 * add booking to user favorite list
	 *
	 * @param GetBookingByBookingIDRequest $request
	 * @return JsonResponse
	 * @throws CustomResponseException
	 * @throws CustomUnAuthenticatedException
	 * @throws CustomBadRequestException
	 * @throws CustomValidationException
	 */
	public function addBooking(GetBookingByBookingIDRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->addBooking($request->booking_id))
					->response();
	}

}
