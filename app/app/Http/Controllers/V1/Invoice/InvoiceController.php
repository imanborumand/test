<?php namespace App\Http\Controllers\V1\Invoice;


use App\Http\Controllers\ControllerBase;
use App\Http\Requests\V1\Invoice\GetInvoiceBookingRequest;
use App\Services\V1\Invoice\InvoiceService;

class InvoiceController extends ControllerBase
{
	public function __construct(InvoiceService $service)
	{
		$this->service = $service;
	}
	
	
	/**
	 * get invoice of booking by booking id
	 * @param GetInvoiceBookingRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Exception
	 */
	public function getInvoiceBooking( GetInvoiceBookingRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->getInvoiceBooking($request->booking_id))
					->response();
	}
}
