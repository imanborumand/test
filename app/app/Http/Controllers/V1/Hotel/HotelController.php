<?php namespace App\Http\Controllers\V1\Hotel;


use App\Exceptions\CustomNotFoundException;
use App\Exceptions\CustomResponseException;
use App\Http\Controllers\ControllerBase;
use App\Http\Requests\V1\Hotel\GetHotelByNameRequest;
use App\Services\V1\Hotel\HotelService;
use Illuminate\Http\JsonResponse;

class HotelController extends ControllerBase
{
	public function __construct(HotelService $service)
	{
		$this->service = $service;
	}
	
	
	/**
	 * send request to ms-hotel for get hotel
	 *
	 * @param GetHotelByNameRequest $request
	 * @return JsonResponse
	 * @throws CustomNotFoundException
	 * @throws CustomResponseException
	 */
	public function getByName(GetHotelByNameRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->getByName($request->validated()))
					->response();
	}
}
