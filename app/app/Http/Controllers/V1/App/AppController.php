<?php
namespace App\Http\Controllers\V1\App;

use App\Exceptions\CustomValidationException;
use App\Services\V1\App\AppService;
use App\Http\Controllers\ControllerBase;
use Illuminate\Http\JsonResponse;

/**
 * Class AppController
 *
 * @package App\Http\Controllers\V1\App
 */
class AppController extends ControllerBase
{
	/**
	 * AppController constructor.
	 *
	 * @param AppService $service
	 */
	public function __construct(AppService $service)
	{
		$this->service = $service;
	}


    /**
     * return some data for when app start to initials
     * @return JsonResponse
     * @throws CustomValidationException
     */
    public function init()
	{
		return $this->successResponse()
					->setData($this->service->init())
					->response();
	}

}
