<?php namespace App\Http\Controllers\V1\Experiences;


use App\Http\Controllers\ControllerBase;
use App\Http\Requests\V1\Experience\ExperiencesRequest;
use App\Services\V1\Experiences\ExperiencesService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class ExperiencesController
 *
 * @package App\Http\Controllers\V1\Experiences
 */
class ExperiencesController extends ControllerBase
{
	/**
	 * @var ExperiencesService
	 */
	protected $service;


	/**
	 * ExperiencesController constructor.
	 *
	 * @param ExperiencesService $service
	 */
	public function __construct(ExperiencesService $service)
	{
		$this->service = $service;
	}
	
	
	/**
	 * @param ExperiencesRequest $request
	 * @return JsonResponse
	 * @throws \App\Exceptions\CustomResponseException
	 */
	public function get(ExperiencesRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->get($request->validated()))
					->response();
	}
}
