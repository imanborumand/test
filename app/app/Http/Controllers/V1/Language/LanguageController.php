<?php namespace App\Http\Controllers\V1\Language;


use App\Http\Controllers\ControllerBase;
use App\Http\Requests\V1\Language\GetLanguageRequest;
use App\Http\Requests\V1\Language\StoreLanguageRequest;
use App\Http\Requests\V1\Language\StoreTranslatesRequest;
use App\Http\Resources\V1\Language\LanguageResource;
use App\Services\V1\Language\LanguageService;
use Exception;
use Illuminate\Http\JsonResponse;

class LanguageController extends ControllerBase
{

	public function __construct(LanguageService $service)
	{
		$this->service = $service;
	}

	/**
	 * return list of language
	 * @return JsonResponse
	 * @throws Exception
	 */
	public function all()
	{
		return $this->successResponse()
					->setData($this->service->all())
					->response();
	}


	/**
	 * return one language with it translate
	 *
	 * get one language and relations by id
	 * @param GetLanguageRequest $request
	 * @return JsonResponse
	 * @throws Exception
	 */
	public function getWith(GetLanguageRequest $request)
	{
		
			$result = $this->service->getLanguageByIdWithTranslates($request->language_id);
			return $this->successResponse()
						->setData((new LanguageResource($result)))
						->response();
	}


	/**
	 * store new translate in database
	 *
	 * @param StoreLanguageRequest $request
	 * @return JsonResponse
	 * @throws Exception
	 */
	public function store(StoreLanguageRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->store($request->validated()))
					->response();
	}

}
