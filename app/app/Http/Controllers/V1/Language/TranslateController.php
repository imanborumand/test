<?php namespace App\Http\Controllers\V1\Language;


use App\Http\Controllers\ControllerBase;
use App\Http\Requests\V1\Language\StoreTranslatesRequest;
use App\Http\Resources\V1\Language\LanguageResource;
use App\Services\V1\Language\TranslateService;
use App\Http\Requests\V1\Language\ImportTranslatesRequest;
use Exception;
use Illuminate\Http\JsonResponse;

/**
 * Class TranslateController
 *
 * @package App\Http\Controllers\V1\Language
 */
class TranslateController extends ControllerBase
{
	
	/**
	 * TranslateController constructor.
	 *
	 * @param TranslateService $service
	 */
	public function __construct( TranslateService $service)
	{
		$this->service = $service;
	}
	
	
	/**
	 * import excel file that upload by user and
	 * save new translate in database
	 * @param ImportTranslatesRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function importExcel( ImportTranslatesRequest $request)
	{
		return $this->successResponse()
					->setData($this->service->importExcel($request->validated()))
					->response();
	}
	
	/**
	 * store translates of language by language id
	 * @param StoreTranslatesRequest $request
	 * @return JsonResponse
	 * @throws Exception
	 */
	public function storeTranslates(StoreTranslatesRequest $request)
	{
		$result = $this->service->storeTranslates($request->validated());
		return $this->successResponse()
					->setData((new LanguageResource($result)))
					->response();
	}
	
}
