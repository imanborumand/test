<?php namespace App\Http\Controllers\V1\Itinerary;

use App\Http\Controllers\ControllerBase;
use App\Http\Requests\V1\Itinerary\GetItineraryByBookingIdRequest;
use App\Models\Entities\User\User;
use App\Services\V1\Itinerary\ItineraryService;
use Exception;
use Illuminate\Http\JsonResponse;

class ItineraryController extends ControllerBase
{
    public function __construct(ItineraryService $service)
    {
        $this->service = $service;
    }


    /**
     * get itinerary by booking id
     * @param GetItineraryByBookingIdRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function getItinerary(GetItineraryByBookingIdRequest $request)
    {
        return $this->successResponse()
			 		->setData($this->service->getItinerary($request->validated()))
					->response();
    }
}
