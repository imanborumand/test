<?php namespace App\Http\Requests\V1\Notify;

use App\Exceptions\CustomValidationException;
use App\Http\Requests\FormRequestBase;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;

/**
 * Class DeleteContactRequest
 *
 * @package App\Http\Requests\V1\Contact
 */
class SendNotificationToUserRequest extends FormRequestBase
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		
		return [
			'app_name' => ['required', 'string', Rule::in(array_keys(config('wrapper_config')))] , //check for app-name exists in config
			'user_id' => 'required|integer|exists:users,id' ,
			'title'   => 'required|array' ,
			'body'    => 'required|array'
		];
	}
	
	
	/**
	 * @return Validator
	 * @throws CustomValidationException
	 */
	protected function getValidatorInstance()
	{
		$this->checkWrapperToken();
		$data = $this->all();
		$data['app_name'] = $this->getAppNameFromHeader(); //add app_name for save in notification log
		$data['user_id'] = $this->route('user_id');
		$this->getInputSource()->replace($data);
		
		return parent::getValidatorInstance();
	}
}
