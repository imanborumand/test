<?php namespace App\Http\Requests\V1\Notify;


use App\Http\Requests\FormRequestBase;

class GetUserNotifiesRequest extends FormRequestBase
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'user_id' => 'required|integer|exists:users,id'
		];
	}
	
	public function all($keys = null)
	{
		$request = request();
		$request['user_id'] = $request->user_id;
		return $request->toArray();
	}
}
