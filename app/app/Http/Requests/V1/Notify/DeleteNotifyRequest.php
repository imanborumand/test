<?php namespace App\Http\Requests\V1\Notify;


use App\Http\Requests\FormRequestBase;

class DeleteNotifyRequest extends FormRequestBase
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'notify_id' => 'required|string' //todo check exists in mongodb
		];
	}
	
	public function all($keys = null)
	{
		$request = request();
		$request['notify_id'] = $request->notify_id;
		return $request->toArray();
	}
}
