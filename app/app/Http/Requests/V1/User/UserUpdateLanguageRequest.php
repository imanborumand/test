<?php namespace App\Http\Requests\V1\User;

use App\Http\Requests\FormRequestBase;
use Illuminate\Contracts\Validation\Validator;


class UserUpdateLanguageRequest extends FormRequestBase
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'language_id' => 'required|integer|exists:languages,id'
        ];
    }


    /**
     * @return Validator
     */
    protected function getValidatorInstance()
    {
        $data = $this->all();
        $data['language_id'] = $this->route('language_id');
        $this->getInputSource()->replace($data);
        return parent::getValidatorInstance();
    }
}
