<?php namespace App\Http\Requests\V1\User;

use App\Http\Requests\FormRequestBase;

class UpdateUserRequest extends FormRequestBase
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'first_name' => 'nullable|max:100',
			'last_name'  => 'nullable|max:100',
			'middle_name'=> 'nullable|max:100',
			'avatar'     => 'file|max:1024|mimes:jpeg,bmp,png|dimensions:max_width:300,max_height:200',
			'birth_date' => 'date|nullable',
			'nationality_id' => 'nullable|integer|exists:countries,id',
		];
	}
	
	
}
