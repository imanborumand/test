<?php namespace App\Http\Requests\V1\Language;

use App\Http\Requests\FormRequestBase;
use Illuminate\Validation\Rule;


class StoreLanguageRequest extends FormRequestBase
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'boolean|required',
			'iso2' => 'string|required|max:5|unique:languages,iso2',
			'title' => 'string|required|max:30',
			'direction' => ['string', 'required', Rule::in(['rtl', 'ltr'])],
			'flag' => 'string|nullable',
        ];
    }
}
