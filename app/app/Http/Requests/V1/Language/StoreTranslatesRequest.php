<?php namespace App\Http\Requests\V1\Language;

use App\Http\Requests\FormRequestBase;


class StoreTranslatesRequest extends FormRequestBase
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'language_id' => 'required|integer|exists:languages,id',
			'translates'  => 'array|min:1',
			'translates.*' => 'string|required',
			'device_type_id.*' => 'integer|required|exists:device_types,id'
        ];
    }
}
