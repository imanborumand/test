<?php

namespace App\Http\Requests\V1\Language;

use App\Http\Requests\FormRequestBase;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ImportTranslatesRequest extends FormRequestBase
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'excel_file' => 'required|file|max:2048|mimes:xls,xlsx',
		];
	}

}
