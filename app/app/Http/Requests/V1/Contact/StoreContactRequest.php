<?php

namespace App\Http\Requests\V1\Contact;

use App\Http\Requests\FormRequestBase;

/**
 * Class StoreContactRequest
 *
 * @package App\Http\Requests\V1\Contact
 */
class StoreContactRequest extends FormRequestBase
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'title' => 'required|string|max:100',
			'key'   => 'required|string|unique:contacts,key',
		];
	}
	
}
