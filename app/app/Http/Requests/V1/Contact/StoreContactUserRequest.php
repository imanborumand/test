<?php

namespace App\Http\Requests\V1\Contact;

use App\Exceptions\CustomValidationException;
use App\Http\Requests\FormRequestBase;
use Illuminate\Contracts\Validation\Validator;

/**
 * Class StoreContactUserRequest
 *
 * @package App\Http\Requests\V1\Contact
 */
class StoreContactUserRequest extends FormRequestBase
{
	
	/**
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	/**
	 * @return array
	 */
	public function rules()
	{
		return [
			'contact_id' => 'required|exists:contacts,id',
			'value'	     => 'required|string',
		];
	}
	
	/**
	 * @return array
	 */
	public function getData()
	{
		return $this->only('contact_id','value');
	}
	
	
	/**
	 * @return Validator
	 * @throws CustomValidationException
	 */
	protected function getValidatorInstance()
	{
		$data['app_name'] = $this->getAppNameFromHeader(); //check exists app-name in header
		$data = $this->request->all();
		//convert  persian or arabic number in value param to English
		if ($this->request->has('value')) {
			$data['value'] = convertPersianNumbersToEnglish($data['value']);
		}
		
		$this->getInputSource()->replace($data);
		return parent::getValidatorInstance();
	}
	
}
