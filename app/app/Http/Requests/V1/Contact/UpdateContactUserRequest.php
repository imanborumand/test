<?php

namespace App\Http\Requests\V1\Contact;

use App\Http\Requests\FormRequestBase;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

/**
 * Class UpdateContactUserRequest
 *
 * @package App\Http\Requests\V1\Contact
 */
class UpdateContactUserRequest extends FormRequestBase
{
	
	/**
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	
	/**
	 * @return array
	 */
	public function rules()
	{
		return [
			'contact_user_id' => 'required|integer|exists:contact_user,id' ,
			'value'           => 'string' ,
			'status'          => "boolean" ,
		];
	}
	
	/**
	 * @return Validator
	 */
	protected function getValidatorInstance()
	{
		$data = $this->all();
		$data['contact_user_id'] = $this->route('contact_user_id');
		
		//convert  persian or arabic number in value param to English
		if ($this->request->has('value')) {
			$data['value'] = convertPersianNumbersToEnglish($data['value']);
		}
		
		$this->getInputSource()->replace($data);
		return parent::getValidatorInstance();
	}
	
	/**
	 * @return array
	 */
	public function getData()
	{
		return $this->only('contact_user_id', 'value', 'status');
	}
	
}
