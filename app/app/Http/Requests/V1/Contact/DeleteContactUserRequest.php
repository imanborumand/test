<?php

namespace App\Http\Requests\V1\Contact;

use App\Http\Requests\FormRequestBase;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

/**
 * Class DeleteContactUserRequest
 *
 * @package App\Http\Requests\V1\Contact
 */
class DeleteContactUserRequest extends FormRequestBase
{
	
	/**
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	
	/**
	 * @return array
	 */
	public function rules()
	{
		return [
			'contact_user_id' => 'required|integer|exists:contact_user,id',
		];
	}
	
	/**
	 * @return Validator
	 */
	protected function getValidatorInstance()
	{
		$data = $this->all();
		$data['contact_user_id'] = $this->route('contact_user_id');
		$this->getInputSource()->replace($data);
		return parent::getValidatorInstance();
	}
	
}
