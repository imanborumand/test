<?php

namespace App\Http\Requests\V1\Contact;

use App\Http\Requests\FormRequestBase;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

/**
 * Class DeleteContactRequest
 *
 * @package App\Http\Requests\V1\Contact
 */
class DeleteContactRequest extends FormRequestBase
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'contact_id' => 'required|integer|exists:contacts,id'
		];
	}
	
	/**
	 * @return Validator
	 */
	protected function getValidatorInstance()
	{
		$data = $this->all();
		$data['contact_id'] = $this->route('contact_id');
		$this->getInputSource()->replace($data);
		return parent::getValidatorInstance();
	}
}
