<?php

namespace App\Http\Requests\V1\Experience;

use App\Http\Requests\FormRequestBase;
use Illuminate\Contracts\Validation\Validator;

/**
 * Class ExperiencesRequest
 *
 * @package App\Http\Requests\V1\Experience
 */
class ExperiencesRequest extends FormRequestBase
{

	/**
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * @return array
	 */
	public function rules()
	{
		return [
			'booking_id'   => 'required|string',
			'type'		   => 'required|in:' . implode(',', config('general.experience_type')),
			'country_code' => 'nullable|string',
			'city_code'    => 'nullable|integer' ,
			'language_id'  => 'required|exists:languages,id' ,
			'last_update'  => 'nullable|date' ,
		];
	}


	/**
	 * @return Validator
	 */
	protected function getValidatorInstance()
	{
		$data = $this->all();
		$data['booking_id'] = $this->route('booking_id');
		$this->getInputSource()->replace($data);
		return parent::getValidatorInstance();
	}
}
