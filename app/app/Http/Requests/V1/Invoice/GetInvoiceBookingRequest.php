<?php namespace App\Http\Requests\V1\Invoice;

use App\Http\Requests\FormRequestBase;


class GetInvoiceBookingRequest extends FormRequestBase
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'booking_id' => 'string|required'
        ];
    }
	
	public function all($keys = null)
	{
		$request = request();
		$request['booking_id'] = $request->booking_id;
		return $request->toArray();
	}
 
}
