<?php
namespace App\Http\Requests\V1\Hotel;

use App\Http\Requests\FormRequestBase;
use Illuminate\Contracts\Validation\Validator;

/**
 * Class ExperiencesRequest
 *
 * @package App\Http\Requests\V1\Experience
 */
class GetHotelByNameRequest extends FormRequestBase
{

	/**
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * @return array
	 */
	public function rules()
	{
		return [
			'iso2'   => 'required|string|exists:countries,iso2',
			'name'   => 'required|string' ,
		];
	}


	/**
	 * @return Validator
	 */
	protected function getValidatorInstance()
	{
		$data = $this->all();
		$data['iso2'] = $this->route('iso2');
		$data['name'] = $this->route('name');
		$this->getInputSource()->replace($data);
		return parent::getValidatorInstance();
	}
}
