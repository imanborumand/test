<?php

namespace App\Http\Requests\V1\Country;

use App\Http\Requests\FormRequestBase;
use Illuminate\Contracts\Validation\Validator;

/**
 * Class GetCountryRequest
 *
 * @package App\Http\Requests\V1\Country
 */
class GetCountryRequest extends FormRequestBase
{

	/**
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * @return array
	 */
	public function rules()
	{
		return [
			'iso2' => 'required|string|exists:countries,iso2'
		];
	}

	/**
	 * @return Validator
	 */
	protected function getValidatorInstance()
	{
		$data = $this->all();
		$data['iso2'] = strtoupper($this->route('iso2'));
		$this->getInputSource()->replace($data);
		return parent::getValidatorInstance();
	}
}
