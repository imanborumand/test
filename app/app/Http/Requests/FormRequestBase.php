<?php namespace App\Http\Requests;

use App\Exceptions\CustomValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

/**
 * Class FormRequestBase
 * all FormRequest must by extend this class
 * @package App\Http\Requests
 */
abstract  class FormRequestBase extends FormRequest
{
	/**
	 * @param Validator $validator
	 * @throws CustomValidationException
	 */
	protected function failedValidation(Validator $validator)
	{
		throw new CustomValidationException(null,Response::HTTP_BAD_REQUEST,STATUS_CODE_BAD_REQUEST,array_values($validator->errors()->toArray()));
	}
	
	
	/**
	 * this function can use for get app name for every FormRequest
	 * @return string
	 * @throws CustomValidationException
	 */
	protected function getAppNameFromHeader() : string
	{
		$appName = request()->header('app-name');
		if(! $appName) {
			throw new CustomValidationException("please add app name!");
		}
		return $appName;
	}
	
	
	/**
	 * check token from wrapper
	 * @throws CustomValidationException
	 */
	protected function checkWrapperToken()
    {
		$token = request()->header('token');
		if(! $token || $token != 'VikTplgucVCtd21jVXsLGMW3H95Ezr66QRnApU19dxUc9uBrbDjCcICkKjvY') {
			throw new CustomValidationException("Wrapper token not valid!");
		}
    }
}
