<?php


namespace App\Http\Requests\Auth\User;


use App\Exceptions\CustomValidationException;
use App\Http\Requests\FormRequestBase;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class LoginByHashRequest extends FormRequestBase
{
	/**
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	/**
	 * @return array
	 */
	public function rules()
	{
		return [
			'booking_id'  => 'nullable|string' ,
			'app_name'    => 'required|string|in:'.implode(',', array_keys(config('wrapper_config'))),
			'mobile'      => 'nullable|string' ,
			'name'        => 'nullable|string' ,
			'language_id' => 'integer|required|exists:languages,id',
			'firebase_token' => 'string'
		];
	}
	
	
	/**
	 * check and validate hash_code [hash_key] in request layer after send to logic layer
	 *
	 * @return Validator
	 */
	protected function getValidatorInstance()
	{
		$request = request();
		$data['hash_code'] = $request->hash_code;
		$errorMessage = config('default_messages.response.url_not_valid');

		
//		dd(base64_encode("miniCs1|Dtr280420|989378894154|Kimia hadad")); //for test
		//check validate hash key if len equal 0 return error
		if (strlen($data['hash_code']) == 0 ) {
			throw new NotFoundHttpException($errorMessage);
		}
		
		//explode hash_key to array- if array count < 4 => this hash not valid and return error
		$keyArray = explode('|', base64_decode($data['hash_code']));
		if (count($keyArray) != 4) {
			throw new NotFoundHttpException($errorMessage);
		}
		
		//hash key valid has this pattern: app_name|booking_id|mobile|name
		//check params
		$data['app_name'] = $keyArray[0];
		$data['booking_id'] = $keyArray[1];
		$data['mobile'] = $keyArray[2];
		$data['name'] = $keyArray[3];
		$data['language_id'] = DEFAULT_LANGUAGE_ID; // add default lan for use login user
		$data['firebase_token'] = $request->firebase_token ?? ""; // add default lan for use login user
		request()->headers->set('app-name', $keyArray[0]); // add app-name to header for get in app-name form curl function
		
		
		$this->getInputSource()->replace($data);
		return parent::getValidatorInstance();
	}
}
