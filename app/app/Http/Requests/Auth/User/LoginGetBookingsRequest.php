<?php

namespace App\Http\Requests\Auth\User;

use App\Http\Requests\FormRequestBase;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

/**
 *
 * Class LoginGetBookingsRequest
 *
 * @package App\Http\Requests\Auth\User
 */
class LoginGetBookingsRequest extends FormRequestBase
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'booking_id' => 'string|present|nullable',
			'mobile' => 'numeric|required',
			'name' => 'string|present|nullable',
        ];
    }


	/**
	 * @return Validator
	 */
	protected function getValidatorInstance()
	{
		$data = $this->request->all();
		//convert  persian or arabic booking_id number to English
		if ($this->request->has('booking_id')) {
			$data['booking_id'] = convertPersianNumbersToEnglish($data['booking_id']);
		}

		//convert  persian or arabic mobile number to English
		if ($this->request->has('mobile')) {
			$data['mobile'] = convertPersianNumbersToEnglish($data['mobile']);
		}
		
		//remove special html character
		if ($this->request->has('name')) {
			$data['name'] = cleanString(htmlentities($data['name']));
		}

		$this->getInputSource()->replace($data);
		return parent::getValidatorInstance();
	}

}
