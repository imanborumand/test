<?php namespace App\Http\Requests\Auth\User;


use App\Exceptions\CustomValidationException;
use App\Http\Requests\FormRequestBase;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserLoginRequest extends FormRequestBase
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}


	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'booking_id'   => 'string|required',
			'mobile' => 'required|string',
			'name' => 'nullable|string',
			'language_id' => 'required|integer',
			'firebase_token' => 'required|string'
		];
	}


	/**
	 * @return Validator
	 */
	protected function getValidatorInstance()
	{
		$data = $this->request->all();
		$this->getInputSource()->replace($data);
		return parent::getValidatorInstance();
	}
}
