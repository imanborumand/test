<?php

namespace App\Http\Requests\Auth\User;

use App\Http\Requests\FormRequestBase;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class CheckMobileInLoginRequest extends FormRequestBase
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile'      => 'string|required',
			'language_id' => 'required|integer|exists:languages,id'
        ];
    }
	
	
	/**
	 * @return Validator
	 */
	protected function getValidatorInstance()
	{
		$data = $this->request->all();
		//convert  persian or arabic mobile number to English
		if ($this->request->has('mobile')) {
			$data['mobile'] = convertPersianNumbersToEnglish($data['mobile']);
		}
		$this->getInputSource()->replace($data);
		return parent::getValidatorInstance();
	}
}
