<?php namespace App\Http\Requests\Auth\User;


use App\Exceptions\CustomValidationException;
use App\Http\Requests\FormRequestBase;
use Illuminate\Contracts\Validation\Validator;

/*
 * if app not install in mobile user call this request in byHash method in UserLoginByHashController
 */
class RedirectToStoreRequest extends FormRequestBase
{
	
	/**
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	/**
	 * @return array
	 */
	public function rules()
	{
		return [
			'hash_code'  => 'required|string',
		];
	}
	
	
	/**
	 * check and validate info [hash_key] in request layer after send to logic layer
	 * @return Validator
	 * @throws CustomValidationException
	 */
	protected function getValidatorInstance()
	{
		$request = request();
		$data['hash_code'] = $request->hash_code;
	
		//check validate hash key if len equal 0 return error
		if (strlen($data['hash_code']) == 0 ) {
			throw new CustomValidationException();
		}

		//explode hash_key to array- if array count < 4 => this hash not valid and return error
		$keyArray = explode('|', base64_decode($data['hash_code']));
		if (count($keyArray) != 4) {
			throw new CustomValidationException();
		}
		
		$this->getInputSource()->replace($data);
		return parent::getValidatorInstance();
	}
}
