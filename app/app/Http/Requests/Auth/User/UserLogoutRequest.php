<?php namespace App\Http\Requests\Auth\User;


use App\Http\Requests\FormRequestBase;
use Illuminate\Contracts\Validation\Validator;

class UserLogoutRequest extends FormRequestBase
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'firebase_token'   => 'string|required|exists:contact_user,value',
		];
	}
	
	
	/**
	 * @return Validator
	 */
	protected function getValidatorInstance()
	{
		$data = $this->request->all();
		$this->getInputSource()->replace($data);
		return parent::getValidatorInstance();
	}
}
