<?php

namespace App\Listeners;

use App\CustomStuff\Traits\JobsLogTrait;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\ServiceProvider;
use Illuminate\Queue\Events\JobProcessed;

class JobDoneListener
{
	use JobsLogTrait;
	
	/**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    	//
	}

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
		$extraData = $event->data;
		$request   = $event->request;
		Queue::after(function (JobProcessed $event)  use ($extraData, $request){
			$params = array_merge($extraData,[
				'connectionName' => $event->connectionName ,
				'resolveName'    => str_replace("App\\Jobs\\","",$event->job->resolveName()),
				'jobPayload'       => $event->job->payload() ,
				'exception'      => null ,
			]);
			$this->doLogResponseAndRequest($params,$request);
		});
    }
}
