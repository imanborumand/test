<?php namespace App\Jobs;

use App\Events\JobDoneEvent;
use App\Events\JobFailedEvent;
use App\Exceptions\CustomResponseException;
use App\Exceptions\CustomValidationException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Exception;

class SendUserInfoToWrapperJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	
	private $appName;
	private $params;
	private $response;
	private $wrapperUrl;
	
	/**
	 * Create a new job instance.
	 *
	 * @param array  $params
	 * @param string $appName
	 */
	public function __construct(array $params, string $appName)
	{
		$this->params = $params;
		$this->appName = $appName;
	}
	
	
	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$this->wrapperUrl = config('wrapper_config')[$this->appName]['base_url'] . 'booking/save-turpal-user-id';
		try {
			$this->response = $this->sendRequestToWrapperLogin();
			
			event(new JobDoneEvent($this->getData(), (array) $this->response));
		} catch(Exception $exception) {
		
		}
	}
	
	
	/**
	 *  send request to wrapper
	 *
	 * @return object
	 * @throws CustomResponseException
	 */
	private function sendRequestToWrapperLogin() : object
	{
		return json_decode(doRequest($this->wrapperUrl, "PUT", $this->params));
	}
	
	
	/**
	 * The job failed to process.
	 *
	 * @param  Exception  $exception
	 * @return void
	 */
	public function failed(Exception $exception)
	{
		event(new JobFailedEvent($this->getData(), (array) $this->response));
	}
	
	/**
	 *
	 * @return mixed
	 */
	private function getData()
	{
		$result['sendParams']     = $this->params;
		$result['wrapper_url']    = $this->wrapperUrl;
		$result['wrapper_response']    = $this->response;
		return $result;
	}
	
}
