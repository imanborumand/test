<?php

namespace App\Jobs;

use App\Models\Entities\Log\LogRequestResponseModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class LogResponseAndRequestJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	/**
     * Create a new job instance.
     *
     * @return void
     */
	private $guard;
	private $request;
	private $response;


	/**
	 * LogResponseAndRequestJob constructor.
	 *
	 * @param $guard
	 * @param $request
	 * @param $response
	 */
	public function __construct($guard,$request,$response)
    {
		$this->guard = $guard;
		$this->request= $request;
		$this->response = $response;
	}

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
		$params =array_merge([
			'guard'    => $this->guard['guard'] ,
			'guard_id' => $this->guard['id'] ,
			'response' => $this->response,
		], $this->request);
		LogRequestResponseModel::create($params);
	}
}
