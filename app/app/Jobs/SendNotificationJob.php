<?php namespace App\Jobs;

use App\CustomStuff\Interfaces\SendNotificationStrategyInterface;
use App\Events\JobDoneEvent;
use App\Events\JobFailedEvent;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Exception;
use Illuminate\Support\Facades\Log;

class SendNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	
	public   $tries = 5;
	private  $extraParams;
    private  $notifyResponse;
	private  $notificationStrategy;
	private  $request;
	private  $delayInSeconds = 5; //time for retry jobs if firebase has error
	
	/**
	 * Create a new job instance.
	 *
	 * @param SendNotificationStrategyInterface $notificationStrategy
	 * @param array                             $extraParams
	 * @param array                             $request
	 */
    public function __construct(SendNotificationStrategyInterface $notificationStrategy, array $extraParams = [], array $request)
    {
        $this->notificationStrategy = $notificationStrategy;
        $this->extraParams = $extraParams;
        $this->request = $request;
    }

    
    /**
	 *
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
    	$firebaseResponse = $this->notificationStrategy->sendNotification(); //send notify
		
		if($this->checkErrorFcmResult($firebaseResponse) === true) {
			return;
		}
		
		$this->notifyResponse = $firebaseResponse;
		event(new JobDoneEvent($this->getData(), $this->request));
    }


    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        //Send user notification of failure, etc...
		event(new JobFailedEvent($this->getData(), $this->request));
	}
	
	
	/**
	 * //todo add comment for this function Nead!!
	 * @return mixed
	 */
	private function getData()
	{
		$sendParams = $this->getNotificationParams();
		$result['user']		   =  $this->getUserObject($sendParams['user']);
		unset($sendParams['user']);
		$result['sendParams']     = $sendParams;
		$result['seen']           = 0;
		$result['extraParams']    = $this->extraParams;
		$result['notifyResponse'] = json_decode($this->notifyResponse[0],1);
		return $result;
	}
	
	
	/**
	 *
	 * //todo add comment for this function Nead!!
	 * @param $user
	 * @return array|null
	 */
	private function getUserObject($user)
	{
		if(isset($user) && $user != null){
			return [
				"id"            => $user->id ,
				"languageId"    => $user->language_id ,
				"firstName"     => $user->first_name ,
				"middleName"    => $user->middle_name ,
				"lastName"      => $user->last_name ,
				"avatar"        => $user->avatar ,
				"birthDate"     => $user->birth_date ,
				"nationalityId" => $user->nationality_id ,
			];
		}
		return  null;
	}
	
	
	/**
	 * //todo add comment for this function Nead!!
	 * @return array|false
	 */
	private function getNotificationParams()
	{
		$data = (array)$this->notificationStrategy;
		$array_new_keys =array_map( function( $value ) {
			return  str_replace("\x00App\CustomStuff\Classes\SendFireBaseNotification\x00","",$value);
		} , array_keys($data) );
		return array_combine($array_new_keys, array_values($data));
		
	}
	
	
	/**
	 * check firebase result if result has error and error equal "InternalServerError"
	 * add attempt for retry jobs after 15 sec
	 * @param array $firebaseResponse
	 * @return bool
	 */
	private function checkErrorFcmResult(array $firebaseResponse) : bool
	{
		try {
			$result  = json_decode($firebaseResponse[0]); //decode firebase result
		
			if (isset($result->results)) {
				if (isset($result->results[0]->error) &&  preg_match('/Internal/i', $result->results[0]->error) ) {
					return $this->addAttempts();
				}
			}
			return false;
			
		} catch(Exception $exception) {
			return $this->addAttempts();
		}
	}
	
	
	/**
	 * add attempt for this job
	 * if attempts < tries return true for run job again
	 * @return bool
	 */
	private function addAttempts()
	{
		if ($this->attempts() < $this->tries) {
			$this->release($this->delayInSeconds);
			Log::debug("not");
			return true;
		}
		return false;
	}
	
	
}
