<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Entities\Language\Language;
use App\Models\Entities\User\User;
use App\Models\Repositories\Country\CountryRepository;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
	$language = Language::pluck('id')->toArray();
	$nationalityId = app(CountryRepository::class)->getByIso2('us');
    return [
		'language_id'    => $language[array_rand( $language )] ,
		'first_name'     => $faker->firstName ,
		'middle_name'    => $faker->firstName ,
		'last_name'      => $faker->lastName ,
		'ds_name'        => $faker->name ,
		'avatar'         => $faker->imageUrl( 250 , 250 ) ,
		'birth_date'     => $faker->dateTime( Carbon::now()->subYears( 50 ) ) ,
		'nationality_id' => $nationalityId
    ];
});
