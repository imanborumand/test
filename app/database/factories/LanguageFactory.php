<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Entities\Language\Language;
use Faker\Generator as Faker;

$factory->define(Language::class, function (Faker $faker) {
	$iso2Array = [
		'en' => 'English',
		'fa' => 'فارسی',
		'ar' => 'عربی',
		'ku' => 'کوردی',
	];
	$directionArray = [
		'en' => 'ltr',
		'fa' => 'rtl',
		'ar' => 'rtl',
		'ku' => 'rtl',
	];
	$randIndex = array_rand($iso2Array);
	
	return [
		'iso2'         => $randIndex,
		'title'       => $iso2Array[$randIndex],
		'direction'   => $directionArray[$randIndex],
		'device'      => 'mobile' ,
		'flag'   	  => $faker->imageUrl(250, 250),
		'status'      => true ,
    ];
});
