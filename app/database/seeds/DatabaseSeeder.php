<?php use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountryTableSeeder::class);
        $this->call(DeviceBrandSeeder::class);
        $this->call(DeviceSeeder::class);
        $this->call(LanguageTableSeeder::class);
        $this->call(TranslateSeeder::class);
        $this->call(ContactTableSeeder::class);
        $this->call(DeviceSettingSeeder::class);
    }
}
