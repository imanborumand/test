<?php

use App\Models\Entities\Device\DeviceType;
use App\Models\Entities\Device\DeviceBrand;
use Illuminate\Database\Seeder;

class DeviceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$deviceBrand = DeviceBrand::where('name',EN_DEFAULT_WORD)->first();
    	if($deviceBrand) {
            DeviceType::firstOrCreate( [
                'device_brand_id' => $deviceBrand->id ,
                'name' => EN_DEFAULT_WORD,
                'os'   => EN_DEFAULT_WORD,
            ]);
        }

    }
}
