<?php

use App\Models\Entities\Device\DeviceBrand;
use Illuminate\Database\Seeder;

/*
 * deviceBrand seeder
 * create default device by id = 1
 */
class DeviceBrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DeviceBrand::firstOrCreate([
			'name' => EN_DEFAULT_WORD ,
		]);
    }
}
