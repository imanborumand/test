<?php

use App\Models\Entities\Language\Language;
use Illuminate\Database\Seeder;

/*
 * seeder for add languages in project in develop and first run in master
 */
class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	
        Language::updateOrCreate(
        	[ 'iso2' => 'en'],
            ['status' => true, 'title' => 'English', 'direction' => 'ltr', 'flag' => 'us']);
        
		Language::updateOrCreate(
			[ 'iso2' => 'fa'],
			['status' => true, 'title' => 'فارسی', 'direction' => 'rtl', 'flag' => 'ir']);
		
		Language::updateOrCreate(
			[ 'iso2' => 'ar'],
			['status' => true, 'title' => 'عربی', 'direction' => 'rtl', 'flag' => 'sa']);
	
		Language::updateOrCreate(
			[ 'iso2' => 'ku'],
			['status' => true, 'title' => 'کوردی', 'direction' => 'rtl', 'flag' => 'kurd']);
	
		Language::updateOrCreate(
			[ 'iso2' => 'tr'],
			['status' => true, 'title' => 'turkish', 'direction' => 'ltr', 'flag' => 'tr']);
	
		Language::updateOrCreate(
			[ 'iso2' => 'ru'],
			['status' => true, 'title' => 'Russian', 'direction' => 'ltr', 'flag' => 'ru']);
		
    }
}
