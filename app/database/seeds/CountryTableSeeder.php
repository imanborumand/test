<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Entities\Country\Country;

class CountryTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $baseSqlFile = __DIR__ . "/static_files/countries.sql";

        if(file_exists($baseSqlFile)) {

            if(Country::all()->count() == 0) {
                //collect contents and pass to DB::unprepared
                DB::unprepared(file_get_contents($baseSqlFile));
            }
        }


    }

}
