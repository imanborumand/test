<?php

use App\Models\Entities\Contact\Contact;
use Illuminate\Database\Seeder;

/**
 * Class ContactTableSeeder
 * contact seeder
 */
class ContactTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Contact::firstOrCreate([
			'title' => 'firebase token',
			'key'   => config('general.contact_type_keys.fcm'),
			'displayable'   => false,
		]);

        Contact::firstOrCreate([
            'title' => 'mobile number',
            'key'   => config('general.contact_type_keys.mobile'),
            'displayable'   => true,
        ]);

        Contact::firstOrCreate([
            'title' => 'email',
            'key'   => config('general.contact_type_keys.email'),
            'displayable'   => true,
        ]);

	}
}
