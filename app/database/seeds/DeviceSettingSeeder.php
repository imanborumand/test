<?php

use App\Models\Entities\Device\DeviceSetting;
use App\Models\Entities\Device\DeviceType;
use App\Models\Entities\Language\Language;
use Illuminate\Database\Seeder;

class DeviceSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $language = Language::where('iso2', DEFAULT_LANGUAGE_ISO2)->first();
        if($language) {
            $deviceType = DeviceType::where('name', EN_DEFAULT_WORD)->first();
            if($deviceType) {
                DeviceSetting::firstOrCreate([
                    'device_type_id' => $deviceType->id,
                    'language_id' => $language->id,
                    'font_size' => '14',
                    'font_family' => 'iran'
                ]);
            }
        }

    }
}
