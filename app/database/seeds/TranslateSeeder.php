<?php

use App\CustomStuff\Classes\TranslatesManagement;
use App\Models\Entities\Country\Country;
use App\Models\Entities\Language\Language;
use App\Models\Entities\Language\TranslateKey;
use App\Models\Repositories\Device\DeviceTypeRepository;
use App\Models\Repositories\Language\LanguageRepository;
use Illuminate\Database\Seeder;

/*
 *
 */
class TranslateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
	 * get user device info
	 * import json file that contain translates to translate table
	 * get country name list  as new keys  and store to translate key table
	 * read
     * @return void
     */
    public function run()
    {
		$defaultDevice = app(DeviceTypeRepository::class)->getByName();
		$this->importLanguageTranslates($defaultDevice);
		$this->insertCountryNameAsKey($defaultDevice);
		
    }
	
	
	/**
	 * 1-get user device info in function params
	 * 2-get list of all languages
	 * for all of language check if there is related translate json file in static file directory
	 * then store new translate key and translates to database
	 *
	 * @param $defaultDevice
	 */
	private function importLanguageTranslates(object $defaultDevice){
		$languages = app(LanguageRepository::class)->allLanguages();
		foreach($languages as $language){
			app(TranslatesManagement::class)->import($language,$defaultDevice);
		}
	}
	
	
	/**
	 * 1-get list of all countries
	 * 2-store country names as new keys in translate key table
	 * 3-get translate key ids
	 * 4-get default language that is ->  en
	 * 5-save country names in translate table by default language
	 * @param $defaultDevice
	 */
	private function insertCountryNameAsKey(object $defaultDevice)
	{
		$countryNames = Country::pluck('name')->toArray();
		$keysData =[];
		foreach($countryNames as $name) {
			$keysData[]['key'] =$name;
		}
		bulkInsertOrUpdate($keysData,'translate_keys');
		
		$translateKeysWithId = TranslateKey::whereIn('key',$countryNames)->pluck('key','id')->toArray();
		$language= app(LanguageRepository::class)->getByIso2(DEFAULT_LANGUAGE_ISO2);
		
		$data = [];
		foreach($translateKeysWithId as $translateKeyId => $translateKeyName){
			$data[] = [
				'language_id'      => $language->id ,
				'translate_key_id' => $translateKeyId ,
				'device_type_id'   => $defaultDevice->id ,
				'value'            => $translateKeyName ,
			];
		}//foreach
		if(count($data) > 0){
			bulkInsertOrUpdate($data,'translates');
		}
	}
	
}
