<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('language_id')->index()->unsigned();
			$table->foreign('language_id')->references('id')->on('languages');
            $table->string('first_name', 80)->nullable();
            $table->string('middle_name', 80)->nullable();
            $table->string('last_name', 80)->nullable();
            $table->string('ds_name', 100)->nullable();
            $table->string('avatar')->nullable();
            $table->date('birth_date')->nullable();
            $table->bigInteger('nationality_id')->index()->unsigned()->nullable()->default(0);
			//$table->foreign('nationality_id')->references('id')->on('countries');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
