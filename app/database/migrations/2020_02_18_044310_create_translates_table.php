<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTranslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translates', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->bigInteger('language_id')->index()->unsigned();
			$table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');
            $table->bigInteger('translate_key_id')->index()->unsigned();
            $table->foreign('translate_key_id')->references('id')->on('translate_keys')->onDelete('cascade');
			$table->bigInteger('device_type_id')->index()->unsigned();
            $table->foreign('device_type_id')->references('id')->on('device_types')->onDelete('cascade');
			$table->text('value')->comment("value of word in base language");
			$table->unique(['language_id', 'translate_key_id','device_type_id']);
            $table->timestamps();

		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translates');
    }
}
