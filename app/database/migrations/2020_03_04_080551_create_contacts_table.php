<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('key', 50)->index()->unique()->comment('type of contact');
			$table->boolean('displayable')->default(true)->comment('for show this contact in front');
			$table->enum('keyboard_type', ['Phone', 'Normal', 'Email', 'Number', 'dateTime'])->default('Normal')->comment('for use in mobile app, show keypad');
			$table->string('regex')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
