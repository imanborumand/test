<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->bigInteger('user_id')->index()->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->string('ds_booking_id');
			$table->bigInteger('country_id')->default(0)->index()->unsigned();
			//$table->foreign('country_id')->references('id')->on('countries');
            $table->timestamps();
			$table->unique(array('user_id', 'ds_booking_id'));
	
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
