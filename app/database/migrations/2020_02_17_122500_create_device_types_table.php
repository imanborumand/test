<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeviceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('device_brand_id')->index()->unsigned();
            $table->foreign('device_brand_id')->references('id')->on('device_brands')->onDelete('cascade');
            $table->string('name', 50)->index()->comment('device name like A9 - A7');
            $table->enum('os', [
            	'default',
                'android',
                'ios',
                'osx',
                'linux',
                'blackBerry',
                'ubuntu',
                'windows',
                'windowsPhone',
            ])->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_types');
    }
}
