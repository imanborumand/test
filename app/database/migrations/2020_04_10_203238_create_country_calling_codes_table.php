<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountryCallingCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_calling_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('country_id')->index()->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->string('code', 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country_calling_codes');
    }
}
