<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/static_files/app_redirect/style.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ env("APP_NAME") }}</title>
</head>
<body>
<div class="wrapper">
    <div class="wrapper__body">


        <div class="wrapper__body__logo">
            <img src="/static_files/app_redirect/assest/img/logo.svg" alt="">
        </div>
        <div class="wrapper__body__title">
            <span class="wrapper__body__title__text">
                    Using technology to provide added value to the travel industry
            </span>
        </div>
        <div class="wrapper__body__descriptBox">
            <span class="wrapper__body__descriptBox__text">
                Welcom to the Turpal Aplication, you are able to install directly via below link
            </span>
        </div>
        <div class="wrapper__body__btnBox">
            <div class="wrapper__body__btnBox__content">
                <div class="wrapper__body__btnBox__content__item">
                    <div class="wrapper__body__btnBox__content__item__logo" >
                        <img src="/static_files/app_redirect/assest/icon/googleplay.svg">
                    </div>
                    <div class="wrapper__body__btnBox__content__item__textBox">
                        <div class="wrapper__body__btnBox__content__item__textBox__head">
                            <span>Download on the</span>
                        </div>
                        <div class="wrapper__body__btnBox__content__item__textBox__footer">
                            <a href="{{$appVersion['android']['store_url']}}">
                                <img src="/static_files/app_redirect/assest/img/textgoogle.svg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper__body__btnBox__content">
                <div class="wrapper__body__btnBox__content__item">

                    <div class="wrapper__body__btnBox__content__item__logo" >

                        <img src="/static_files/app_redirect/assest/icon/apple.svg">

                    </div>

                    <div class="wrapper__body__btnBox__content__item__textBox">
                        <div class="wrapper__body__btnBox__content__item__textBox__head">
                            <span>Download on the</span>
                        </div>
                        <div class="wrapper__body__btnBox__content__item__textBox__footer">
                            <a href="{{$appVersion['iphone']['store_url']}}">
                            <img src="/static_files/app_redirect/assest/img/textapple.svg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper__footer">
        <div class="wrapper__footer__img">
            <img src="/static_files/app_redirect/assest/img/world.svg" alt="">
        </div>
        <div class="wrapper__footer__text">
            <span>Copyright ©  {{date('Y')}} <a href="#">DREAM DAYS LLC</a> All rights reserved.</span>
        </div>
    </div>                                                                                                                                                      </div>
</div>

</body>
</html>
