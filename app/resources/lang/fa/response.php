<?php

return [
    'messages' => [
		'an_error_occurred'        => 'خطایی رخ داده است.' ,
		'item_not_found'           => 'نتیجه ای با توجه اطلاعات وارد شده یافت نشد' ,
		'bad_request_error'        => 'درخواست ارسال شده سمت سرور صحیح نمیباشد' ,
		'unauthorized_error'       => 'شما جهت دسترسی به این بخش احراز هویت نشدید' ,
		'forbidden_error'          => 'شما دسترسی لازم برای انجام عملیات را ندارید' ,
		'internal_error'           => 'خطایی سمت سرور راخ داده است' ,
		'uploading_file_error'     => 'آپلود فایل با خطا مواجه شده است' ,
		'validation_error_message' => 'اطلاعات وارد شده معتبر نیستند' ,
		'products_import'		   => 'محصولات با موفقیت بارگزاری شدند' ,
	
		'item_created' => 'آیتم مورد نظر ثبت گردید.' ,
		'item_updated' => 'آیتم مورد نظر به روز رسانی گشت' ,
		'item_patched' => 'آیتم مورد نظر به روز رسانی گشت' ,
		'item_deleted' => 'آیتم مورد نظر حذف گشت' ,
    ]
];
